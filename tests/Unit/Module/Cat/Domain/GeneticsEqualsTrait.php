<?php

declare(strict_types=1);

namespace App\Tests\Unit\Module\Cat\Domain;

use App\Module\Cat\Domain\ValueObject\Genetics;
use UnexpectedValueException;

trait GeneticsEqualsTrait
{
    private static function assertGeneticsEquals(string $expected, Genetics $genetics): void
    {
        $neededKeys = array_map(
            fn ($v) => explode(':', $v)[0],
            explode('|', $expected),
        );

        self::assertSame($expected, self::getGeneticsAsString($genetics, ...$neededKeys));
    }

    private static function getGeneticsAsString(Genetics $genetics, string ...$neededKeys): string
    {
        $parts = [];
        foreach ($neededKeys as $key) {
            $parts[] = $key . ':' . match ($key) {
                    'breed' => $genetics->breed->name,
                    'sex' => strtolower($genetics->sex->name[0]),
                    'whAmt' => $genetics->whiteAmount?->name,
                    'eye' => $genetics->eyeColor?->name,
                    'W' => "{$genetics->whiteAlleles->allele1->name},{$genetics->whiteAlleles->allele2?->name}",
                    'O' => "{$genetics->orangeAlleles->allele1?->name},{$genetics->orangeAlleles->allele2?->name}",
                    'B' => "{$genetics->blackAlleles->allele1?->name},{$genetics->blackAlleles->allele2?->name}",
                    'D' => "{$genetics->dilutorAlleles->allele1?->name},{$genetics->dilutorAlleles->allele2?->name}",
                    'Dm' => sprintf(
                        '%s,%s',
                        $genetics->diluteModifierAlleles->allele1?->name,
                        $genetics->diluteModifierAlleles->allele2?->name,
                    ),
                    'C' => "{$genetics->colorAlleles->allele1?->name},{$genetics->colorAlleles->allele2?->name}",
                    'A' => "{$genetics->agutiAlleles->allele1?->name},{$genetics->agutiAlleles->allele2?->name}",
                    'I' => "{$genetics->silverAlleles->allele1?->name},{$genetics->silverAlleles->allele2?->name}",
                    'S' => "{$genetics->withWhiteAlleles->allele1?->name},{$genetics->withWhiteAlleles->allele2?->name}",
                    'T' => "{$genetics->tabbyAlleles->allele1?->name},{$genetics->tabbyAlleles->allele2?->name}",
                    'pra' => "{$genetics->praAlleles->allele1?->name},{$genetics->praAlleles->allele2?->name}",
                    default => throw new UnexpectedValueException(),
                };
        }

        return join('|', $parts);
    }

}
