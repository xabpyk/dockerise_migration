<?php

declare(strict_types=1);

namespace App\Tests\Unit\Module\Cat\Domain\Service;

use App\Module\Cat\Domain\DependencyInterface\CatRepositoryInterface;
use App\Module\Cat\Domain\Entity\Cat;
use App\Module\Cat\Domain\Entity\CatFactoryInterface;
use App\Module\Cat\Domain\Service\CatHandler;
use App\Module\Cat\Domain\ValueObject\CatName;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Breed;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Color;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Sex;
use App\Module\Cat\Domain\ValueObject\Genetics;
use App\Module\Cat\Domain\ValueObject\PedigreeNumber;
use DateTimeImmutable;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

final class CatHandlerTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    private const int CAT_ID = 10;

    private const int FATHER_ID = 20;
    private const int MOTHER_ID = 30;

    private const int FIRST_CHILD_ID = 100;
    private const int SECOND_CHILD_ID = 200;
    private const int THIRD_CHILD_ID = 300;
    private const int FOURTH_CHILD_ID = 400;

    private const int OTHER_PARENT_ID = 1000;

    public function testAddShort(): void
    {
        $name = new CatName('Murzik Vasyliovych');
        $genetics = Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK);
        $cat = new Cat(self::CAT_ID, $name, $genetics);

        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('nextId')->once()->andReturn(self::CAT_ID);
        $repository->shouldReceive('add')->once()->with($cat);

        $catFactory = Mockery::mock(CatFactoryInterface::class);
        $catFactory->shouldReceive('create')->once()->with(
            self::CAT_ID,
            $name,
            $genetics,
            null,
            null,
            null,
            null,
        )->andReturn($cat);

        (new CatHandler($repository, $catFactory))->add($name, $genetics);
    }

    public function testAddFull(): void
    {
        $name = new CatName('Murzik Vasyliovych');
        $genetics = Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK);
        $birthDate = new DateTimeImmutable('2020-01-01');
        $pedigreeNumber = new PedigreeNumber('1234567');

        $cat = new Cat(self::CAT_ID, $name, $genetics, $birthDate, $pedigreeNumber, self::FATHER_ID, self::MOTHER_ID);

        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('nextId')->once()->andReturn(self::CAT_ID);
        $repository->shouldReceive('add')->once()->with($cat);

        $catFactory = Mockery::mock(CatFactoryInterface::class);
        $catFactory->shouldReceive('create')->once()->with(
            self::CAT_ID,
            $name,
            $genetics,
            $birthDate,
            $pedigreeNumber,
            self::FATHER_ID,
            self::MOTHER_ID,
        )->andReturn($cat);

        (new CatHandler($repository, $catFactory))->add(
            $name,
            $genetics,
            $birthDate,
            $pedigreeNumber,
            self::FATHER_ID,
            self::MOTHER_ID,
        );
    }

    public function testUpdateShort(): void
    {
        $name = new CatName('Murzik Vasyliovych');
        $genetics = Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK);
        $cat = new Cat(self::CAT_ID, $name, $genetics);

        $catFactory = Mockery::mock(CatFactoryInterface::class);
        $catFactory->shouldReceive('create')->once()->with(
            self::CAT_ID,
            $name,
            $genetics,
            null,
            null,
            null,
            null,
        )->andReturn($cat);

        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('update')->once()->with($cat);

        (new CatHandler($repository, $catFactory))->update(self::CAT_ID, $name, $genetics);
    }

    public function testUpdateFull(): void
    {
        $name = new CatName('Murzik Vasyliovych');
        $genetics = Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK);
        $birthDate = new DateTimeImmutable('2020-01-01');
        $pedigreeNumber = new PedigreeNumber('1234567');

        $cat = new Cat(self::CAT_ID, $name, $genetics, $birthDate, $pedigreeNumber, self::FATHER_ID, self::MOTHER_ID);

        $catFactory = Mockery::mock(CatFactoryInterface::class);
        $catFactory->shouldReceive('create')->once()->with(
            self::CAT_ID,
            $name,
            $genetics,
            $birthDate,
            $pedigreeNumber,
            self::FATHER_ID,
            self::MOTHER_ID,
        )->andReturn($cat);

        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('update')->once()->with($cat);

        (new CatHandler($repository, $catFactory))->update(
            self::CAT_ID,
            $name,
            $genetics,
            $birthDate,
            $pedigreeNumber,
            self::FATHER_ID,
            self::MOTHER_ID,
        );
    }

    public static function deleteWithoutChildExistProvider(): iterable
    {
        $male = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );
        $female = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );

        yield [$male];
        yield [$female];
    }

    #[DataProvider('deleteWithoutChildExistProvider')]
    public function testDeleteWithoutChildExists(Cat $cat): void
    {
        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getChildren')->once()->with(self::CAT_ID)->andReturn([]);
        $repository->shouldReceive('remove')->once()->with(self::CAT_ID);

        $catFactory = self::createStub(CatFactoryInterface::class);

        (new CatHandler($repository, $catFactory))->remove(self::CAT_ID);
    }

    public static function deleteWithChildExistsProvider(): iterable
    {
        $male = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );

        yield [
            $male,
            [new Cat(
                self::FIRST_CHILD_ID,
                new CatName('Murzik Vasyliovych'),
                Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
                fatherId: self::CAT_ID,
            )],
            [self::FIRST_CHILD_ID => ['father' => null, 'mother' => null]],
        ];
        yield [
            $male,
            [new Cat(
                self::FIRST_CHILD_ID,
                new CatName('Nika Vasylivna'),
                Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
                fatherId: self::CAT_ID,
            )],
            [self::FIRST_CHILD_ID => ['father' => null, 'mother' => null]],
        ];
        yield [
            $male,
            [new Cat(
                self::FIRST_CHILD_ID,
                new CatName('Murzik Vasyliovych'),
                Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
                fatherId: self::CAT_ID,
                motherId: self::OTHER_PARENT_ID,
            )],
            [self::FIRST_CHILD_ID => ['father' => null, 'mother' => self::OTHER_PARENT_ID]],
        ];
        yield [
            $male,
            [new Cat(
                self::FIRST_CHILD_ID,
                new CatName('Nika Vasylivna'),
                Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
                fatherId: self::CAT_ID,
                motherId: self::OTHER_PARENT_ID,
            )],
            [self::FIRST_CHILD_ID => ['father' => null, 'mother' => self::OTHER_PARENT_ID]],
        ];
        yield [
            $male,
            [
                new Cat(
                    self::FIRST_CHILD_ID,
                    new CatName('Murzik Vasyliovych'),
                    Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
                    fatherId: self::CAT_ID,
                ),
                new Cat(
                    self::SECOND_CHILD_ID,
                    new CatName('Nika Vasylivna'),
                    Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
                    fatherId: self::CAT_ID,
                ),
                new Cat(
                    self::THIRD_CHILD_ID,
                    new CatName('Zhorik Vasyliovych'),
                    Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
                    fatherId: self::CAT_ID,
                    motherId: self::OTHER_PARENT_ID,
                ),
                new Cat(
                    self::FOURTH_CHILD_ID,
                    new CatName('Rosa Vasylivna'),
                    Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
                    fatherId: self::CAT_ID,
                    motherId: self::OTHER_PARENT_ID,
                ),
            ],
            [
                self::FIRST_CHILD_ID => ['father' => null, 'mother' => null],
                self::SECOND_CHILD_ID => ['father' => null, 'mother' => null],
                self::THIRD_CHILD_ID => ['father' => null, 'mother' => self::OTHER_PARENT_ID],
                self::FOURTH_CHILD_ID => ['father' => null, 'mother' => self::OTHER_PARENT_ID],
            ],
        ];

        $female = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );

        yield [
            $female,
            [new Cat(
                self::FIRST_CHILD_ID,
                new CatName('Murzik Vasyliovych'),
                Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
                motherId: self::CAT_ID,
            )],
            [self::FIRST_CHILD_ID => ['father' => null, 'mother' => null]],
        ];
        yield [
            $female,
            [new Cat(
                self::FIRST_CHILD_ID,
                new CatName('Nika Vasylivna'),
                Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
                motherId: self::CAT_ID,
            )],
            [self::FIRST_CHILD_ID => ['father' => null, 'mother' => null]],
        ];
        yield [
            $female,
            [new Cat(
                self::FIRST_CHILD_ID,
                new CatName('Murzik Vasyliovych'),
                Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
                fatherId: self::OTHER_PARENT_ID,
                motherId: self::CAT_ID,
            )],
            [self::FIRST_CHILD_ID => ['father' => self::OTHER_PARENT_ID, 'mother' => null]],
        ];
        yield [
            $female,
            [new Cat(
                self::FIRST_CHILD_ID,
                new CatName('Nika Vasylivna'),
                Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
                fatherId: self::OTHER_PARENT_ID,
                motherId: self::CAT_ID,
            )],
            [self::FIRST_CHILD_ID => ['father' => self::OTHER_PARENT_ID, 'mother' => null]],
        ];
        yield [
            $female,
            [
                new Cat(
                    self::FIRST_CHILD_ID,
                    new CatName('Murzik Vasyliovych'),
                    Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
                    motherId: self::CAT_ID,
                ),
                new Cat(
                    self::SECOND_CHILD_ID,
                    new CatName('Nika Vasylivna'),
                    Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
                    motherId: self::CAT_ID,
                ),
                new Cat(
                    self::THIRD_CHILD_ID,
                    new CatName('Zhorik Vasyliovych'),
                    Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
                    fatherId: self::OTHER_PARENT_ID,
                    motherId: self::CAT_ID,
                ),
                new Cat(
                    self::FOURTH_CHILD_ID,
                    new CatName('Rosa Vasylivna'),
                    Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
                    fatherId: self::OTHER_PARENT_ID,
                    motherId: self::CAT_ID,
                ),
            ],
            [
                self::FIRST_CHILD_ID => ['father' => null, 'mother' => null],
                self::SECOND_CHILD_ID => ['father' => null, 'mother' => null],
                self::THIRD_CHILD_ID => ['father' => self::OTHER_PARENT_ID, 'mother' => null],
                self::FOURTH_CHILD_ID => ['father' => self::OTHER_PARENT_ID, 'mother' => null],
            ],
        ];
    }

    #[DataProvider('deleteWithChildExistsProvider')]
    public function testDeleteWithChildExists(Cat $cat, array $children, array $parentIds): void
    {
        $updatingChildrenCheckCallback = function (Cat ...$updatingCats) use ($children, $parentIds): bool {
            $children = array_combine(array_map(fn (Cat $c) => $c->id, $children), $children);

            foreach ($updatingCats as $updatingCat) {
                if (
                    !isset($children[$updatingCat->id]) ||
                    $updatingCat !== $children[$updatingCat->id] ||
                    $updatingCat->getFatherId() !== $parentIds[$updatingCat->id]['father'] ||
                    $updatingCat->getMotherId() !== $parentIds[$updatingCat->id]['mother']
                ) {
                    return false;
                }

                unset($children[$updatingCat->id]);
            }

            return empty($children);
        };

        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getCatsById')->once()->with(self::CAT_ID)->andReturn($cat);
        $repository->shouldReceive('getChildren')->once()->with(self::CAT_ID)->andReturn($children);
        $repository->shouldReceive('update')->once()->withArgs($updatingChildrenCheckCallback);
        $repository->shouldReceive('remove')->once()->with(self::CAT_ID);

        $catFactory = self::createStub(CatFactoryInterface::class);

        (new CatHandler($repository, $catFactory))->remove(self::CAT_ID);
    }
}
