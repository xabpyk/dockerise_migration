<?php

declare(strict_types=1);

namespace App\Tests\Unit\Module\Cat\Domain\Criteria;

use App\Module\Cat\Domain\Criteria\CatHasCorrectChildrenCriterion;
use App\Module\Cat\Domain\DependencyInterface\CatRepositoryInterface;
use App\Module\Cat\Domain\Entity\Cat;
use App\Module\Cat\Domain\Exception\CatValidationException;
use App\Module\Cat\Domain\ValueObject\CatName;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Breed;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Color;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Sex;
use App\Module\Cat\Domain\ValueObject\Genetics;
use DateTimeImmutable;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

final class CatHasCorrectChildrenCriterionTest extends TestCase
{
    private const int CAT_ID = 10;
    private const int FIRST_OTHER_PARENT_ID = 21;
    private const int SECOND_OTHER_PARENT_ID = 22;
    private const int THIRD_OTHER_PARENT_ID = 23;
    private const int FIRST_CHILD_ID = 100;
    private const int SECOND_CHILD_ID = 200;
    private const int THIRD_CHILD_ID = 300;
    private const int FOURTH_CHILD_ID = 400;
    private const int FIFTH_CHILD_ID = 500;
    private const int SIXTH_CHILD_ID = 600;
    private const int SEVENTH_CHILD_ID = 700;
    private const int EIGHTH_CHILD_ID = 800;

    use MockeryPHPUnitIntegration;

    public static function withoutChildrenProvider(): iterable
    {
        $male = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );
        $female = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );

        yield [$male];
        yield [$female];
    }

    #[DataProvider('withoutChildrenProvider')]
    public function testWithoutChildren(Cat $cat): void
    {
        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getChildren')->with(self::CAT_ID)->once()->andReturn([]);

        (new CatHasCorrectChildrenCriterion($repository))->ensureIsSatisfiedBy($cat);
    }

    public static function catHasCompatibleSexWithChildrenProvider(): iterable
    {
        $maleCat = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );
        $femaleCat = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );

        $firstChildWithFather = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $secondChildWithFather = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );

        $firstChildWithMother = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $secondChildWithMother = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );

        return [
            [$maleCat, $firstChildWithFather],
            [$maleCat, $firstChildWithFather, $secondChildWithFather],
            [$femaleCat, $firstChildWithMother],
            [$femaleCat, $firstChildWithMother, $secondChildWithMother],
        ];
    }

    #[DataProvider('catHasCompatibleSexWithChildrenProvider')]
    public function testCatHasCompatibleSexWithChildren(Cat $cat, Cat ...$children): void
    {
        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getChildren')->with(self::CAT_ID)->once()->andReturn($children);

        (new CatHasCorrectChildrenCriterion($repository))->ensureIsSatisfiedBy($cat);
    }

    public static function catHasIncompatibleSexWithChildrenProvider(): iterable
    {
        $maleCat = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );
        $femaleCat = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );

        $firstChildWithFather = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $secondChildWithFather = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );

        $firstChildWithMother = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $secondChildWithMother = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );

        return [
            [$maleCat, $firstChildWithMother],
            [$maleCat, $firstChildWithMother, $secondChildWithMother],
            [$femaleCat, $firstChildWithFather],
            [$femaleCat, $firstChildWithFather, $secondChildWithFather],
        ];
    }

    #[DataProvider('catHasIncompatibleSexWithChildrenProvider')]
    public function testCatHasIncompatibleSexWithChildren(Cat $cat, Cat ...$children): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::FORBIDDEN_TO_CHANGE_SEX_OF_PARENT_WHO_HAS_CHILD);

        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getChildren')->with(self::CAT_ID)->once()->andReturn($children);

        (new CatHasCorrectChildrenCriterion($repository))->ensureIsSatisfiedBy($cat);
    }

    public static function childrenHasCompatibleBirthDateProvider(): iterable
    {
        $maleCatWithoutBirthDate = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );
        $maleCatWithBirthDate = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2000-04-10'),
        );
        $sonWithoutBirthDate = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $sonWithMinBirthDate = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2000-10-10'),
            fatherId: self::CAT_ID,
        );
        $sonWithMaxBirthDate = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Barmaley Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2015-04-10'),
            fatherId: self::CAT_ID,
        );
        $daughterWithoutBirthDate = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $daughterWithMinBirthDate = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Rosa Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2000-10-10'),
            fatherId: self::CAT_ID,
        );
        $daughterWithMaxBirthDate = new Cat(
            self::SIXTH_CHILD_ID,
            new CatName('Monya Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2015-04-10'),
            fatherId: self::CAT_ID,
        );

        yield [$maleCatWithoutBirthDate, $sonWithoutBirthDate];
        yield [$maleCatWithoutBirthDate, $sonWithMinBirthDate];
        yield [$maleCatWithoutBirthDate, $daughterWithoutBirthDate];
        yield [$maleCatWithoutBirthDate, $daughterWithMinBirthDate];
        yield [
            $maleCatWithoutBirthDate,
            $sonWithoutBirthDate,
            $sonWithMinBirthDate,
            $sonWithMaxBirthDate,
            $daughterWithoutBirthDate,
            $daughterWithMinBirthDate,
            $daughterWithMaxBirthDate,
        ];
        yield [$maleCatWithBirthDate, $sonWithoutBirthDate];
        yield [$maleCatWithBirthDate, $sonWithMinBirthDate];
        yield [$maleCatWithBirthDate, $sonWithMaxBirthDate];
        yield [$maleCatWithBirthDate, $daughterWithoutBirthDate];
        yield [$maleCatWithBirthDate, $daughterWithMinBirthDate];
        yield [$maleCatWithBirthDate, $daughterWithMaxBirthDate];
        yield [
            $maleCatWithBirthDate,
            $sonWithoutBirthDate,
            $sonWithMinBirthDate,
            $sonWithMaxBirthDate,
            $daughterWithoutBirthDate,
            $daughterWithMinBirthDate,
            $daughterWithMaxBirthDate,
        ];

        $femaleCatWithoutBirthDate = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );
        $femaleCatWithBirthDate = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2000-04-10'),
        );
        $sonWithoutBirthDate = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $sonWithMinBirthDate = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2000-12-10'),
            motherId: self::CAT_ID,
        );
        $sonWithMaxBirthDate = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Barmaley Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2010-04-10'),
            motherId: self::CAT_ID,
        );
        $daughterWithoutBirthDate = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $daughterWithMinBirthDate = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Rosa Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2000-12-10'),
            motherId: self::CAT_ID,
        );
        $daughterWithMaxBirthDate = new Cat(
            self::SIXTH_CHILD_ID,
            new CatName('Monya Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2010-04-10'),
            motherId: self::CAT_ID,
        );

        yield [$femaleCatWithoutBirthDate, $sonWithoutBirthDate];
        yield [$femaleCatWithoutBirthDate, $sonWithMinBirthDate];
        yield [$femaleCatWithoutBirthDate, $daughterWithoutBirthDate];
        yield [$femaleCatWithoutBirthDate, $daughterWithMinBirthDate];
        yield [
            $femaleCatWithoutBirthDate,
            $sonWithoutBirthDate,
            $sonWithMinBirthDate,
            $sonWithMaxBirthDate,
            $daughterWithoutBirthDate,
            $daughterWithMinBirthDate,
            $daughterWithMaxBirthDate,
        ];
        yield [$femaleCatWithBirthDate, $sonWithoutBirthDate];
        yield [$femaleCatWithBirthDate, $sonWithMinBirthDate];
        yield [$femaleCatWithBirthDate, $sonWithMaxBirthDate];
        yield [$femaleCatWithBirthDate, $daughterWithoutBirthDate];
        yield [$femaleCatWithBirthDate, $daughterWithMinBirthDate];
        yield [$femaleCatWithBirthDate, $daughterWithMaxBirthDate];
        yield [
            $femaleCatWithBirthDate,
            $sonWithoutBirthDate,
            $sonWithMinBirthDate,
            $sonWithMaxBirthDate,
            $daughterWithoutBirthDate,
            $daughterWithMinBirthDate,
            $daughterWithMaxBirthDate,
        ];
    }

    #[DataProvider('childrenHasCompatibleBirthDateProvider')]
    public function testChildrenHasCompatibleBirthDate(Cat $cat, Cat ...$children): void
    {
        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getChildren')->with(self::CAT_ID)->once()->andReturn($children);

        (new CatHasCorrectChildrenCriterion($repository))->ensureIsSatisfiedBy($cat);
    }

    public static function childrenHasIncompatibleBirthDateProvider(): iterable
    {
        $maleCat = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2000-04-10'),
        );
        $tooYoungSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2000-10-09'),
            fatherId: self::CAT_ID,
        );
        $tooYoungDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2000-10-09'),
            fatherId: self::CAT_ID,
        );
        $tooOldSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2015-04-11'),
            fatherId: self::CAT_ID,
        );
        $tooOldDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2015-04-11'),
            fatherId: self::CAT_ID,
        );
        $validSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2002-04-10'),
            fatherId: self::CAT_ID,
        );
        $validDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2002-04-10'),
            fatherId: self::CAT_ID,
        );

        yield [$maleCat, $tooYoungSon];
        yield [$maleCat, $tooYoungDaughter];
        yield [$maleCat, $tooOldSon];
        yield [$maleCat, $tooOldDaughter];
        yield [$maleCat, $tooYoungSon, $tooYoungDaughter, $tooOldSon, $tooOldDaughter];
        yield [$maleCat, $validSon, $tooYoungSon];
        yield [$maleCat, $validSon, $tooOldSon];
        yield [$maleCat, $validDaughter, $tooYoungDaughter];
        yield [$maleCat, $validDaughter, $tooOldDaughter];
        yield [
            $maleCat,
            $validSon,
            $validDaughter,
            $tooYoungSon,
            $tooYoungDaughter,
            $tooOldSon,
            $tooOldDaughter,
        ];

        $femaleCat = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2000-04-10'),
        );
        $tooYoungSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2000-12-09'),
            motherId: self::CAT_ID,
        );
        $tooYoungDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2000-12-09'),
            motherId: self::CAT_ID,
        );
        $tooOldSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2010-04-11'),
            motherId: self::CAT_ID,
        );
        $tooOldDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2010-04-11'),
            motherId: self::CAT_ID,
        );
        $validSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2002-04-10'),
            motherId: self::CAT_ID,
        );
        $validDaughter = new Cat(
            self::SIXTH_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2002-04-10'),
            motherId: self::CAT_ID,
        );

        yield [$femaleCat, $tooYoungSon];
        yield [$femaleCat, $tooYoungDaughter];
        yield [$femaleCat, $tooOldSon];
        yield [$femaleCat, $tooOldDaughter];
        yield [$femaleCat, $tooYoungSon, $tooYoungDaughter, $tooOldSon, $tooOldDaughter];
        yield [$femaleCat, $validSon, $tooYoungSon];
        yield [$femaleCat, $validSon, $tooOldSon];
        yield [$femaleCat, $validDaughter, $tooYoungDaughter];
        yield [$femaleCat, $validDaughter, $tooOldDaughter];
        yield [
            $femaleCat,
            $validSon,
            $validDaughter,
            $tooYoungSon,
            $tooYoungDaughter,
            $tooOldSon,
            $tooOldDaughter,
        ];
    }

    #[DataProvider('childrenHasIncompatibleBirthDateProvider')]
    public function testChildrenHasIncompatibleBirthDate(Cat $cat, Cat ...$children): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::CHILDREN_HAS_INCOMPATIBLE_BIRTH_DATE);

        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getChildren')->with(self::CAT_ID)->once()->andReturn($children);

        (new CatHasCorrectChildrenCriterion($repository))->ensureIsSatisfiedBy($cat);
    }

    public static function otherParentOfChildDoesntExistsProvider(): iterable
    {
        $maleCatWithoutBirthDate = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );
        $maleCatWithBirthDate = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2000-04-10'),
        );
        $sonWithoutBirthDate = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $sonWithBirthDate = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2002-04-10'),
            fatherId: self::CAT_ID,
        );
        $daughterWithoutBirthDate = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $daughterWithBirthDate = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2002-04-10'),
            fatherId: self::CAT_ID,
        );

        yield [$maleCatWithoutBirthDate, $sonWithoutBirthDate];
        yield [$maleCatWithoutBirthDate, $sonWithBirthDate];
        yield [$maleCatWithoutBirthDate, $daughterWithoutBirthDate];
        yield [$maleCatWithoutBirthDate, $daughterWithBirthDate];
        yield [
            $maleCatWithoutBirthDate,
            $sonWithoutBirthDate,
            $sonWithBirthDate,
            $daughterWithoutBirthDate,
            $daughterWithBirthDate,
        ];
        yield [$maleCatWithBirthDate, $sonWithoutBirthDate];
        yield [$maleCatWithBirthDate, $sonWithBirthDate];
        yield [$maleCatWithBirthDate, $daughterWithoutBirthDate];
        yield [$maleCatWithBirthDate, $daughterWithBirthDate];
        yield [
            $maleCatWithBirthDate,
            $sonWithoutBirthDate,
            $sonWithBirthDate,
            $daughterWithoutBirthDate,
            $daughterWithBirthDate,
        ];

        $femaleCatWithoutBirthDate = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );
        $femaleCatWithBirthDate = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2000-04-10'),
        );
        $sonWithoutBirthDate = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $sonWithBirthDate = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2002-04-10'),
            motherId: self::CAT_ID,
        );
        $daughterWithoutBirthDate = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $daughterWithBirthDate = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2002-04-10'),
            motherId: self::CAT_ID,
        );

        yield [$femaleCatWithoutBirthDate, $sonWithoutBirthDate];
        yield [$femaleCatWithoutBirthDate, $sonWithBirthDate];
        yield [$femaleCatWithoutBirthDate, $daughterWithoutBirthDate];
        yield [$femaleCatWithoutBirthDate, $daughterWithBirthDate];
        yield [
            $femaleCatWithoutBirthDate,
            $sonWithoutBirthDate,
            $sonWithBirthDate,
            $daughterWithoutBirthDate,
            $daughterWithBirthDate,
        ];
        yield [$femaleCatWithBirthDate, $sonWithoutBirthDate];
        yield [$femaleCatWithBirthDate, $sonWithBirthDate];
        yield [$femaleCatWithBirthDate, $daughterWithoutBirthDate];
        yield [$femaleCatWithBirthDate, $daughterWithBirthDate];
        yield [
            $femaleCatWithBirthDate,
            $sonWithoutBirthDate,
            $sonWithBirthDate,
            $daughterWithoutBirthDate,
            $daughterWithBirthDate,
        ];
    }

    #[DataProvider('otherParentOfChildDoesntExistsProvider')]
    public function testOtherParentOfChildDoesntExists(Cat $cat, Cat ...$children): void
    {
        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getChildren')->with(self::CAT_ID)->once()->andReturn($children);

        (new CatHasCorrectChildrenCriterion($repository))->ensureIsSatisfiedBy($cat);
    }

    public static function catWithoutBirthDateWithOtherParentOfChildExistsProvider(): iterable
    {
        $maleCatWithoutBirthDate = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );

        $mother = new Cat(
            self::FIRST_OTHER_PARENT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );

        $sonWithoutBirthDateWithMother = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::FIRST_OTHER_PARENT_ID,
        );
        $sonWithBirthDateWithMother = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2002-04-10'),
            fatherId: self::CAT_ID,
            motherId: self::FIRST_OTHER_PARENT_ID,
        );
        $daughterWithoutBirthDateWithMother = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::FIRST_OTHER_PARENT_ID,
        );
        $daughterWithBirthDateWithMother = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2002-04-10'),
            fatherId: self::CAT_ID,
            motherId: self::FIRST_OTHER_PARENT_ID,
        );

        yield [$maleCatWithoutBirthDate, [$sonWithoutBirthDateWithMother], [self::FIRST_OTHER_PARENT_ID], [$mother]];
        yield [$maleCatWithoutBirthDate, [$sonWithBirthDateWithMother], [self::FIRST_OTHER_PARENT_ID], [$mother]];
        yield [
            $maleCatWithoutBirthDate,
            [$daughterWithoutBirthDateWithMother],
            [self::FIRST_OTHER_PARENT_ID],
            [$mother],
        ];
        yield [$maleCatWithoutBirthDate, [$daughterWithBirthDateWithMother], [self::FIRST_OTHER_PARENT_ID], [$mother]];
        yield [
            $maleCatWithoutBirthDate,
            [
                $sonWithoutBirthDateWithMother,
                $sonWithBirthDateWithMother,
                $daughterWithoutBirthDateWithMother,
                $daughterWithBirthDateWithMother,
            ],
            [self::FIRST_OTHER_PARENT_ID],
            [$mother],
        ];

        $femaleCatWithoutBirthDate = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );

        $father = new Cat(
            self::FIRST_OTHER_PARENT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );

        $sonWithoutBirthDateWithMother = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::FIRST_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $sonWithBirthDateWithMother = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2002-04-10'),
            fatherId: self::FIRST_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $daughterWithoutBirthDateWithMother = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::FIRST_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $daughterWithBirthDateWithMother = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2002-04-10'),
            fatherId: self::FIRST_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );

        yield [$femaleCatWithoutBirthDate, [$sonWithoutBirthDateWithMother], [self::FIRST_OTHER_PARENT_ID], [$father]];
        yield [$femaleCatWithoutBirthDate, [$sonWithBirthDateWithMother], [self::FIRST_OTHER_PARENT_ID], [$father]];
        yield [
            $femaleCatWithoutBirthDate,
            [$daughterWithoutBirthDateWithMother],
            [self::FIRST_OTHER_PARENT_ID],
            [$father],
        ];
        yield [
            $femaleCatWithoutBirthDate,
            [$daughterWithBirthDateWithMother],
            [self::FIRST_OTHER_PARENT_ID],
            [$father],
        ];
        yield [
            $femaleCatWithoutBirthDate,
            [
                $sonWithoutBirthDateWithMother,
                $sonWithBirthDateWithMother,
                $daughterWithoutBirthDateWithMother,
                $daughterWithBirthDateWithMother,
            ],
            [self::FIRST_OTHER_PARENT_ID],
            [$father],
        ];
    }

    /**
     * @param Cat $cat
     * @param Cat[] $children
     * @param int[] $otherParentIds
     * @param Cat[] $otherParents
     */
    #[DataProvider('catWithoutBirthDateWithOtherParentOfChildExistsProvider')]
    public function testCatWithoutBirthDateWithOtherParentOfChildExists(
        Cat $cat,
        array $children,
        array $otherParentIds,
        array $otherParents,
    ): void {
        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getChildren')->with(self::CAT_ID)->once()->andReturn($children);
        $repository->shouldReceive('getCatsByIds')
            ->withArgs(fn (int ...$arg) => self::assertEqualsCanonicalizing($otherParentIds, $arg) ?? true)
            ->once()
            ->andReturn($otherParents);

        (new CatHasCorrectChildrenCriterion($repository))->ensureIsSatisfiedBy($cat);
    }


    public static function otherParentOfChildHasNotBirthDateProvider(): iterable
    {
        $maleCat = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2000-04-10'),
        );
        $childsMother = new Cat(
            self::FIRST_OTHER_PARENT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );
        $sonWithMother = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::FIRST_OTHER_PARENT_ID,
        );
        $sonWithoutMother = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $daughterWithMother = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::FIRST_OTHER_PARENT_ID,
        );
        $daughterWithoutMother = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );

        yield [$maleCat, [$sonWithMother], [self::FIRST_OTHER_PARENT_ID], [$childsMother]];
        yield [$maleCat, [$sonWithoutMother, $sonWithMother], [self::FIRST_OTHER_PARENT_ID], [$childsMother]];
        yield [$maleCat, [$daughterWithMother], [self::FIRST_OTHER_PARENT_ID], [$childsMother]];
        yield [$maleCat, [$daughterWithoutMother, $daughterWithMother], [self::FIRST_OTHER_PARENT_ID], [$childsMother]];
        yield [
            $maleCat,
            [$sonWithoutMother, $daughterWithoutMother, $sonWithMother, $daughterWithMother],
            [self::FIRST_OTHER_PARENT_ID],
            [$childsMother],
        ];

        $femaleCat = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2000-04-10'),
        );
        $childsFather = new Cat(
            self::FIRST_OTHER_PARENT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );
        $sonWithFather = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::FIRST_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $sonWithoutFather = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $daughterWithFather = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::FIRST_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $daughterWithoutFather = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );

        yield [$femaleCat, [$sonWithFather], [self::FIRST_OTHER_PARENT_ID], [$childsFather]];
        yield [$femaleCat, [$sonWithoutFather, $sonWithFather], [self::FIRST_OTHER_PARENT_ID], [$childsFather]];
        yield [$femaleCat, [$daughterWithFather], [self::FIRST_OTHER_PARENT_ID], [$childsFather]];
        yield [$femaleCat, [$daughterWithoutFather, $daughterWithFather], [self::FIRST_OTHER_PARENT_ID], [$childsFather]];
        yield [
            $femaleCat,
            [$sonWithoutFather, $daughterWithoutFather, $sonWithFather, $daughterWithFather],
            [self::FIRST_OTHER_PARENT_ID],
            [$childsFather],
        ];
    }

    /**
     * @param Cat $cat
     * @param Cat[] $children
     * @param int[] $otherParentIds
     * @param Cat[] $otherParents
     */
    #[DataProvider('otherParentOfChildHasNotBirthDateProvider')]
    public function testOtherParentOfChildHasNotBirthDate(
        Cat $cat,
        array $children,
        array $otherParentIds,
        array $otherParents,
    ): void {
        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getChildren')->with(self::CAT_ID)->once()->andReturn($children);
        $repository->shouldReceive('getCatsByIds')
            ->withArgs(fn (int ...$arg) => self::assertEqualsCanonicalizing($otherParentIds, $arg) ?? true)
            ->once()
            ->andReturn($otherParents);

        (new CatHasCorrectChildrenCriterion($repository))->ensureIsSatisfiedBy($cat);
    }

    public static function otherParentOfChildHasCompatibleBirthDateProvider(): iterable
    {
        $maleCat = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2005-10-10'),
        );

        $childsYoungerMother = new Cat(
            self::FIRST_OTHER_PARENT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2020-02-10'),
        );
        $childsOlderMother = new Cat(
            self::SECOND_OTHER_PARENT_ID,
            new CatName('Princess Murcha'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('1996-04-10'),
        );
        $childsMotherWithoutBirthDate = new Cat(
            self::THIRD_OTHER_PARENT_ID,
            new CatName('Princess Musya'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );

        $sonWithYoungerMother = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::FIRST_OTHER_PARENT_ID,
        );
        $sonWithOlderMother = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::SECOND_OTHER_PARENT_ID,
        );
        $sonWithMotherWithoutBirthDate = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Barmaley Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::THIRD_OTHER_PARENT_ID,
        );
        $sonWithoutMother = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Begemot Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $daughterWithYoungerMother = new Cat(
            self::FIFTH_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::FIRST_OTHER_PARENT_ID,
        );
        $daughterWithOlderMother = new Cat(
            self::SIXTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::SECOND_OTHER_PARENT_ID,
        );
        $daughterWithMotherWithoutBirthDate = new Cat(
            self::SEVENTH_CHILD_ID,
            new CatName('Rosa Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::THIRD_OTHER_PARENT_ID,
        );
        $daughterWithoutMother = new Cat(
            self::EIGHTH_CHILD_ID,
            new CatName('Monya Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );

        yield [$maleCat, [$sonWithYoungerMother], [self::FIRST_OTHER_PARENT_ID], [$childsYoungerMother]];
        yield [$maleCat, [$sonWithOlderMother], [self::SECOND_OTHER_PARENT_ID], [$childsOlderMother]];
        yield [
            $maleCat,
            [$sonWithMotherWithoutBirthDate],
            [self::THIRD_OTHER_PARENT_ID],
            [$childsMotherWithoutBirthDate],
        ];
        yield [$maleCat, [$daughterWithYoungerMother], [self::FIRST_OTHER_PARENT_ID], [$childsYoungerMother]];
        yield [$maleCat, [$daughterWithOlderMother], [self::SECOND_OTHER_PARENT_ID], [$childsOlderMother]];
        yield [
            $maleCat,
            [$daughterWithMotherWithoutBirthDate],
            [self::THIRD_OTHER_PARENT_ID],
            [$childsMotherWithoutBirthDate],
        ];
        yield [
            $maleCat,
            [
                $sonWithoutMother,
                $daughterWithoutMother,
                $sonWithMotherWithoutBirthDate,
                $daughterWithMotherWithoutBirthDate,
                $sonWithYoungerMother,
                $daughterWithYoungerMother,
                $sonWithOlderMother,
                $daughterWithOlderMother,
            ],
            [self::FIRST_OTHER_PARENT_ID, self::SECOND_OTHER_PARENT_ID, self::THIRD_OTHER_PARENT_ID],
            [$childsYoungerMother, $childsOlderMother, $childsMotherWithoutBirthDate],
        ];

        $femaleCat = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2005-04-10'),
        );

        $childsYoungerFather = new Cat(
            self::FIRST_OTHER_PARENT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2014-10-10'),
        );
        $childsOlderFather = new Cat(
            self::SECOND_OTHER_PARENT_ID,
            new CatName('Vasyliy Baronovich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('1990-12-10'),
        );
        $childsFatherWithoutBirthDate = new Cat(
            self::THIRD_OTHER_PARENT_ID,
            new CatName('Vasyliy Batonovich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );

        $sonWithYoungerFather = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::FIRST_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $sonWithOlderFather = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::SECOND_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $sonWithFatherWithoutBirthDate = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Barmaley Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::THIRD_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $sonWithoutFather = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Begemot Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $daughterWithYoungerFather = new Cat(
            self::FIFTH_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::FIRST_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $daughterWithOlderFather = new Cat(
            self::SIXTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::SECOND_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $daughterWithFatherWithoutBirthDate = new Cat(
            self::SEVENTH_CHILD_ID,
            new CatName('Rosa Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::THIRD_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $daughterWithoutFather = new Cat(
            self::EIGHTH_CHILD_ID,
            new CatName('Monya Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );

        yield [$femaleCat, [$sonWithYoungerFather], [self::FIRST_OTHER_PARENT_ID], [$childsYoungerFather]];
        yield [$femaleCat, [$sonWithOlderFather], [self::SECOND_OTHER_PARENT_ID], [$childsOlderFather]];
        yield [
            $femaleCat,
            [$sonWithFatherWithoutBirthDate],
            [self::THIRD_OTHER_PARENT_ID],
            [$childsFatherWithoutBirthDate],
        ];
        yield [$femaleCat, [$daughterWithYoungerFather], [self::FIRST_OTHER_PARENT_ID], [$childsYoungerFather]];
        yield [$femaleCat, [$daughterWithOlderFather], [self::SECOND_OTHER_PARENT_ID], [$childsOlderFather]];
        yield [
            $femaleCat,
            [$daughterWithFatherWithoutBirthDate],
            [self::THIRD_OTHER_PARENT_ID],
            [$childsFatherWithoutBirthDate],
        ];
        yield [
            $femaleCat,
            [
                $sonWithoutFather,
                $daughterWithoutFather,
                $sonWithFatherWithoutBirthDate,
                $daughterWithFatherWithoutBirthDate,
                $sonWithYoungerFather,
                $daughterWithYoungerFather,
                $sonWithOlderFather,
                $daughterWithOlderFather,
            ],
            [self::FIRST_OTHER_PARENT_ID, self::SECOND_OTHER_PARENT_ID, self::THIRD_OTHER_PARENT_ID],
            [$childsYoungerFather, $childsOlderFather, $childsFatherWithoutBirthDate],
        ];
    }

    /**
     * @param Cat $cat
     * @param Cat[] $children
     * @param int[] $otherParentIds
     * @param Cat[] $otherParents
     */
    #[DataProvider('otherParentOfChildHasCompatibleBirthDateProvider')]
    public function testOtherParentOfChildHasCompatibleBirthDate(
        Cat $cat,
        array $children,
        array $otherParentIds,
        array $otherParents,
    ): void {
        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getChildren')->with(self::CAT_ID)->once()->andReturn($children);
        $repository->shouldReceive('getCatsByIds')
            ->withArgs(fn (int ...$arg) => self::assertEqualsCanonicalizing($otherParentIds, $arg) ?? true)
            ->once()
            ->andReturn($otherParents);

        (new CatHasCorrectChildrenCriterion($repository))->ensureIsSatisfiedBy($cat);
    }

    public static function otherParentOfChildHasIncompatibleBirthDateProvider(): iterable
    {
        $maleCat = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2005-10-10'),
        );

        $childsTooYoungMother = new Cat(
            self::FIRST_OTHER_PARENT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2020-02-11'),
        );
        $childsTooOldMother = new Cat(
            self::SECOND_OTHER_PARENT_ID,
            new CatName('Princess Murcha'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('1996-04-09'),
        );
        $childsMotherWithoutBirthDate = new Cat(
            self::THIRD_OTHER_PARENT_ID,
            new CatName('Princess Musya'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );

        $sonWithTooYoungMother = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::FIRST_OTHER_PARENT_ID,
        );
        $sonWithTooOldMother = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::SECOND_OTHER_PARENT_ID,
        );
        $sonWithMotherWithoutBirthDate = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Barmaley Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::THIRD_OTHER_PARENT_ID,
        );
        $sonWithoutMother = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Begemot Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $daughterWithTooYoungMother = new Cat(
            self::FIFTH_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::FIRST_OTHER_PARENT_ID,
        );
        $daughterWithTooOldMother = new Cat(
            self::SIXTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::SECOND_OTHER_PARENT_ID,
        );
        $daughterWithMotherWithoutBirthDate = new Cat(
            self::SEVENTH_CHILD_ID,
            new CatName('Rosa Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::THIRD_OTHER_PARENT_ID,
        );
        $daughterWithoutMother = new Cat(
            self::EIGHTH_CHILD_ID,
            new CatName('Monya Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );

        yield [$maleCat, [$sonWithTooYoungMother], [self::FIRST_OTHER_PARENT_ID], [$childsTooYoungMother]];
        yield [$maleCat, [$sonWithTooOldMother], [self::SECOND_OTHER_PARENT_ID], [$childsTooOldMother]];
        yield [
            $maleCat,
            [$sonWithMotherWithoutBirthDate, $sonWithTooYoungMother],
            [self::THIRD_OTHER_PARENT_ID, self::FIRST_OTHER_PARENT_ID],
            [$childsMotherWithoutBirthDate, $childsTooYoungMother],
        ];
        yield [
            $maleCat,
            [$sonWithoutMother, $sonWithTooOldMother],
            [self::SECOND_OTHER_PARENT_ID],
            [$childsTooOldMother],
        ];
        yield [$maleCat, [$daughterWithTooYoungMother], [self::FIRST_OTHER_PARENT_ID], [$childsTooYoungMother]];
        yield [$maleCat, [$daughterWithTooOldMother], [self::SECOND_OTHER_PARENT_ID], [$childsTooOldMother]];
        yield [
            $maleCat,
            [$daughterWithMotherWithoutBirthDate, $daughterWithTooYoungMother],
            [self::THIRD_OTHER_PARENT_ID, self::FIRST_OTHER_PARENT_ID],
            [$childsMotherWithoutBirthDate, $childsTooYoungMother],
        ];
        yield [
            $maleCat,
            [$daughterWithoutMother, $daughterWithTooOldMother],
            [self::SECOND_OTHER_PARENT_ID],
            [$childsTooOldMother],
        ];
        yield [
            $maleCat,
            [
                $sonWithoutMother,
                $daughterWithoutMother,
                $sonWithMotherWithoutBirthDate,
                $daughterWithMotherWithoutBirthDate,
                $sonWithTooYoungMother,
                $daughterWithTooYoungMother,
                $sonWithTooOldMother,
                $daughterWithTooOldMother,
            ],
            [self::FIRST_OTHER_PARENT_ID, self::SECOND_OTHER_PARENT_ID, self::THIRD_OTHER_PARENT_ID],
            [$childsTooOldMother, $childsTooYoungMother, $childsMotherWithoutBirthDate],
        ];

        $femaleCat = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2005-04-10'),
        );

        $childsTooYoungFather= new Cat(
            self::FIRST_OTHER_PARENT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2014-10-11'),
        );
        $childsTooOldFather = new Cat(
            self::SECOND_OTHER_PARENT_ID,
            new CatName('Vasyliy Baronovich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('1990-12-09'),
        );
        $childsFatherWithoutBirthDate = new Cat(
            self::THIRD_OTHER_PARENT_ID,
            new CatName('Vasyliy Batonovich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );

        $sonWithTooYoungFather = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::FIRST_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $sonWithTooOldFather = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::SECOND_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $sonWithFatherWithoutBirthDate = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Barmaley Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: self::THIRD_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $sonWithoutFather = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Begemot Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $daughterWithTooYoungFather = new Cat(
            self::FIFTH_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::FIRST_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $daughterWithTooOldFather = new Cat(
            self::SIXTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::SECOND_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $daughterWithFatherWithoutBirthDate = new Cat(
            self::SEVENTH_CHILD_ID,
            new CatName('Rosa Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            fatherId: self::THIRD_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $daughterWithoutFather = new Cat(
            self::EIGHTH_CHILD_ID,
            new CatName('Monya Vasylivna'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );

        yield [$femaleCat, [$sonWithTooYoungFather], [self::FIRST_OTHER_PARENT_ID], [$childsTooYoungFather]];
        yield [$femaleCat, [$sonWithTooOldFather], [self::SECOND_OTHER_PARENT_ID], [$childsTooOldFather]];
        yield [
            $femaleCat,
            [$sonWithFatherWithoutBirthDate, $sonWithTooYoungFather],
            [self::THIRD_OTHER_PARENT_ID, self::FIRST_OTHER_PARENT_ID],
            [$childsFatherWithoutBirthDate, $childsTooYoungFather],
        ];
        yield [
            $femaleCat,
            [$sonWithoutFather, $sonWithTooOldFather],
            [self::SECOND_OTHER_PARENT_ID],
            [$childsTooOldFather],
        ];
        yield [$femaleCat, [$daughterWithTooYoungFather], [self::FIRST_OTHER_PARENT_ID], [$childsTooYoungFather]];
        yield [$femaleCat, [$daughterWithTooOldFather], [self::SECOND_OTHER_PARENT_ID], [$childsTooOldFather]];
        yield [
            $femaleCat,
            [$daughterWithFatherWithoutBirthDate, $daughterWithTooYoungFather],
            [self::THIRD_OTHER_PARENT_ID, self::FIRST_OTHER_PARENT_ID],
            [$childsFatherWithoutBirthDate, $childsTooYoungFather],
        ];
        yield [
            $femaleCat,
            [$daughterWithoutFather, $daughterWithTooOldFather],
            [self::SECOND_OTHER_PARENT_ID],
            [$childsTooOldFather],
        ];
        yield [
            $femaleCat,
            [
                $sonWithoutFather,
                $daughterWithoutFather,
                $sonWithFatherWithoutBirthDate,
                $daughterWithFatherWithoutBirthDate,
                $sonWithTooYoungFather,
                $daughterWithTooYoungFather,
                $sonWithTooOldFather,
                $daughterWithTooOldFather,
            ],
            [self::FIRST_OTHER_PARENT_ID, self::SECOND_OTHER_PARENT_ID, self::THIRD_OTHER_PARENT_ID],
            [$childsTooOldFather, $childsTooYoungFather, $childsFatherWithoutBirthDate],
        ];
    }

    /**
     * @param Cat $cat
     * @param Cat[] $children
     * @param int[] $otherParentIds
     * @param Cat[] $otherParents
     */
    #[DataProvider('otherParentOfChildHasIncompatibleBirthDateProvider')]
    public function testOtherParentOfChildHasIncompatibleBirthDate(
        Cat $cat,
        array $children,
        array $otherParentIds,
        array $otherParents,
    ): void {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::OTHER_PARENT_OF_CHILD_HAS_INCOMPATIBLE_BIRTH_DATE);

        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getChildren')->with(self::CAT_ID)->once()->andReturn($children);
        $repository->shouldReceive('getCatsByIds')
            ->withArgs(fn (int ...$arg) => self::assertEqualsCanonicalizing($otherParentIds, $arg) ?? true)
            ->once()
            ->andReturn($otherParents);

        (new CatHasCorrectChildrenCriterion($repository))->ensureIsSatisfiedBy($cat);
    }

    public static function childrenWithoutOtherParentHaveTheSameBreedProvider(): iterable
    {
        foreach (Breed::cases() as $breed) {
            $maleCat = new Cat(
                self::CAT_ID,
                new CatName('Vasyliy Alibabaevich'),
                Genetics::fromCharacteristics($breed, Sex::MALE, Color::BLACK),
            );

            $son = new Cat(
                self::FIRST_CHILD_ID,
                new CatName('Murzik Vasyliovych'),
                Genetics::fromCharacteristics($breed, Sex::MALE, Color::BLACK),
                fatherId: self::CAT_ID,
            );
            $daughter = new Cat(
                self::SECOND_CHILD_ID,
                new CatName('Murka Vasylivna'),
                Genetics::fromCharacteristics($breed, Sex::FEMALE, Color::BLACK),
                fatherId: self::CAT_ID,
            );

            yield [$maleCat, $son];
            yield [$maleCat, $daughter];
            yield [$maleCat, $son, $daughter];

            $femaleCat = new Cat(
                self::CAT_ID,
                new CatName('Princess Murka'),
                Genetics::fromCharacteristics($breed, Sex::FEMALE, Color::BLACK),
            );

            $son = new Cat(
                self::FIRST_CHILD_ID,
                new CatName('Murzik Vasyliovych'),
                Genetics::fromCharacteristics($breed, Sex::MALE, Color::BLACK),
                motherId: self::CAT_ID,
            );
            $daughter = new Cat(
                self::SECOND_CHILD_ID,
                new CatName('Murka Vasylivna'),
                Genetics::fromCharacteristics($breed, Sex::FEMALE, Color::BLACK),
                motherId: self::CAT_ID,
            );
            yield [$femaleCat, $son];
            yield [$femaleCat, $daughter];
            yield [$femaleCat, $son, $daughter];
        }
    }

    public static function shortHairOrientalHasPebChildrenWithoutOtherParentProvider(): iterable
    {
        foreach (Breed::shortHairOrientalCases() as $shortHairOrientalBreed) {
            $maleCat = new Cat(
                self::CAT_ID,
                new CatName('Vasyliy Alibabaevich'),
                Genetics::fromCharacteristics($shortHairOrientalBreed, Sex::MALE, Color::BLACK),
            );

            $pebSon = new Cat(
                self::FIRST_CHILD_ID,
                new CatName('Murzik Vasyliovych'),
                Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
                fatherId: self::CAT_ID,
            );
            $pebDaughter = new Cat(
                self::SECOND_CHILD_ID,
                new CatName('Murka Vasylivna'),
                Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
                fatherId: self::CAT_ID,
            );
            $shortHairOrientalSon = new Cat(
                self::THIRD_CHILD_ID,
                new CatName('Zhorik Vasyliovych'),
                Genetics::fromCharacteristics($shortHairOrientalBreed, Sex::MALE, Color::BLACK),
                fatherId: self::CAT_ID,
            );
            $shortHairOrientalDaughter = new Cat(
                self::FOURTH_CHILD_ID,
                new CatName('Nika Vasylivna'),
                Genetics::fromCharacteristics($shortHairOrientalBreed, Sex::FEMALE, Color::BLACK),
                fatherId: self::CAT_ID,
            );

            yield [$maleCat, $pebSon];
            yield [$maleCat, $pebDaughter];
            yield [$maleCat, $shortHairOrientalSon, $shortHairOrientalDaughter, $pebSon, $pebDaughter];

            $femaleCat = new Cat(
                self::CAT_ID,
                new CatName('Princess Murka'),
                Genetics::fromCharacteristics($shortHairOrientalBreed, Sex::FEMALE, Color::BLACK),
            );

            $pebSon = new Cat(
                self::FIRST_CHILD_ID,
                new CatName('Murzik Vasyliovych'),
                Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
                motherId: self::CAT_ID,
            );
            $pebDaughter = new Cat(
                self::SECOND_CHILD_ID,
                new CatName('Murka Vasylivna'),
                Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
                motherId: self::CAT_ID,
            );
            $shortHairOrientalSon = new Cat(
                self::THIRD_CHILD_ID,
                new CatName('Zhorik Vasyliovych'),
                Genetics::fromCharacteristics($shortHairOrientalBreed, Sex::MALE, Color::BLACK),
                motherId: self::CAT_ID,
            );
            $shortHairOrientalDaughter = new Cat(
                self::FOURTH_CHILD_ID,
                new CatName('Nika Vasylivna'),
                Genetics::fromCharacteristics($shortHairOrientalBreed, Sex::FEMALE, Color::BLACK),
                motherId: self::CAT_ID,
            );

            yield [$femaleCat, $pebSon];
            yield [$femaleCat, $pebDaughter];
            yield [
                $femaleCat,
                $shortHairOrientalSon,
                $shortHairOrientalDaughter,
                $pebSon,
                $pebDaughter,
            ];
        }
    }

    public static function siaHasOshChildrenWithoutOtherParentProvider(): iterable
    {
        $maleSiaCat = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::SIA, Sex::MALE, Color::BLACK),
        );

        $oshSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::OSH, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $oshDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::OSH, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $siaSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::SIA, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $siaDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::SIA, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );

        yield [$maleSiaCat, $oshSon];
        yield [$maleSiaCat, $oshDaughter];
        yield [$maleSiaCat, $siaSon, $siaDaughter, $oshSon, $oshDaughter];

        $femaleSiaCat = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::SIA, Sex::FEMALE, Color::BLACK),
        );

        $oshSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::OSH, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $oshDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::OSH, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $siaSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::SIA, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $siaDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::SIA, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );

        yield [$femaleSiaCat, $oshSon];
        yield [$femaleSiaCat, $oshDaughter];
        yield [$femaleSiaCat, $siaSon, $siaDaughter, $oshSon, $oshDaughter];
    }

    public static function oshHasSiaChildrenWithoutOtherParentProvider(): iterable
    {
        $maleOshCat = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::OSH, Sex::MALE, Color::BLACK),
        );

        $siaSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::SIA, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $siaDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::SIA, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $oshSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::OSH, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $oshDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::OSH, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );

        yield [$maleOshCat, $siaSon];
        yield [$maleOshCat, $siaDaughter];
        yield [$maleOshCat, $oshSon, $oshDaughter, $siaSon, $siaDaughter];

        $femaleOshCat = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::OSH, Sex::FEMALE, Color::BLACK),
        );

        $siaSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::SIA, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $siaDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::SIA, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $oshSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::OSH, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $oshDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::OSH, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );

        yield [$femaleOshCat, $siaSon];
        yield [$femaleOshCat, $siaDaughter];
        yield [$femaleOshCat, $oshSon, $oshDaughter, $siaSon, $siaDaughter];
    }

    public static function shortHairOrientalHasLongHairOrientalChildrenWithoutOtherParentProvider(): iterable
    {
        foreach (Breed::shortHairOrientalCases() as $shortHairOrientalBreed) {
            foreach (Breed::longHairOrientalCases() as $longHairOrientalBreed) {
                $shortHairOrientalMaleCat = new Cat(
                    self::CAT_ID,
                    new CatName('Vasyliy Alibabaevich'),
                    Genetics::fromCharacteristics($shortHairOrientalBreed, Sex::MALE, Color::BLACK),
                );

                $longHairOrientalSon = new Cat(
                    self::FIRST_CHILD_ID,
                    new CatName('Murzik Vasyliovych'),
                    Genetics::fromCharacteristics($longHairOrientalBreed, Sex::MALE, Color::BLACK),
                    fatherId: self::CAT_ID,
                );
                $longHairOrientalDaughter = new Cat(
                    self::SECOND_CHILD_ID,
                    new CatName('Murka Vasylivna'),
                    Genetics::fromCharacteristics($longHairOrientalBreed, Sex::FEMALE, Color::BLACK),
                    fatherId: self::CAT_ID,
                );
                $shortHairOrientalSon = new Cat(
                    self::THIRD_CHILD_ID,
                    new CatName('Zhorik Vasyliovych'),
                    Genetics::fromCharacteristics($shortHairOrientalBreed, Sex::MALE, Color::BLACK),
                    fatherId: self::CAT_ID,
                );
                $shortHairOrientalDaughter = new Cat(
                    self::FOURTH_CHILD_ID,
                    new CatName('Nika Vasylivna'),
                    Genetics::fromCharacteristics($shortHairOrientalBreed, Sex::FEMALE, Color::BLACK),
                    fatherId: self::CAT_ID,
                );

                yield [$shortHairOrientalMaleCat, $longHairOrientalSon];
                yield [$shortHairOrientalMaleCat, $longHairOrientalDaughter];
                yield [
                    $shortHairOrientalMaleCat,
                    $shortHairOrientalSon,
                    $shortHairOrientalDaughter,
                    $longHairOrientalSon,
                    $longHairOrientalDaughter,
                ];

                $shortHairOrientalFemaleCat = new Cat(
                    self::CAT_ID,
                    new CatName('Princess Murka'),
                    Genetics::fromCharacteristics($shortHairOrientalBreed, Sex::FEMALE, Color::BLACK),
                );

                $longHairOrientalSon = new Cat(
                    self::FIRST_CHILD_ID,
                    new CatName('Murzik Vasyliovych'),
                    Genetics::fromCharacteristics($longHairOrientalBreed, Sex::MALE, Color::BLACK),
                    motherId: self::CAT_ID,
                );
                $longHairOrientalDaughter = new Cat(
                    self::SECOND_CHILD_ID,
                    new CatName('Murka Vasylivna'),
                    Genetics::fromCharacteristics($longHairOrientalBreed, Sex::FEMALE, Color::BLACK),
                    motherId: self::CAT_ID,
                );
                $shortHairOrientalSon = new Cat(
                    self::THIRD_CHILD_ID,
                    new CatName('Zhorik Vasyliovych'),
                    Genetics::fromCharacteristics($shortHairOrientalBreed, Sex::MALE, Color::BLACK),
                    motherId: self::CAT_ID,
                );
                $shortHairOrientalDaughter = new Cat(
                    self::FOURTH_CHILD_ID,
                    new CatName('Nika Vasylivna'),
                    Genetics::fromCharacteristics($shortHairOrientalBreed, Sex::FEMALE, Color::BLACK),
                    motherId: self::CAT_ID,
                );

                yield [$shortHairOrientalFemaleCat, $longHairOrientalSon];
                yield [$shortHairOrientalFemaleCat, $longHairOrientalDaughter];
                yield [
                    $shortHairOrientalFemaleCat,
                    $shortHairOrientalSon,
                    $shortHairOrientalDaughter,
                    $longHairOrientalSon,
                    $longHairOrientalDaughter,
                ];
            }
        }
    }

    public static function balHasOlhChildrenWithoutOtherParentProvider(): iterable
    {
        $maleBalCat = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::BAL, Sex::MALE, Color::BLACK),
        );

        $olhSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::OLH, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $olhDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::OLH, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $balSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::BAL, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $balDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::BAL, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );

        yield [$maleBalCat, $olhSon];
        yield [$maleBalCat, $olhDaughter];
        yield [$maleBalCat, $balSon, $balDaughter, $olhSon, $olhDaughter];

        $femaleBalCat = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::BAL, Sex::FEMALE, Color::BLACK),
        );

        $olhSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::OLH, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $olhDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::OLH, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $balSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::BAL, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $balDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::BAL, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );

        yield [$femaleBalCat, $olhSon];
        yield [$femaleBalCat, $olhDaughter];
        yield [$femaleBalCat, $balSon, $balDaughter, $olhSon, $olhDaughter];
    }

    public static function olhHasBalChildrenWithoutOtherParentProvider(): iterable
    {
        $maleOlhCat = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::OLH, Sex::MALE, Color::BLACK),
        );

        $balSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::BAL, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $balDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::BAL, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $olhSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::OLH, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $olhDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::OLH, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );

        yield [$maleOlhCat, $balSon];
        yield [$maleOlhCat, $balDaughter];
        yield [$maleOlhCat, $olhSon, $olhDaughter, $balSon, $balDaughter];

        $femaleOlhCat = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::OLH, Sex::FEMALE, Color::BLACK),
        );

        $balSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::BAL, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $balDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics(Breed::BAL, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $olhSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::OLH, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $olhDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics(Breed::OLH, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );

        yield [$femaleOlhCat, $balSon];
        yield [$femaleOlhCat, $balDaughter];
        yield [$femaleOlhCat, $olhSon, $olhDaughter, $balSon, $balDaughter];
    }

    #[DataProvider('childrenWithoutOtherParentHaveTheSameBreedProvider')]
    #[DataProvider('shortHairOrientalHasPebChildrenWithoutOtherParentProvider')]
    #[DataProvider('siaHasOshChildrenWithoutOtherParentProvider')]
    #[DataProvider('oshHasSiaChildrenWithoutOtherParentProvider')]
    #[DataProvider('shortHairOrientalHasLongHairOrientalChildrenWithoutOtherParentProvider')]
    #[DataProvider('balHasOlhChildrenWithoutOtherParentProvider')]
    #[DataProvider('olhHasBalChildrenWithoutOtherParentProvider')]
    public function testChildrenWithoutOtherParentHaveCompatibleBreeds(Cat $cat, Cat ...$children): void
    {
        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getChildren')->with(self::CAT_ID)->once()->andReturn($children);

        (new CatHasCorrectChildrenCriterion($repository))->ensureIsSatisfiedBy($cat);
    }

    public static function childrenWithOtherParentHaveCompatibleBreedsProvider(): iterable
    {
        yield from self::parentsAndChildCompleteBreedsCases(Breed::PEB, Breed::PEB, Breed::PEB, Breed::PEB, Breed::SIA);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::PEB, Breed::PEB, Breed::SIA, Breed::PEB, Breed::PEB);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::PEB, Breed::PEB, Breed::OSH, Breed::PEB, Breed::PEB);

        yield from self::parentsAndChildCompleteBreedsCases(Breed::SIA, Breed::PEB, Breed::PEB, Breed::SIA, Breed::SIA);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::SIA, Breed::SIA, Breed::SIA, Breed::OSH, Breed::OSH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::SIA, Breed::SIA, Breed::OSH, Breed::OSH, Breed::OSH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::SIA, Breed::OSH, Breed::OSH, Breed::SIA, Breed::SIA);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::SIA, Breed::BAL, Breed::BAL, Breed::SIA, Breed::SIA);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::SIA, Breed::BAL, Breed::OLH, Breed::SIA, Breed::SIA);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::SIA, Breed::OLH, Breed::OLH, Breed::SIA, Breed::SIA);

        yield from self::parentsAndChildCompleteBreedsCases(Breed::OSH, Breed::PEB, Breed::PEB, Breed::OSH, Breed::OSH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::OSH, Breed::SIA, Breed::SIA, Breed::OSH, Breed::OSH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::OSH, Breed::SIA, Breed::OSH, Breed::OSH, Breed::SIA);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::OSH, Breed::OSH, Breed::SIA, Breed::SIA, Breed::OSH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::OSH, Breed::OSH, Breed::OSH, Breed::SIA, Breed::SIA);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::OSH, Breed::BAL, Breed::BAL, Breed::OSH, Breed::OSH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::OSH, Breed::BAL, Breed::OLH, Breed::OSH, Breed::OSH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::OSH, Breed::OLH, Breed::BAL, Breed::OSH, Breed::OSH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::OSH, Breed::OLH, Breed::OLH, Breed::OSH, Breed::OSH);

        yield from self::parentsAndChildCompleteBreedsCases(Breed::BAL, Breed::BAL, Breed::SIA, Breed::BAL, Breed::BAL);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::BAL, Breed::BAL, Breed::OLH, Breed::BAL, Breed::BAL);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::BAL, Breed::BAL, Breed::BAL, Breed::OLH, Breed::OLH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::BAL, Breed::BAL, Breed::OLH, Breed::OLH, Breed::OLH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::BAL, Breed::OLH, Breed::OSH, Breed::BAL, Breed::BAL);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::BAL, Breed::OLH, Breed::OLH, Breed::BAL, Breed::BAL);

        yield from self::parentsAndChildCompleteBreedsCases(Breed::OLH, Breed::BAL, Breed::SIA, Breed::OLH, Breed::OLH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::OLH, Breed::BAL, Breed::OSH, Breed::OLH, Breed::OLH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::OLH, Breed::BAL, Breed::BAL, Breed::OLH, Breed::OLH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::OLH, Breed::BAL, Breed::OLH, Breed::OLH, Breed::BAL);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::OLH, Breed::OLH, Breed::SIA, Breed::OLH, Breed::OLH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::OLH, Breed::OLH, Breed::OSH, Breed::OLH, Breed::OLH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::OLH, Breed::OLH, Breed::BAL, Breed::BAL, Breed::OLH);
        yield from self::parentsAndChildCompleteBreedsCases(Breed::OLH, Breed::OLH, Breed::OLH, Breed::BAL, Breed::BAL);
    }

    private static function parentsAndChildCompleteBreedsCases(
        Breed $mainCatBreed,
        Breed $mainChildBreed,
        Breed $mainOtherParentBreed,
        Breed $extraChildBreed,
        Breed $extraOtherParentBreed,
    ): iterable {
        $mainMaleCat = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics($mainCatBreed, Sex::MALE, Color::BLACK),
        );

        $mainMother = new Cat(
            self::FIRST_OTHER_PARENT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics($mainOtherParentBreed, Sex::FEMALE, Color::BLACK),
        );
        $extraMother = new Cat(
            self::SECOND_OTHER_PARENT_ID,
            new CatName('Princess Murcha'),
            Genetics::fromCharacteristics($extraOtherParentBreed, Sex::FEMALE, Color::BLACK),
        );

        $mainSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics($mainChildBreed, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::FIRST_OTHER_PARENT_ID,
        );
        $mainDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics($mainChildBreed, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::FIRST_OTHER_PARENT_ID,
        );
        $extraSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics($extraChildBreed, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::SECOND_OTHER_PARENT_ID,
        );
        $extraDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics($extraChildBreed, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::SECOND_OTHER_PARENT_ID,
        );
        $sonWithoutMother = new Cat(
            self::FIFTH_CHILD_ID,
            new CatName('Barmaley Vasyliovych'),
            Genetics::fromCharacteristics($mainChildBreed, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $daughterWithoutMother = new Cat(
            self::SIXTH_CHILD_ID,
            new CatName('Rosa Vasylivna'),
            Genetics::fromCharacteristics($mainChildBreed, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );

        yield [$mainMaleCat, [$mainSon], [self::FIRST_OTHER_PARENT_ID], [$mainMother]];
        yield [$mainMaleCat, [$mainDaughter], [self::FIRST_OTHER_PARENT_ID], [$mainMother]];
        yield [
            $mainMaleCat,
            [$sonWithoutMother, $daughterWithoutMother, $extraSon, $extraDaughter, $mainSon, $mainDaughter],
            [self::FIRST_OTHER_PARENT_ID, self::SECOND_OTHER_PARENT_ID],
            [$extraMother, $mainMother],
        ];

        $mainFemaleCat = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics($mainCatBreed, Sex::FEMALE, Color::BLACK),
        );

        $mainFather = new Cat(
            self::FIRST_OTHER_PARENT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics($mainOtherParentBreed, Sex::MALE, Color::BLACK),
        );

        $extraFather = new Cat(
            self::SECOND_OTHER_PARENT_ID,
            new CatName('Vasyliy Baronovich'),
            Genetics::fromCharacteristics($extraOtherParentBreed, Sex::MALE, Color::BLACK),
        );

        $mainSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics($mainChildBreed, Sex::MALE, Color::BLACK),
            fatherId: self::FIRST_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $mainDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics($mainChildBreed, Sex::FEMALE, Color::BLACK),
            fatherId: self::FIRST_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $extraSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics($extraChildBreed, Sex::MALE, Color::BLACK),
            fatherId: self::SECOND_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $extraDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics($extraChildBreed, Sex::FEMALE, Color::BLACK),
            fatherId: self::SECOND_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $sonWithoutFather = new Cat(
            self::FIFTH_CHILD_ID,
            new CatName('Barmaley Vasyliovych'),
            Genetics::fromCharacteristics($mainChildBreed, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $daughterWithoutFather = new Cat(
            self::SIXTH_CHILD_ID,
            new CatName('Rosa Vasylivna'),
            Genetics::fromCharacteristics($mainChildBreed, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );

        yield [$mainFemaleCat, [$mainSon], [self::FIRST_OTHER_PARENT_ID], [$mainFather]];
        yield [$mainFemaleCat, [$mainDaughter], [self::FIRST_OTHER_PARENT_ID], [$mainFather]];
        yield [
            $mainFemaleCat,
            [$sonWithoutFather, $daughterWithoutFather, $extraSon, $extraDaughter, $mainSon, $mainDaughter],
            [self::FIRST_OTHER_PARENT_ID, self::SECOND_OTHER_PARENT_ID],
            [$extraFather, $mainFather],
        ];
    }

    /**
     * @param Cat $cat
     * @param Cat[] $children
     * @param int[] $otherParentIds
     * @param Cat[] $otherParents
     */
    #[DataProvider('childrenWithOtherParentHaveCompatibleBreedsProvider')]
    public function testChildrenWithOtherParentHaveCompatibleBreeds(
        Cat $cat,
        array $children,
        array $otherParentIds,
        array $otherParents,
    ): void {
        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getChildren')->with(self::CAT_ID)->once()->andReturn($children);
        $repository->shouldReceive('getCatsByIds')
            ->withArgs(fn (int ...$arg) => self::assertEqualsCanonicalizing($otherParentIds, $arg) ?? true)
            ->once()
            ->andReturn($otherParents);

        (new CatHasCorrectChildrenCriterion($repository))->ensureIsSatisfiedBy($cat);
    }

    public static function childWithoutOtherParentHaveIncompatibleBreedProvider(): iterable
    {
        foreach (Breed::OrientalCases() as $orientalBreed) {
            yield from self::childWithoutOtherParentHasIncompleteBreedCases(Breed::PEB, $orientalBreed);
        }

        foreach (Breed::longHairOrientalCases() as $longHairOrientalBreed) {
            yield from self::childWithoutOtherParentHasIncompleteBreedCases($longHairOrientalBreed, Breed::PEB);

            foreach (Breed::shortHairOrientalCases() as $shortHairOrientalBreed) {
                yield from self::childWithoutOtherParentHasIncompleteBreedCases(
                    $longHairOrientalBreed,
                    $shortHairOrientalBreed,
                );
            }
        }
    }

    private static function childWithoutOtherParentHasIncompleteBreedCases(
        Breed $catBreed,
        Breed $incompatibleChildBreed,
    ): iterable {
        $maleCat = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics($catBreed, Sex::MALE, Color::BLACK),
        );

        $incompatibleSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics($incompatibleChildBreed, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $incompatibleDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics($incompatibleChildBreed, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $compatibleSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics($catBreed, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $compatibleDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics($catBreed, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );

        yield [$maleCat, $incompatibleSon];
        yield [$maleCat, $incompatibleDaughter];
        yield [$maleCat, $compatibleSon, $compatibleDaughter, $incompatibleSon];
        yield [$maleCat, $compatibleSon, $compatibleDaughter, $incompatibleDaughter];

        $femaleCat = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics($catBreed, Sex::FEMALE, Color::BLACK),
        );

        $incompatibleSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics($incompatibleChildBreed, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $incompatibleDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics($incompatibleChildBreed, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $compatibleSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics($catBreed, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $compatibleDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics($catBreed, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );

        yield [$femaleCat, $incompatibleSon];
        yield [$femaleCat, $incompatibleDaughter];
        yield [$femaleCat, $compatibleSon, $compatibleDaughter, $incompatibleSon];
        yield [$femaleCat, $compatibleSon, $compatibleDaughter, $incompatibleDaughter];
    }

    #[DataProvider('childWithoutOtherParentHaveIncompatibleBreedProvider')]
    public function testChildWithoutOtherParentHaveIncompatibleBreed(Cat $cat, Cat ...$children): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::CHILD_HAS_INCOMPATIBLE_BREED);

        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getChildren')->with(self::CAT_ID)->once()->andReturn($children);

        (new CatHasCorrectChildrenCriterion($repository))->ensureIsSatisfiedBy($cat);
    }

    public static function childAndOtherParentHasIncompatibleBreedsProvider(): iterable
    {
        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::SIA, Breed::PEB, Breed::SIA);
        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::SIA, Breed::PEB, Breed::OSH);
        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::SIA, Breed::OSH, Breed::SIA);
        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::SIA, Breed::BAL, Breed::SIA);
        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::SIA, Breed::BAL, Breed::OSH);
        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::SIA, Breed::OLH, Breed::SIA);
        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::SIA, Breed::OLH, Breed::OSH);
        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::SIA, Breed::OLH, Breed::BAL);

        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::OSH, Breed::PEB, Breed::SIA);
        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::OSH, Breed::PEB, Breed::OSH);
        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::OSH, Breed::BAL, Breed::SIA);
        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::OSH, Breed::BAL, Breed::OSH);
        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::OSH, Breed::OLH, Breed::SIA);
        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::OSH, Breed::OLH, Breed::OSH);

        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::BAL, Breed::OLH, Breed::SIA);
        yield from self::childAndOtherParentHasIncompatibleBreedsCases(Breed::BAL, Breed::OLH, Breed::BAL);
    }

    private static function childAndOtherParentHasIncompatibleBreedsCases(
        Breed $catBreed,
        Breed $childBreed,
        Breed $otherParentBreed,
    ): iterable {
        $mainMaleCat = new Cat(
            self::CAT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics($catBreed, Sex::MALE, Color::BLACK),
        );

        $mainMother = new Cat(
            self::FIRST_OTHER_PARENT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics($otherParentBreed, Sex::FEMALE, Color::BLACK),
        );
        $extraMother = new Cat(
            self::SECOND_OTHER_PARENT_ID,
            new CatName('Princess Murcha'),
            Genetics::fromCharacteristics($catBreed, Sex::FEMALE, Color::BLACK),
        );

        $mainSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics($childBreed, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::FIRST_OTHER_PARENT_ID,
        );
        $mainDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics($childBreed, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::FIRST_OTHER_PARENT_ID,
        );
        $extraSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics($catBreed, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::SECOND_OTHER_PARENT_ID,
        );
        $extraDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics($catBreed, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
            motherId: self::SECOND_OTHER_PARENT_ID,
        );
        $sonWithoutMother = new Cat(
            self::FIFTH_CHILD_ID,
            new CatName('Barmaley Vasyliovych'),
            Genetics::fromCharacteristics($catBreed, Sex::MALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );
        $daughterWithoutMother = new Cat(
            self::SIXTH_CHILD_ID,
            new CatName('Rosa Vasylivna'),
            Genetics::fromCharacteristics($catBreed, Sex::FEMALE, Color::BLACK),
            fatherId: self::CAT_ID,
        );

        yield [$mainMaleCat, [$mainSon], [self::FIRST_OTHER_PARENT_ID], [$mainMother]];
        yield [$mainMaleCat, [$mainDaughter], [self::FIRST_OTHER_PARENT_ID], [$mainMother]];
        yield [
            $mainMaleCat,
            [$sonWithoutMother, $daughterWithoutMother, $extraSon, $extraDaughter, $mainSon],
            [self::FIRST_OTHER_PARENT_ID, self::SECOND_OTHER_PARENT_ID],
            [$extraMother, $mainMother],
        ];
        yield [
            $mainMaleCat,
            [$sonWithoutMother, $daughterWithoutMother, $extraSon, $extraDaughter, $mainDaughter],
            [self::FIRST_OTHER_PARENT_ID, self::SECOND_OTHER_PARENT_ID],
            [$extraMother, $mainMother],
        ];

        $mainFemaleCat = new Cat(
            self::CAT_ID,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics($catBreed, Sex::FEMALE, Color::BLACK),
        );

        $mainFather = new Cat(
            self::FIRST_OTHER_PARENT_ID,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics($otherParentBreed, Sex::MALE, Color::BLACK),
        );

        $extraFather = new Cat(
            self::SECOND_OTHER_PARENT_ID,
            new CatName('Vasyliy Baronovich'),
            Genetics::fromCharacteristics($catBreed, Sex::MALE, Color::BLACK),
        );

        $mainSon = new Cat(
            self::FIRST_CHILD_ID,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics($childBreed, Sex::MALE, Color::BLACK),
            fatherId: self::FIRST_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $mainDaughter = new Cat(
            self::SECOND_CHILD_ID,
            new CatName('Murka Vasylivna'),
            Genetics::fromCharacteristics($childBreed, Sex::FEMALE, Color::BLACK),
            fatherId: self::FIRST_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $extraSon = new Cat(
            self::THIRD_CHILD_ID,
            new CatName('Zhorik Vasyliovych'),
            Genetics::fromCharacteristics($catBreed, Sex::MALE, Color::BLACK),
            fatherId: self::SECOND_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $extraDaughter = new Cat(
            self::FOURTH_CHILD_ID,
            new CatName('Nika Vasylivna'),
            Genetics::fromCharacteristics($catBreed, Sex::FEMALE, Color::BLACK),
            fatherId: self::SECOND_OTHER_PARENT_ID,
            motherId: self::CAT_ID,
        );
        $sonWithoutFather = new Cat(
            self::FIFTH_CHILD_ID,
            new CatName('Barmaley Vasyliovych'),
            Genetics::fromCharacteristics($catBreed, Sex::MALE, Color::BLACK),
            motherId: self::CAT_ID,
        );
        $daughterWithoutFather = new Cat(
            self::SIXTH_CHILD_ID,
            new CatName('Rosa Vasylivna'),
            Genetics::fromCharacteristics($catBreed, Sex::FEMALE, Color::BLACK),
            motherId: self::CAT_ID,
        );

        yield [$mainFemaleCat, [$mainSon], [self::FIRST_OTHER_PARENT_ID], [$mainFather]];
        yield [$mainFemaleCat, [$mainDaughter], [self::FIRST_OTHER_PARENT_ID], [$mainFather]];
        yield [
            $mainFemaleCat,
            [$sonWithoutFather, $daughterWithoutFather, $extraSon, $extraDaughter, $mainSon],
            [self::FIRST_OTHER_PARENT_ID, self::SECOND_OTHER_PARENT_ID],
            [$extraFather, $mainFather],
        ];
        yield [
            $mainFemaleCat,
            [$sonWithoutFather, $daughterWithoutFather, $extraSon, $extraDaughter, $mainDaughter],
            [self::FIRST_OTHER_PARENT_ID, self::SECOND_OTHER_PARENT_ID],
            [$extraFather, $mainFather],
        ];
    }

    /**
     * @param Cat $cat
     * @param Cat[] $children
     * @param int[] $otherParentIds
     * @param Cat[] $otherParents
     */
    #[DataProvider('childAndOtherParentHasIncompatibleBreedsProvider')]
    public function testChildAndOtherParentHasIncompatibleBreeds(
        Cat $cat,
        array $children,
        array $otherParentIds,
        array $otherParents,
    ): void {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::CHILD_AND_OTHER_PARENT_HAS_INCOMPATIBLE_BREEDS);

        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getChildren')->with(self::CAT_ID)->once()->andReturn($children);
        $repository->shouldReceive('getCatsByIds')
            ->withArgs(fn (int ...$arg) => self::assertEqualsCanonicalizing($otherParentIds, $arg) ?? true)
            ->once()
            ->andReturn($otherParents);

        (new CatHasCorrectChildrenCriterion($repository))->ensureIsSatisfiedBy($cat);
    }
}
