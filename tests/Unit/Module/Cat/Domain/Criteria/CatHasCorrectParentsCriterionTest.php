<?php

declare(strict_types=1);

namespace App\Tests\Unit\Module\Cat\Domain\Criteria;

use App\Module\Cat\Domain\Criteria\CatHasCorrectParentsCriterion;
use App\Module\Cat\Domain\DependencyInterface\CatRepositoryInterface;
use App\Module\Cat\Domain\Entity\Cat;
use App\Module\Cat\Domain\Exception\CatValidationException;
use App\Module\Cat\Domain\ValueObject\CatName;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Breed;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Color;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Sex;
use App\Module\Cat\Domain\ValueObject\Genetics;
use DateTimeImmutable;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\DoesNotPerformAssertions;
use PHPUnit\Framework\TestCase;

final class CatHasCorrectParentsCriterionTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    #[DoesNotPerformAssertions]
    public function testWithoutParents(): void
    {
        $cat = new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );

        $criterion = new CatHasCorrectParentsCriterion(self::createMock(CatRepositoryInterface::class));
        $criterion->ensureIsSatisfiedBy($cat);
    }

    public static function parentDoesNotExistProvider(): iterable
    {
        $father = new Cat(
            2,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );
        $mother = new Cat(
            3,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );

        return [
            [2, null, []],
            [null, 3, []],
            [2, 3, [$father]],
            [2, 3, [$mother]],
            [2, 3, []],
        ];
    }

    #[DataProvider('parentDoesNotExistProvider')]
    public function testParentDoesNotExist(?int $fatherId, ?int $motherId, array $parents): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::PARENT_DOES_NOT_EXIST);

        $cat = new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: $fatherId,
            motherId: $motherId,
        );

        $repository = self::createStub(CatRepositoryInterface::class);
        $repository->method('getCatsByIds')->willReturn($parents);

        $criterion = new CatHasCorrectParentsCriterion($repository);
        $criterion->ensureIsSatisfiedBy($cat);

    }

    public static function parentHasCorrectSexProvider(): iterable
    {
        $father = new Cat(
            2,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );
        $mother = new Cat(
            3,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );

        return [
            [2, null, [2], [$father]],
            [null, 3, [3], [$mother]],
            [2, 3, [2, 3], [$father, $mother]],
            [2, 3, [3, 2], [$father, $mother]],
        ];
    }

    #[DataProvider('parentHasCorrectSexProvider')]
    public function testParentHasCorrectSex(?int $fatherId, ?int $motherId, array $ids, array $parents): void
    {
        $cat = new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: $fatherId,
            motherId: $motherId,
        );

        $repository = Mockery::mock(CatRepositoryInterface::class);
        $repository->shouldReceive('getCatsByIds')
            ->withArgs(fn (int ...$arg) => self::assertEqualsCanonicalizing($ids, $arg) ?? true)
            ->once()
            ->andReturn($parents);

        $criterion = new CatHasCorrectParentsCriterion($repository);
        $criterion->ensureIsSatisfiedBy($cat);
    }

    public static function parentHasIncorrectSexProvider(): iterable
    {
        $okFather = new Cat(
            2,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );
        $okMother = new Cat(
            3,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );
        $badFather = new Cat(
            2,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );
        $badMother = new Cat(
            3,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );

        return [
            [2, null, [$badFather]],
            [2, 3, [$badFather, $okMother]],
            [null, 3, [$badMother]],
            [2, 3, [$badMother, $okFather]],
            [2, 3, [$badFather, $badMother]],
        ];
    }

    #[DataProvider('parentHasIncorrectSexProvider')]
    public function testParentHasIncorrectSex(?int $fatherId, ?int $motherId, array $parents): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::PARENT_HAS_INCORRECT_SEX);

        $cat = new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            fatherId: $fatherId,
            motherId: $motherId,
        );

        $repository = self::createStub(CatRepositoryInterface::class);
        $repository->method('getCatsByIds')->willReturn($parents);

        $criterion = new CatHasCorrectParentsCriterion($repository);
        $criterion->ensureIsSatisfiedBy($cat);
    }

    public static function parentHasCorrectBirthDateOrCatHasNoBirthDateProvider(): iterable
    {
        $fatherWithoutDate = new Cat(
            2,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );
        $motherWithoutDate = new Cat(
            3,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );
        $fatherYoungerThanMother = new Cat(
            2,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2020-10-10'),
        );
        $motherOlderThanFather = new Cat(
            3,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2011-04-10'),
        );
        $fatherOlderThanMother = new Cat(
            2,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2005-10-10'),
        );
        $motherYoungerThanFather = new Cat(
            3,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2020-02-10'),
        );
        $oldFather = new Cat(
            2,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2005-10-10'),
        );
        $oldMother = new Cat(
            3,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2010-10-10'),
        );
        $youngFather = new Cat(
            2,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2020-04-10'),
        );
        $youngMother = new Cat(
            3,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2020-02-10'),
        );

        return [
            [null, 2, null, [$fatherWithoutDate]],
            [null, null, 3, [$motherWithoutDate]],
            [null, 2, 3, [$fatherWithoutDate, $motherWithoutDate]],
            [null, 2, 3, [$fatherYoungerThanMother, $motherOlderThanFather]],
            [null, 2, 3, [$fatherOlderThanMother, $motherYoungerThanFather]],
            [new DateTimeImmutable('2020-10-10'), 2, null, [$fatherWithoutDate]],
            [new DateTimeImmutable('2020-10-10'), null, 3, [$motherWithoutDate]],
            [new DateTimeImmutable('2020-10-10'), 2, 3, [$fatherWithoutDate, $motherWithoutDate]],
            [new DateTimeImmutable('2020-10-10'), 2, null, [$oldFather]],
            [new DateTimeImmutable('2020-10-10'), null, 3, [$oldMother]],
            [new DateTimeImmutable('2020-10-10'), 2, 3, [$oldFather, $oldMother]],
            [new DateTimeImmutable('2020-10-10'), 2, null, [$youngFather]],
            [new DateTimeImmutable('2020-10-10'), null, 3, [$youngMother]],
            [new DateTimeImmutable('2020-10-10'), 2, 3, [$youngFather, $youngMother]],
        ];
    }

    #[DataProvider('parentHasCorrectBirthDateOrCatHasNoBirthDateProvider')]
    #[DoesNotPerformAssertions]
    public function testParentHasCorrectBirthDateOrCatHasNoBirthDate(
        ?DateTimeImmutable $catBirthDate,
        ?int $fatherId,
        ?int $motherId,
        array $parents,
    ): void {
        $cat = new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            $catBirthDate,
            fatherId: $fatherId,
            motherId: $motherId,
        );

        $repository = self::createStub(CatRepositoryInterface::class);
        $repository->method('getCatsByIds')->willReturn($parents);

        $criterion = new CatHasCorrectParentsCriterion($repository);
        $criterion->ensureIsSatisfiedBy($cat);
    }

    public static function parentHasIncompatibleBirthDateProvider(): iterable
    {
        $fatherTooYoungerThanMother = new Cat(
            2,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2020-10-10'),
        );
        $motherTooOlderThanFather = new Cat(
            3,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2011-04-09'),
        );
        $fatherTooOlderThanMother = new Cat(
            2,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2005-10-10'),
        );
        $motherTooYoungerThanFather = new Cat(
            3,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2020-02-11'),
        );
        $tooOldFather = new Cat(
            2,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2005-10-09'),
        );
        $tooOldMother = new Cat(
            3,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2010-10-09'),
        );
        $tooYoungFather = new Cat(
            2,
            new CatName('Vasyliy Alibabaevich'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2020-04-11'),
        );
        $tooYoungMother = new Cat(
            3,
            new CatName('Princess Murka'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            new DateTimeImmutable('2020-02-11'),
        );

        return [
            [null, 2, 3, [$fatherTooYoungerThanMother, $motherTooOlderThanFather]],
            [null, 2, 3, [$fatherTooOlderThanMother, $motherTooYoungerThanFather]],
            [new DateTimeImmutable('2020-10-10'), 2, null, [$tooOldFather]],
            [new DateTimeImmutable('2020-10-10'), null, 3, [$tooOldMother]],
            [new DateTimeImmutable('2020-10-10'), 2, 3, [$tooOldFather, $tooOldMother]],
            [new DateTimeImmutable('2020-10-10'), 2, null, [$tooYoungFather]],
            [new DateTimeImmutable('2020-10-10'), null, 3, [$tooYoungMother]],
            [new DateTimeImmutable('2020-10-10'), 2, 3, [$tooYoungFather, $tooYoungMother]],
        ];
    }

    #[DataProvider('parentHasIncompatibleBirthDateProvider')]
    public function testParentHasIncompatibleBirthDate(
        ?DateTimeImmutable $catBirthDate,
        ?int $fatherId,
        ?int $motherId,
        array $parents,
    ): void {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::PARENT_HAS_INCOMPATIBLE_BIRTH_DATE);

        $cat = new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            $catBirthDate,
            fatherId: $fatherId,
            motherId: $motherId,
        );

        $repository = self::createStub(CatRepositoryInterface::class);
        $repository->method('getCatsByIds')->willReturn($parents);

        $criterion = new CatHasCorrectParentsCriterion($repository);
        $criterion->ensureIsSatisfiedBy($cat);
    }

    public static function parentHasCompatibleBreedProvider(): iterable
    {
        foreach ([Breed::PEB, Breed::SIA, Breed::OSH] as $breed) {
            $father = new Cat(
                2,
                new CatName('Vasyliy Alibabaevich'),
                Genetics::fromCharacteristics($breed, Sex::MALE, Color::BLACK),
            );
            $mother = new Cat(
                3,
                new CatName('Princess Murka'),
                Genetics::fromCharacteristics($breed, Sex::FEMALE, Color::BLACK),
            );
            $pebFather = new Cat(
                2,
                new CatName('Vasyliy Alibabaevich'),
                Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            );
            $pebMother = new Cat(
                3,
                new CatName('Princess Murka'),
                Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
            );

            yield [Breed::PEB, 2, null, [$father]];
            yield [Breed::PEB, null, 3, [$mother]];
            yield [Breed::PEB, 2, 3, [$pebFather, $mother]];
            yield [Breed::PEB, 2, 3, [$father, $pebMother]];
        }

        foreach (Breed::shortHairOrientalCases() as $childBreed) {
            foreach (Breed::shortHairOrientalCases() as $firstParentBreed) {
                $firstParentFather = new Cat(
                    2,
                    new CatName('Vasyliy Alibabaevich'),
                    Genetics::fromCharacteristics($firstParentBreed, Sex::MALE, Color::BLACK),
                );
                $firstParentMother = new Cat(
                    3,
                    new CatName('Princess Murka'),
                    Genetics::fromCharacteristics($firstParentBreed, Sex::FEMALE, Color::BLACK),
                );

                yield [$childBreed, 2, null, [$firstParentFather]];
                yield [$childBreed, null, 3, [$firstParentMother]];

                foreach (Breed::shortHairOrientalCases() as $secondParentBreed) {
                    if (Breed::OSH === $childBreed &&
                        Breed::SIA === $firstParentBreed &&
                        Breed::SIA === $secondParentBreed
                    ) {
                        continue;
                    }

                    $secondParentFather = new Cat(
                        2,
                        new CatName('Vasyliy Alibabaevich'),
                        Genetics::fromCharacteristics($secondParentBreed, Sex::MALE, Color::BLACK),
                    );
                    $secondParentMother = new Cat(
                        3,
                        new CatName('Princess Murka'),
                        Genetics::fromCharacteristics($secondParentBreed, Sex::FEMALE, Color::BLACK),
                    );

                    yield [$childBreed, 2, 3, [$firstParentFather, $secondParentMother]];
                    yield [$childBreed, 2, 3, [$secondParentFather, $firstParentMother]];
                }
            }
        }

        foreach (Breed::longHairOrientalCases() as $childBreed) {
            foreach (Breed::OrientalCases() as $firstParentBreed) {
                $firstParentFather = new Cat(
                    2,
                    new CatName('Vasyliy Alibabaevich'),
                    Genetics::fromCharacteristics($firstParentBreed, Sex::MALE, Color::BLACK),
                );
                $firstParentMother = new Cat(
                    3,
                    new CatName('Princess Murka'),
                    Genetics::fromCharacteristics($firstParentBreed, Sex::FEMALE, Color::BLACK),
                );

                yield [$childBreed, 2, null, [$firstParentFather]];
                yield [$childBreed, null, 3, [$firstParentMother]];

                foreach (Breed::longHairOrientalCases() as $secondParentBreed) {
                    if (
                        Breed::OLH === $childBreed &&
                        $firstParentBreed->isBalOrSia() &&
                        $secondParentBreed->isBalOrSia()
                    ) {
                        continue;
                    }

                    $secondParentFather = new Cat(
                        2,
                        new CatName('Vasyliy Alibabaevich'),
                        Genetics::fromCharacteristics($secondParentBreed, Sex::MALE, Color::BLACK),
                    );
                    $secondParentMother = new Cat(
                        3,
                        new CatName('Princess Murka'),
                        Genetics::fromCharacteristics($secondParentBreed, Sex::FEMALE, Color::BLACK),
                    );

                    yield [$childBreed, 2, 3, [$firstParentFather, $secondParentMother]];
                    yield [$childBreed, 2, 3, [$secondParentFather, $firstParentMother]];
                }
            }
        }
    }

    #[DataProvider('parentHasCompatibleBreedProvider')]
    #[DoesNotPerformAssertions]
    public function testParentHasCompatibleBreed(
        Breed $breed,
        ?int $fatherId,
        ?int $motherId,
        array $parents,
    ): void {
        $cat = new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics($breed, Sex::MALE, Color::BLACK),
            fatherId: $fatherId,
            motherId: $motherId,
        );

        $repository = self::createStub(CatRepositoryInterface::class);
        $repository->method('getCatsByIds')->willReturn($parents);

        $criterion = new CatHasCorrectParentsCriterion($repository);
        $criterion->ensureIsSatisfiedBy($cat);
    }

    public static function parentHasIncompatibleBreedProvider(): iterable
    {
        foreach (Breed::cases() as $firstParentBreed) {
            $firstParentFather = new Cat(
                2,
                new CatName('Vasyliy Alibabaevich'),
                Genetics::fromCharacteristics($firstParentBreed, Sex::MALE, Color::BLACK),
            );
            $firstParentMother = new Cat(
                3,
                new CatName('Princess Murka'),
                Genetics::fromCharacteristics($firstParentBreed, Sex::FEMALE, Color::BLACK),
            );

            if (Breed::PEB !== $firstParentBreed && !$firstParentBreed->isShortHairOriental()) {
                yield [Breed::PEB, 2, null, [$firstParentFather]];
                yield [Breed::PEB, null, 3, [$firstParentMother]];
            }

            foreach (Breed::cases() as $secondParentBreed) {
                if (
                    Breed::PEB === $firstParentBreed &&
                    (Breed::PEB === $secondParentBreed || $secondParentBreed->isShortHairOriental())
                ) {
                    continue;
                }

                if (
                    Breed::PEB === $secondParentBreed &&
                    (Breed::PEB === $firstParentBreed || $firstParentBreed->isShortHairOriental())
                ) {
                    continue;
                }

                $secondParentFather = new Cat(
                    2,
                    new CatName('Vasyliy Alibabaevich'),
                    Genetics::fromCharacteristics($secondParentBreed, Sex::MALE, Color::BLACK),
                );
                $secondParentMother = new Cat(
                    3,
                    new CatName('Princess Murka'),
                    Genetics::fromCharacteristics($secondParentBreed, Sex::FEMALE, Color::BLACK),
                );

                yield [Breed::PEB, 2, 3, [$firstParentFather, $secondParentMother]];
                yield [Breed::PEB, 2, 3, [$secondParentFather, $firstParentMother]];
            }
        }

        foreach (Breed::shortHairOrientalCases() as $childBreed) {
            foreach (Breed::cases() as $firstParentBreed) {
                $firstParentFather = new Cat(
                    2,
                    new CatName('Vasyliy Alibabaevich'),
                    Genetics::fromCharacteristics($firstParentBreed, Sex::MALE, Color::BLACK),
                );
                $firstParentMother = new Cat(
                    3,
                    new CatName('Princess Murka'),
                    Genetics::fromCharacteristics($firstParentBreed, Sex::FEMALE, Color::BLACK),
                );

                if (!$firstParentBreed->isShortHairOriental()) {
                    yield [$childBreed, 2, null, [$firstParentFather]];
                    yield [$childBreed, null, 3, [$firstParentMother]];
                }

                foreach (Breed::cases() as $secondParentBreed) {
                    if (
                        $firstParentBreed->isShortHairOriental() &&
                        $secondParentBreed->isShortHairOriental() &&
                        (
                            Breed::SIA === $childBreed ||
                            Breed::SIA !== $firstParentBreed ||
                            Breed::SIA !== $secondParentBreed
                        )
                    ) {
                        continue;
                    }

                    $secondParentFather = new Cat(
                        2,
                        new CatName('Vasyliy Alibabaevich'),
                        Genetics::fromCharacteristics($secondParentBreed, Sex::MALE, Color::BLACK),
                    );
                    $secondParentMother = new Cat(
                        3,
                        new CatName('Princess Murka'),
                        Genetics::fromCharacteristics($secondParentBreed, Sex::FEMALE, Color::BLACK),
                    );

                    yield [$childBreed, 2, 3, [$firstParentFather, $secondParentMother]];
                    yield [$childBreed, 2, 3, [$secondParentFather, $firstParentMother]];
                }
            }
        }

        foreach (Breed::longHairOrientalCases() as $childBreed) {
            foreach (Breed::cases() as $firstParentBreed) {
                $firstParentFather = new Cat(
                    2,
                    new CatName('Vasyliy Alibabaevich'),
                    Genetics::fromCharacteristics($firstParentBreed, Sex::MALE, Color::BLACK),
                );
                $firstParentMother = new Cat(
                    3,
                    new CatName('Princess Murka'),
                    Genetics::fromCharacteristics($firstParentBreed, Sex::FEMALE, Color::BLACK),
                );

                if (!$firstParentBreed->isOriental()) {
                    yield [$childBreed, 2, null, [$firstParentFather]];
                    yield [$childBreed, null, 3, [$firstParentMother]];
                }

                foreach (Breed::cases() as $secondParentBreed) {
                    if (
                        Breed::BAL === $childBreed &&
                        (
                            ($firstParentBreed->isLongHairOriental() && $secondParentBreed->isOriental()) ||
                            ($secondParentBreed->isLongHairOriental() && $firstParentBreed->isOriental())
                        )
                    ) {
                        continue;
                    }

                    if (
                        Breed::OLH === $childBreed &&
                        (
                            (Breed::OLH === $firstParentBreed && $secondParentBreed->isOriental()) ||
                            (Breed::OLH === $secondParentBreed && $firstParentBreed->isOriental())
                        )
                    ) {
                        continue;
                    }

                    if (
                        Breed::OLH === $childBreed &&
                        (
                            (Breed::BAL === $firstParentBreed && $secondParentBreed->isOlhOrOsh()) ||
                            (Breed::BAL === $secondParentBreed && $firstParentBreed->isOlhOrOsh())
                        )
                    ) {
                        continue;
                    }

                    $secondParentFather = new Cat(
                        2,
                        new CatName('Vasyliy Alibabaevich'),
                        Genetics::fromCharacteristics($secondParentBreed, Sex::MALE, Color::BLACK),
                    );
                    $secondParentMother = new Cat(
                        3,
                        new CatName('Princess Murka'),
                        Genetics::fromCharacteristics($secondParentBreed, Sex::FEMALE, Color::BLACK),
                    );

                    yield [$childBreed, 2, 3, [$firstParentFather, $secondParentMother]];
                    yield [$childBreed, 2, 3, [$secondParentFather, $firstParentMother]];
                }
            }
        }
    }

    #[DataProvider('parentHasIncompatibleBreedProvider')]
    public function testParentHasIncompatibleBreed(
        Breed $breed,
        ?int $fatherId,
        ?int $motherId,
        array $parents,
    ): void {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::PARENT_HAS_INCOMPATIBLE_BREED);

        $cat = new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics($breed, Sex::MALE, Color::BLACK),
            fatherId: $fatherId,
            motherId: $motherId,
        );

        $repository = self::createStub(CatRepositoryInterface::class);
        $repository->method('getCatsByIds')->willReturn($parents);

        $criterion = new CatHasCorrectParentsCriterion($repository);
        $criterion->ensureIsSatisfiedBy($cat);
    }
}
