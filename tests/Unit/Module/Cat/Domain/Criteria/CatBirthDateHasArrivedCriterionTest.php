<?php

declare(strict_types=1);

namespace App\Tests\Unit\Module\Cat\Domain\Criteria;

use App\Module\Cat\Domain\Criteria\CatBirthDateHasArrivedCriterion;
use App\Module\Cat\Domain\Entity\Cat;
use App\Module\Cat\Domain\Exception\CatValidationException;
use App\Module\Cat\Domain\ValueObject\CatName;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Breed;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Color;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Sex;
use App\Module\Cat\Domain\ValueObject\Genetics;
use DateTimeImmutable;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\DoesNotPerformAssertions;
use PHPUnit\Framework\TestCase;

final class CatBirthDateHasArrivedCriterionTest extends TestCase
{
    public static function satisfiedProvider(): iterable
    {
        return [
            [null],
            [new DateTimeImmutable('1800-01-01')],
            [new DateTimeImmutable('2020-01-01')],
            [new DateTimeImmutable('2020-01-02')],
        ];
    }

    #[DataProvider('satisfiedProvider')]
    #[DoesNotPerformAssertions]
    public function testSatisfied(?DateTimeImmutable $birthDate): void
    {
        $cat = new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            $birthDate,
        );

        $catBirthDateCriterion = new CatBirthDateHasArrivedCriterion(new DateTimeImmutable('2020-01-02'));
        $catBirthDateCriterion->ensureIsSatisfiedBy($cat);
    }

    public function testNotSatisfied(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::BIRTH_DATE_HAS_NOT_ARRIVED_ERR);

        $cat = new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('2020-01-03'),
        );

        $catBirthDateCriterion = new CatBirthDateHasArrivedCriterion(new DateTimeImmutable('2020-01-02'));
        $catBirthDateCriterion->ensureIsSatisfiedBy($cat);
    }
}
