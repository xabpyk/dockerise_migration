<?php

declare(strict_types=1);

namespace App\Tests\Unit\Module\Cat\Domain\Criteria;

use App\Module\Cat\Domain\Criteria\UniqueCatNameCriterion;
use App\Module\Cat\Domain\DependencyInterface\CatRepositoryInterface;
use App\Module\Cat\Domain\Entity\Cat;
use App\Module\Cat\Domain\Exception\CatValidationException;
use App\Module\Cat\Domain\ValueObject\CatName;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Breed;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Color;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Sex;
use App\Module\Cat\Domain\ValueObject\Genetics;
use PHPUnit\Framework\TestCase;

final class UniqueCatNameCriterionTest extends TestCase
{
    public function testSatisfied(): void
    {
        $cat = new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );

        $repository = self::createMock(CatRepositoryInterface::class);
        $repository->expects($this->once())
            ->method('catWithSameNameExists')
            ->with($cat)
            ->willReturn(false);

        $criterion = new UniqueCatNameCriterion($repository);

        $criterion->ensureIsSatisfiedBy($cat);
    }

    public function testNotSatisfied(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::NAME_IS_NOT_UNIQUE_ERR);

        $cat = new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );

        $repository = self::createMock(CatRepositoryInterface::class);
        $repository->expects($this->once())
            ->method('catWithSameNameExists')
            ->with($cat)
            ->willReturn(true);

        $criterion = new UniqueCatNameCriterion($repository);
        $criterion->ensureIsSatisfiedBy($cat);
    }
}
