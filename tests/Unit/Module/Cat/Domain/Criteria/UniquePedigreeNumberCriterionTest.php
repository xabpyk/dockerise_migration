<?php

declare(strict_types=1);

namespace App\Tests\Unit\Module\Cat\Domain\Criteria;

use App\Module\Cat\Domain\Criteria\UniquePedigreeNumberCriterion;
use App\Module\Cat\Domain\DependencyInterface\CatRepositoryInterface;
use App\Module\Cat\Domain\Entity\Cat;
use App\Module\Cat\Domain\Exception\CatValidationException;
use App\Module\Cat\Domain\ValueObject\CatName;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Breed;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Color;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Sex;
use App\Module\Cat\Domain\ValueObject\Genetics;
use PHPUnit\Framework\TestCase;

final class UniquePedigreeNumberCriterionTest extends TestCase
{
    public function testSatisfied(): void
    {
        $cat = new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );

        $repository = self::createMock(CatRepositoryInterface::class);

        $repository->expects($this->once())
            ->method('catWithSamePedigreeNumberExists')
            ->with($cat)
            ->willReturn(false);

        $criterion = new UniquePedigreeNumberCriterion($repository);

        $criterion->ensureIsSatisfiedBy($cat);
    }

    public function testNotSatisfied(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::PEDIGREE_NUMBER_IS_NOT_UNIQUE_ERR);

        $cat = new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
        );

        $repository = self::createMock(CatRepositoryInterface::class);

        $repository->expects($this->once())
            ->method('catWithSamePedigreeNumberExists')
            ->with($cat)
            ->willReturn(true);

        $criterion = new UniquePedigreeNumberCriterion($repository);

        $criterion->ensureIsSatisfiedBy($cat);
    }
}
