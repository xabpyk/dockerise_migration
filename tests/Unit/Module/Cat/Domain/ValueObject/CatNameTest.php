<?php

declare(strict_types=1);

namespace App\Tests\Unit\Module\Cat\Domain\ValueObject;

use App\Module\Cat\Domain\Exception\CatValidationException;
use App\Module\Cat\Domain\ValueObject\CatName;
use PHPUnit\Framework\TestCase;

final class CatNameTest extends TestCase
{
    public function testValidName(): void
    {
        $catName = new CatName('abc abc abc');

        self::assertSame('abc abc abc', $catName->name);
    }

    public function testEmptyName(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::NAME_MIN_LENGTH_ERR);

        new CatName('');
    }

    public function testShortName(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::NAME_MIN_LENGTH_ERR);

        new CatName('123 56');
    }

    public function testMinimumName(): void
    {
        $catName = new CatName('123 567');

        self::assertSame('123 567', $catName->name);
    }

    public function testMaximumName(): void
    {
        $catName = new CatName('123456789 123456789 123456789 123456789 12345');

        self::assertSame('123456789 123456789 123456789 123456789 12345', $catName->name);
    }

    public function testLongName(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::NAME_MAX_LENGTH_ERR);

        new CatName('123456789 123456789 123456789 123456789 123456');
    }

    public function testNameStartedWithSpace(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::NAME_STARTED_OR_ENDED_WITH_SPACE_ERR);

        new CatName(' 123456789');
    }

    public function testNameEndedWithSpace(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::NAME_STARTED_OR_ENDED_WITH_SPACE_ERR);

        new CatName('123456789 ');
    }

    public function testNameWithoutSpace(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::NAME_DOES_NOT_CONTAIN_SPACE_ERR);

        new CatName('123456789');
    }

    public function testNameWithSpaceInFirstThreeCharacters(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::NAME_SPACE_IN_FIRST_FEW_CHARACTERS_ERR);

        new CatName('12 456789');
    }
}
