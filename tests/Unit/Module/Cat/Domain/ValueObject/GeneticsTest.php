<?php

declare(strict_types=1);

namespace App\Tests\Unit\Module\Cat\Domain\ValueObject;

use App\Module\Cat\Domain\Exception\CatValidationException;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Breed;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Color;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\EyeColor;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Pattern;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\PRA;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Sex;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\SilverSmoke;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\WhiteAmount;
use App\Module\Cat\Domain\ValueObject\Genetics;
use App\Tests\Unit\Module\Cat\Domain\GeneticsEqualsTrait;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

final class GeneticsTest extends TestCase
{
    use GeneticsEqualsTrait;

    public function testMin(): void
    {
        self::assertGeneticsEquals(
            'breed:PEB|sex:f|whAmt:|W:w,w|O:o,o|B:B,|D:D,|Dm:,|C:C,|A:a,a|I:i,i|S:s,s|T:,|eye:|pra:,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLACK),
        );
    }

    public function testMax(): void
    {
        $genetics = Genetics::fromCharacteristics(
            Breed::PEB,
            Sex::FEMALE,
            Color::LILAC,
            true,
            true,
            SilverSmoke::SILVER,
            WhiteAmount::HARLEQUIN,
            Pattern::UNSPECIFIED_TABBY,
            EyeColor::ODD,
            PRA::NM,
        );

        self::assertGeneticsEquals(
            'breed:PEB|sex:f|whAmt:HARLEQUIN|W:w,w|O:o,o|B:b,|D:d,d|Dm:Dm,|C:cs,cs|A:A,|I:I,|S:S,|T:,|eye:ODD|pra:N,M',
            $genetics
        );
    }

    public static function breedProvider(): iterable
    {
        return [
            ['breed:BAL', Breed::BAL],
            ['breed:OLH', Breed::OLH],
            ['breed:OSH', Breed::OSH],
            ['breed:PEB', Breed::PEB],
            ['breed:SIA', Breed::SIA],
        ];
    }

    #[DataProvider('breedProvider')]
    public function testBreed(string $expected, Breed $breed): void
    {
        self::assertGeneticsEquals(
            $expected,
            Genetics::fromCharacteristics($breed, Sex::FEMALE, COLOR::BLACK),
        );
    }

    public static function whiteAmountProvider(): iterable
    {
        return [
            ['whAmt:', null],
            ['whAmt:VAN', WhiteAmount::VAN],
            ['whAmt:HARLEQUIN', WhiteAmount::HARLEQUIN],
            ['whAmt:BICOLOUR', WhiteAmount::BICOLOUR],
            ['whAmt:UNSPECIFIED', WhiteAmount::UNSPECIFIED],
        ];
    }

    #[DataProvider('whiteAmountProvider')]
    public function testWhiteAmount(string $expected, ?WhiteAmount $whiteAmount): void
    {
        self::assertGeneticsEquals(
            $expected,
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, COLOR::BLACK, whiteAmount: $whiteAmount),
        );
    }

    public static function sexProvider(): iterable
    {
        yield ['sex:f', Sex::FEMALE];
        yield ['sex:m', Sex::MALE];
    }

    #[DataProvider('sexProvider')]
    public function testSex(string $expected, Sex $sex): void
    {
        self::assertGeneticsEquals(
            $expected,
            Genetics::fromCharacteristics(Breed::PEB, $sex, COLOR::BLACK),
        );
    }

    public function testWhiteColorMaleOrangeAlleles(): void
    {
        self::assertGeneticsEquals(
            'O:,Y',
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::WHITE, eyeColor: EyeColor::GREEN),
        );

    }

    public static function maleOrangeAllelesHaveBigOColors(): iterable
    {
        yield [Color::CREAM];
        yield [Color::RED];
    }


    #[DataProvider('maleOrangeAllelesHaveBigOColors')]
    public function testMaleOrangeAllelesHaveBigO(Color $color): void
    {
        self::assertGeneticsEquals(
            'O:O,Y',
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, $color),
        );

    }

    public static function maleOrangeAllelesDontHaveBigOColors(): iterable
    {
        return [
            [Color::BLACK],
            [Color::BLUE],
            [Color::CHOCOLATE],
            [Color::CINNAMON_SORREL],
            [Color::FAWN],
            [Color::LILAC],
        ];
    }

    #[DataProvider('maleOrangeAllelesDontHaveBigOColors')]
    public function testMaleOrangeAllelesDontHaveBigO(Color $color): void
    {
        self::assertGeneticsEquals(
            'O:o,Y',
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, $color),
        );

    }

    public function testWhiteFemaleOrangeAlleles(): void
    {
        self::assertGeneticsEquals(
            'O:,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::WHITE, eyeColor: EyeColor::GREEN),
        );
    }

    public static function femaleOrangeAllelesHaveBigOBigOColors(): iterable
    {
        yield [Color::RED];
        yield [Color::CREAM];
    }

    #[DataProvider('femaleOrangeAllelesHaveBigOBigOColors')]
    public function testFemaleOrangeAllelesHaveBigOBigO(Color $color): void
    {
        self::assertGeneticsEquals(
            'O:O,O',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color),
        );

    }

    public static function tortieColorsProvider(): iterable
    {
        return [
            [Color::BLACK_TORTIE],
            [Color::BLUE_TORTIE],
            [Color::CHOCOLATE_TORTIE],
            [Color::CINNAMON_SORREL_TORTIE],
            [Color::FAWN_TORTIE],
            [Color::LILAC_TORTIE],
        ];
    }

    #[DataProvider('tortieColorsProvider')]
    public function testFemaleOrangeAllelesHaveBigOSmallO(Color $color): void
    {
        self::assertGeneticsEquals(
            'O:O,o',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color),
        );

    }

    public static function femaleOrangeAllelesDontHaveBigOColors(): iterable
    {
        return [
            [Color::BLACK],
            [Color::BLUE],
            [Color::CHOCOLATE],
            [Color::CINNAMON_SORREL],
            [Color::FAWN],
            [Color::LILAC],
        ];
    }

    #[DataProvider('femaleOrangeAllelesDontHaveBigOColors')]
    public function testFemaleOrangeAllelesDontHaveBigO(Color $color): void
    {
        self::assertGeneticsEquals(
            'O:o,o',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color),
        );

    }

    public function testWhiteAllelesHaveBigW(): void
    {
        self::assertGeneticsEquals(
            'W:W,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::WHITE, eyeColor: EyeColor::GREEN),
        );
    }

    public static function noWhiteColors(): iterable
    {
        foreach (Color::cases() as $color) {
            if (Color::WHITE === $color) {
                continue;
            }

            yield [$color];
        }
    }

    #[DataProvider('noWhiteColors')]
    public function testWhiteAllelesDontHaveBigW(Color $color): void
    {
        self::assertGeneticsEquals(
            'W:w,w',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color),
        );
    }

    public function testBlackAllelesUnknown(): void
    {
        self::assertGeneticsEquals(
            'B:,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, COLOR::WHITE, eyeColor: EyeColor::GREEN),
        );
    }

    public static function blackAllelesHaveBigBColors(): iterable
    {
        return [
            [Color::BLACK],
            [Color::BLACK_TORTIE],
            [Color::BLUE],
            [Color::BLUE_TORTIE],
        ];
    }

    #[DataProvider('blackAllelesHaveBigBColors')]
    public function testBlackAllelesHaveBigB(Color $color): void
    {
        self::assertGeneticsEquals(
            'B:B,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color),
        );
    }

    public static function blackAllelesHaveSmallBColors(): iterable
    {
        return [
            [Color::CHOCOLATE],
            [Color::CHOCOLATE_TORTIE],
            [Color::LILAC],
            [Color::LILAC_TORTIE],
        ];
    }

    #[DataProvider('blackAllelesHaveSmallBColors')]
    public function testBlackAllelesHaveSmallB(Color $color): void
    {
        self::assertGeneticsEquals(
            'B:b,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color),
        );
    }

    public static function blackAllelesHaveBlblColors(): iterable
    {
        return [
            [Color::CINNAMON_SORREL],
            [Color::CINNAMON_SORREL_TORTIE],
            [Color::FAWN],
            [Color::FAWN_TORTIE],
        ];
    }

    #[DataProvider('blackAllelesHaveBlblColors')]
    public function testBlackAllelesHaveBlbl(Color $color): void
    {
        self::assertGeneticsEquals(
            'B:bl,bl',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color),
        );
    }

    public function testDilutorAllelesUnknown(): void
    {
        self::assertGeneticsEquals(
            'D:,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, COLOR::WHITE, eyeColor: EyeColor::GREEN),
        );
    }

    public static function dilutorAllelesHaveBigDColors(): iterable
    {
        return [
            [Color::BLACK],
            [Color::BLACK_TORTIE],
            [Color::CHOCOLATE],
            [Color::CHOCOLATE_TORTIE],
            [Color::CINNAMON_SORREL],
            [Color::CINNAMON_SORREL_TORTIE],
            [Color::RED],
        ];
    }

    #[DataProvider('dilutorAllelesHaveBigDColors')]
    public function testDilutorAllelesHaveBigD(Color $color): void
    {
        self::assertGeneticsEquals(
            'D:D,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color),
        );
    }

    public static function dilutorAllelesDontHaveBigDColors(): iterable
    {
        return [
            [Color::BLUE],
            [Color::BLUE_TORTIE],
            [Color::CREAM],
            [Color::FAWN],
            [Color::FAWN_TORTIE],
            [Color::LILAC],
            [Color::LILAC_TORTIE],
        ];
    }

    #[DataProvider('dilutorAllelesDontHaveBigDColors')]
    public function testDilutorAllelesDontHaveBigD(Color $color): void
    {
        self::assertGeneticsEquals(
            'D:d,d',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color),
        );
    }

    public function testWhiteColorDiluteModifierAllelesUnknown(): void
    {
        self::assertGeneticsEquals(
            'Dm:,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::WHITE, eyeColor: EyeColor::GREEN),
        );
    }

    public static function noWhiteColorDiluteModifierAllelesUnknownProvider(): iterable
    {
        foreach (Color::cases() as $color) {
            if ($color->isBlueOrCreamOrLilacOrFawn() || Color::WHITE === $color) {
                continue;
            }

            yield [$color];
        }
    }

    #[DataProvider('noWhiteColorDiluteModifierAllelesUnknownProvider')]
    public function testNoWhiteColorDiluteModifierAllelesUnknown(Color $color): void
    {
        self::assertGeneticsEquals(
            'Dm:,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color),
        );
    }

    public static function possibleColorsWithCaramelProvider(): iterable
    {
        foreach (Color::cases() as $color) {
            if (!$color->isBlueOrCreamOrLilacOrFawn()) {
                continue;
            }

            yield [$color];
        }
    }

    #[DataProvider('possibleColorsWithCaramelProvider')]
    public function testDiluteModifierAllelesHaveBigDm(Color $color): void
    {
        self::assertGeneticsEquals(
            'Dm:Dm,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color, caramel: true),
        );
    }

    #[DataProvider('possibleColorsWithCaramelProvider')]
    public function testDiluteModifierAllelesDontHaveBigDm(Color $color): void
    {
        self::assertGeneticsEquals(
            'Dm:dm,dm',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color),
        );
    }

    public static function colorAllelesUnknownProvider(): iterable
    {
        yield [Breed::PEB, EyeColor::INTENSE_DEEP_BLUE];
        yield [Breed::OLH, EyeColor::DEEP_BLUE];
        yield [Breed::OSH, EyeColor::DEEP_BLUE];
    }

    #[DataProvider('colorAllelesUnknownProvider')]
    public function testColorAllelesUnknown(Breed $breed, EyeColor $eyeColor): void
    {
        self::assertGeneticsEquals(
            'C:,',
            Genetics::fromCharacteristics($breed, Sex::MALE, Color::WHITE, eyeColor: $eyeColor),
        );
    }

    public static function pebColorAllelesHaveBigCProvider(): iterable
    {
        foreach (Color::cases() as $color) {
            foreach (array_merge(WhiteAmount::cases(), [null]) as $whiteAmount) {
                if (Color::WHITE === $color && null !== $whiteAmount) {
                    continue;
                }

                if (
                    Color::WHITE !== $color &&
                    in_array($whiteAmount, [WhiteAmount::BICOLOUR, WhiteAmount::UNSPECIFIED, null], true)
                ) {
                    continue;
                }

                foreach (array_merge(EyeColor::cases(), [null]) as $eyeColor) {
                    if (EyeColor::DEEP_BLUE === $eyeColor) {
                        continue;
                    }

                    if (Color::WHITE === $color && in_array($eyeColor, [null, EyeColor::INTENSE_DEEP_BLUE], true)) {
                        continue;
                    }

                    yield [$color, $whiteAmount, $eyeColor];
                }
            }
        }
    }

    #[DataProvider('pebColorAllelesHaveBigCProvider')]
    public function testPebColorAllelesHaveBigC(
        Color $color,
        ?WhiteAmount $whiteAmount,
        ?EyeColor $eyeColor,
    ): void {
        self::assertGeneticsEquals(
            'C:C,',
            Genetics::fromCharacteristics(
                Breed::PEB,
                Sex::FEMALE,
                $color,
                whiteAmount: $whiteAmount,
                eyeColor: $eyeColor,
            ),
        );
    }

    public static function olhOrOshColorAllelesHaveBigCProvider(): iterable
    {
        foreach ([Breed::OLH, Breed::OSH] as $breed) {
            yield [$breed, Color::WHITE, null, EyeColor::ODD];
            yield [$breed, Color::WHITE, null, EyeColor::GREEN];

            foreach (Color::cases() as $color) {
                if (Color::WHITE === $color) {
                    continue;
                }

                foreach ([WhiteAmount::VAN, WhiteAmount::HARLEQUIN] as $whiteAmount) {
                    yield [$breed, $color, $whiteAmount, null];
                }
            }
        }
    }

    #[DataProvider('olhOrOshColorAllelesHaveBigCProvider')]
    public function testOlhOrOshColorAllelesHaveBigC(
        Breed $breed,
        Color $color,
        ?WhiteAmount $whiteAmount,
        ?EyeColor $eyeColor,
    ): void {
        self::assertGeneticsEquals(
            'C:C,',
            Genetics::fromCharacteristics($breed, Sex::FEMALE, $color, whiteAmount: $whiteAmount, eyeColor: $eyeColor),
        );
    }

    public static function siaOrBalColorAllelesProvider(): iterable
    {
        foreach ([Breed::BAL, Breed::SIA] as $breed) {
            foreach (Color::cases() as $color) {
                yield [$breed, $color, Color::WHITE === $color ? EyeColor::INTENSE_DEEP_BLUE : null];
            }
        }
    }

    #[DataProvider('siaOrBalColorAllelesProvider')]
    public function testSiaOrBalColorAlleles(Breed $breed, Color $color, ?EyeColor $eyeColor): void
    {
        self::assertGeneticsEquals(
            'C:cs,cs',
            Genetics::fromCharacteristics($breed, Sex::FEMALE, $color, eyeColor: $eyeColor),
        );
    }

    public function testSiamesePointedColorAlleles(): void
    {
        self::assertGeneticsEquals(
            'C:cs,cs',
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK, true),
        );
    }

    public function testAgutiAllelesUnknown(): void
    {
        self::assertGeneticsEquals(
            'A:,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, COLOR::WHITE, eyeColor: EyeColor::GREEN),
        );
    }

    public static function agutiAllelesHaveBigAPrivider(): iterable
    {
        foreach (Color::cases() as $color) {
            if (Color::WHITE === $color) {
                continue;
            }

            foreach (Pattern::cases() as $pattern) {
                yield [$color, $pattern];
            }
        }
    }

    #[DataProvider('agutiAllelesHaveBigAPrivider')]
    public function testAgutiAllelesHaveBigA(Color $color, Pattern $pattern): void
    {
        self::assertGeneticsEquals(
            'A:A,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color, pattern: $pattern),
        );
    }

    #[DataProvider('noWhiteColors')]
    public function testAgutiAllelesDontHaveBigA(Color $color): void
    {
        self::assertGeneticsEquals(
            'A:a,a',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color),
        );
    }

    public function testSilverAllelesUnknown(): void
    {
        self::assertGeneticsEquals(
            'I:,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, COLOR::WHITE, eyeColor: EyeColor::GREEN),
        );
    }

    public static function silverAllelesHaveBigIProvider(): iterable
    {
        foreach (Color::cases() as $color) {
            if (Color::WHITE === $color) {
                continue;
            }

            foreach (SilverSmoke::cases() as $smoke) {
                yield [$color, $smoke];
            }
        }
    }

    #[DataProvider('silverAllelesHaveBigIProvider')]
    public function testSilverAllelesHaveBigI(Color $color, SilverSmoke $silverSmoke): void
    {
        self::assertGeneticsEquals(
            'I:I,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color, silverSmoke: $silverSmoke),
        );
    }

    #[DataProvider('noWhiteColors')]
    public function testSilverAllelesDontHaveBigI(Color $color): void
    {
        self::assertGeneticsEquals(
            'I:i,i',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color),
        );
    }

    public function testWithWhiteAllelesUnknown(): void
    {
        self::assertGeneticsEquals(
            'S:,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, COLOR::WHITE, eyeColor: EyeColor::GREEN),
        );
    }

    public static function withWhiteAllelesHaveBigSProvider(): iterable
    {
        foreach (Color::cases() as $color) {
            if (Color::WHITE === $color) {
                continue;
            }

            foreach (WhiteAmount::cases() as $whiteAmount) {
                yield [$color, $whiteAmount];
            }
        }
    }

    #[DataProvider('withWhiteAllelesHaveBigSProvider')]
    public function testWithWhiteAllelesHaveBigS(Color $color, WhiteAmount $whiteAmount): void
    {
        self::assertGeneticsEquals(
            'S:S,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color, whiteAmount: $whiteAmount),
        );
    }

    #[DataProvider('noWhiteColors')]
    public function testWithWhiteAllelesDontHaveBigS(Color $color): void
    {
        self::assertGeneticsEquals(
            'S:s,s',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color),
        );
    }

    public static function tabbyAllelesUnknownProvider(): iterable
    {
        yield [Color::WHITE, null, EyeColor::GREEN];

        foreach (Color::cases() as $color) {
            if (Color::WHITE === $color) {
                continue;
            }

            yield [$color, null, null];
            yield [$color, Pattern::UNSPECIFIED_TABBY, null];
        }
    }

    #[DataProvider('tabbyAllelesUnknownProvider')]
    public function testTabbyAllelesUnknown(Color $color, ?Pattern $pattern, ?EyeColor $eyeColor): void
    {
        self::assertGeneticsEquals(
            'T:,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color, pattern: $pattern, eyeColor: $eyeColor),
        );
    }

    #[DataProvider('noWhiteColors')]
    public function testTabbyAllelesHaveBigTa(Color $color): void
    {
        self::assertGeneticsEquals(
            'T:Ta,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color, pattern: Pattern::TICKED_TABBY),
        );
    }

    #[DataProvider('noWhiteColors')]
    public function testTabbyAllelesHaveBigTAndDontHaveBigTa(Color $color): void
    {
        self::assertGeneticsEquals(
            'T:T,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color, pattern: Pattern::MACKEREL_TABBY),
        );
    }

    #[DataProvider('noWhiteColors')]
    public function testTabbyAllelesHaveSmallTsAndDontHaveBigTaAndBigT(Color $color): void
    {
        self::assertGeneticsEquals(
            'T:ts,',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color, pattern: Pattern::SPOTTED_TABBY),
        );
    }

    #[DataProvider('noWhiteColors')]
    public function testTabbyAllelesHaveSmallTbtb(Color $color): void
    {
        self::assertGeneticsEquals(
            'T:tb,tb',
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color, pattern: Pattern::BLOTCHED_TABBY),
        );
    }

    public static function praAllelesProvider(): iterable
    {
        return [
            ['pra:,', null],
            ['pra:N,N', PRA::NN],
            ['pra:N,M', PRA::NM],
            ['pra:M,M', PRA::MM],
        ];
    }

    #[DataProvider('praAllelesProvider')]
    public function testPraAlleles(string $expected, ?PRA $pra): void
    {
        self::assertGeneticsEquals(
            $expected,
            Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, COLOR::BLACK, pra: $pra),
        );
    }

    public static function eyeColorUnknownProvider(): iterable
    {
        foreach (Breed::cases() as $breed) {
            foreach (Color::cases() as $color) {
                foreach (array_merge([null], WhiteAmount::cases()) as $whiteAmount) {
                    foreach ([false, true] as $siamesePointed) {
                        if (
                            Color::WHITE === $color && null !== $whiteAmount ||
                            Breed::PEB !== $breed && ($whiteAmount === WhiteAmount::UNSPECIFIED || $siamesePointed)
                        ) {
                            continue;
                        }

                        if (
                            Color::WHITE !== $color &&
                            (Breed::SIA === $breed || $siamesePointed || null === $whiteAmount || $breed->isOlhOrOsh())
                        ) {
                            yield [$breed, $color, $siamesePointed, $whiteAmount];
                        }
                    }
                }
            }
        }
    }

    #[DataProvider('eyeColorUnknownProvider')]
    public function testEyeColorUnknown(
        Breed $breed,
        Color $color,
        bool $siamesePointed,
        ?WhiteAmount $whiteAmount,
    ): void {
        self::assertGeneticsEquals(
            'eye:',
            Genetics::fromCharacteristics($breed, Sex::FEMALE, $color, $siamesePointed, whiteAmount: $whiteAmount),
        );
    }

    public static function EyeColorSiaOrBalKnownProvider(): iterable
    {
        yield [Breed::SIA];
        yield [Breed::BAL];
    }

    #[DataProvider('EyeColorSiaOrBalKnownProvider')]
    public function testEyeColorSiaAndBalKnown(
        Breed $breed,
    ): void {
        self::assertGeneticsEquals(
            'eye:INTENSE_DEEP_BLUE',
            Genetics::fromCharacteristics(
                $breed,
                Sex::FEMALE,
                color:Color::WHITE,
                eyeColor: EyeColor::INTENSE_DEEP_BLUE,
            ),
        );
    }

    public static function EyeColorOlhOrOshKnownProvider(): iterable
    {
        return [
            ['eye:ODD', EyeColor::ODD, Breed::OLH],
            ['eye:GREEN', EyeColor::GREEN, Breed::OLH],
            ['eye:DEEP_BLUE', EyeColor::DEEP_BLUE, Breed::OLH],
            ['eye:ODD', EyeColor::ODD, Breed::OSH],
            ['eye:GREEN', EyeColor::GREEN, Breed::OSH],
            ['eye:DEEP_BLUE', EyeColor::DEEP_BLUE, Breed::OSH],
        ];
    }

    #[DataProvider('EyeColorOlhOrOshKnownProvider')]
    public function testEyeColorOlhOrOshKnown(
        $expected,
        EyeColor $eyeColor,
        Breed $breed,
    ): void {
        self::assertGeneticsEquals(
            $expected,
            Genetics::fromCharacteristics($breed, Sex::FEMALE, color:Color::WHITE, eyeColor: $eyeColor),
        );
    }

    public static function EyeColorPebKnownProvider(): iterable
    {
        yield ['eye:ODD', Color::WHITE, false, null, EyeColor::ODD];
        yield ['eye:GREEN', Color::WHITE, false, null, EyeColor::GREEN];
        yield ['eye:YELLOW', Color::WHITE, false, null, EyeColor::YELLOW];
        yield ['eye:AQUAMARINE', Color::WHITE, false, null, EyeColor::AQUAMARINE];
        yield ['eye:INTENSE_DEEP_BLUE', Color::WHITE, false, null, EyeColor::INTENSE_DEEP_BLUE];

        foreach (Color::cases() as $color) {
            if (Color::WHITE === $color) {
                continue;
            }

            foreach (WhiteAmount::cases() as $whiteAmount) {
                yield ['eye:ODD', $color, false, $whiteAmount, EyeColor::ODD];
                yield ['eye:GREEN', $color, false, $whiteAmount, EyeColor::GREEN];
                yield ['eye:ODD', $color, true, $whiteAmount, EyeColor::ODD];
                yield ['eye:INTENSE_DEEP_BLUE', $color, true, $whiteAmount, EyeColor::INTENSE_DEEP_BLUE];
            }
        }
    }

    #[DataProvider('EyeColorPebKnownProvider')]
    public function testEyeColorPebKnown(
        string $expected,
        Color $color,
        bool $siamesePointed,
        ?WhiteAmount $whiteAmount,
        EyeColor $eyeColor,
    ): void {
        self::assertGeneticsEquals(
            $expected,
            Genetics::fromCharacteristics(
                Breed::PEB,
                Sex::FEMALE,
                $color,
                $siamesePointed,
                whiteAmount:$whiteAmount,
                eyeColor: $eyeColor,
            ),
        );
    }

    #[DataProvider('tortieColorsProvider')]
    public function testMaleCantBeTortie(Color $color): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::GENETIC_MALE_CANT_BE_TORTIE_ERR);

        Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, $color);
    }

    public static function orientalBreedsProvider(): iterable
    {
        foreach (Breed::cases() as $breed) {
            if ($breed->isOriental()) {
                yield [$breed];
            }
        }
    }

    #[DataProvider('orientalBreedsProvider')]
    public function testForbiddenSiamesePointedAndOrientalBreed(Breed $breed): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::GENETIC_FORBIDDEN_SIAMESE_POINTED_AND_ORIENTAL_BREED_ERR);

        Genetics::fromCharacteristics($breed, Sex::MALE, Color::BLACK, siamesePointed: true);
    }

    public function testForbiddenSiamesePointedAndWhiteColor(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::GENETIC_FORBIDDEN_SIAMESE_POINTED_AND_WHITE_COLOR_ERR);

        Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::WHITE, siamesePointed: true);
    }

    public static function forbiddenCaramelAndColorProvider(): iterable
    {
        foreach (Color::cases() as $color) {
            if ($color->isBlueOrCreamOrLilacOrFawn()) {
                continue;
            }

            yield [$color];
        }
    }

    #[DataProvider('forbiddenCaramelAndColorProvider')]
    public function testForbiddenCaramelAndColor(Color $color): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::GENETIC_FORBIDDEN_CARAMEL_AND_COLOR_ERR);

        Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, $color, caramel: true);
    }

    public static function forbiddenSilverAndBreedProvider(): iterable
    {
        foreach (SilverSmoke::cases() as $silverSmoke) {
            yield [$silverSmoke, Breed::BAL];
            yield [$silverSmoke, Breed::SIA];
        }
    }

    #[DataProvider('forbiddenSilverAndBreedProvider')]
    public function testForbiddenSilverAndBreed(SilverSmoke $silverSmoke, Breed $breed): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::GENETIC_FORBIDDEN_SILVER_AND_BREED_ERR);

        Genetics::fromCharacteristics($breed, Sex::FEMALE, Color::BLACK, silverSmoke: $silverSmoke);
    }

    public static function silverSmokeProvider(): iterable
    {
        yield [SilverSmoke::SILVER];
        yield [SilverSmoke::SMOKE];
    }

    #[DataProvider('silverSmokeProvider')]
    public function testForbiddenSilverAndWhiteColor(SilverSmoke $silverSmoke): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::GENETIC_FORBIDDEN_SILVER_AND_WHITE_COLOR_ERR);

        Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::WHITE, silverSmoke: $silverSmoke);
    }

    #[DataProvider('orientalBreedsProvider')]
    public function testForbiddenUnspecifiedWhiteAmountAndOrientalBreed(Breed $breed): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(
            CatValidationException::GENETIC_FORBIDDEN_UNSPECIFIED_WHITE_AMOUNT_AND_ORIENTAL_BREED_ERR,
        );

        Genetics::fromCharacteristics($breed, Sex::MALE, Color::BLACK, whiteAmount: WhiteAmount::UNSPECIFIED);
    }

    public static function forbiddenWhiteAmountAndWhiteColorProvider(): iterable
    {
        foreach (Breed::cases() as $breed) {
            foreach (WhiteAmount::cases() as $whiteAmount) {
                if (WhiteAmount::UNSPECIFIED === $whiteAmount && $breed->isOriental()) {
                    continue;
                }

                yield [$breed, $whiteAmount];
            }
        }
    }

    #[DataProvider('forbiddenWhiteAmountAndWhiteColorProvider')]
    public function testForbiddenWhiteAmountAndWhiteColor(Breed $breed, WhiteAmount $whiteAmount): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(
            CatValidationException::GENETIC_FORBIDDEN_WHITE_AMOUNT_AND_WHITE_COLOR_ERR,
        );

        Genetics::fromCharacteristics($breed, Sex::MALE, Color::WHITE, whiteAmount: $whiteAmount);
    }

    public static function forbiddenSpecifiedPatternAndSiaOrBalProvider(): iterable
    {
        foreach (Pattern::cases() as $pattern) {
            if (Pattern::UNSPECIFIED_TABBY === $pattern) {
                continue;
            }

            yield [$pattern, Breed::BAL];
            yield [$pattern, Breed::SIA];
        }
    }

    #[DataProvider('forbiddenSpecifiedPatternAndSiaOrBalProvider')]
    public function testForbiddenSpecifiedPatternAndSiaOrBal(Pattern $pattern, Breed $breed): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::GENETIC_FORBIDDEN_SPECIFIED_PATTERN_AND_SIA_OR_BAL_ERR);

        Genetics::fromCharacteristics($breed, Sex::MALE, Color::BLACK, pattern: $pattern);
    }

    public static function forbiddenPatternAndWhiteAmountProvider(): iterable
    {
        foreach (Pattern::cases() as $pattern) {
            foreach (Breed::cases() as $breed) {
                if ($pattern->isSpecified() && $breed->isBalOrSia()) {
                    continue;
                }

                yield [$breed, $pattern];
            }
        }
    }

    #[DataProvider('forbiddenPatternAndWhiteAmountProvider')]
    public function testForbiddenPatternAndWhite(Breed $breed, Pattern $pattern): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::GENETIC_FORBIDDEN_WHITE_AMOUNT_AND_PATTERN_ERR);

        Genetics::fromCharacteristics($breed, Sex::MALE, Color::WHITE, pattern: $pattern);
    }

    public static function specifiedPatternProvider(): iterable
    {
        foreach (Pattern::cases() as $pattern) {
            if (!$pattern->isSpecified()) {
                continue;
            }

            yield [$pattern];
        }
    }

    #[DataProvider('specifiedPatternProvider')]
    public function testForbiddenSpecifiedPatternAndSiamesePointed(Pattern $pattern): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::GENETIC_FORBIDDEN_SPECIFIED_PATTERN_AND_SIAMESE_POINTED_ERR);

        Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK, siamesePointed: true, pattern: $pattern);
    }

    public static function forbiddenUnspecifiedPatternAndBicolourWhiteAmountAndOlhOrOshProvider(): iterable
    {
        yield [Breed::OLH];
        yield [Breed::OSH];
    }

    #[DataProvider('forbiddenUnspecifiedPatternAndBicolourWhiteAmountAndOlhOrOshProvider')]
    public function testForbiddenUnspecifiedPatternAndBicolourWhiteAmountAndOlhOrOsh(Breed $breed): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(
            CatValidationException::GENETIC_FORBIDDEN_UNSPECIFIED_PATTERN_AND_BICOLOUR_WHITE_AMOUNT_AND_OLH_OR_OSH_ERR
        );

        Genetics::fromCharacteristics(
            $breed,
            Sex::MALE,
            Color::BLACK,
            whiteAmount: WhiteAmount::BICOLOUR,
            pattern: Pattern::UNSPECIFIED_TABBY,
        );
    }

    public function testForbiddenDeepBlueAndPeb(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::GENETIC_FORBIDDEN_EYE_COLOR_AND_BREED_ERR);

        Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::WHITE, eyeColor: EyeColor::DEEP_BLUE);
    }

    public static function forbiddenEyeColorAndBalOrSiaProvider(): iterable
    {
        foreach (EyeColor::cases() as $eyeColor) {
            if (EyeColor::INTENSE_DEEP_BLUE === $eyeColor) {
                continue;
            }

            yield [$eyeColor, Breed::BAL];
            yield [$eyeColor, Breed::SIA];
        }
    }

    #[DataProvider('forbiddenEyeColorAndBalOrSiaProvider')]
    public function testForbiddenEyeColorAndBalOrSia(EyeColor $eyeColor, Breed $breed): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::GENETIC_FORBIDDEN_EYE_COLOR_AND_BREED_ERR);

        Genetics::fromCharacteristics($breed, Sex::MALE, Color::WHITE, eyeColor: $eyeColor);
    }

    public static function forbiddenEyeColorAndOlhOrOshProvider(): iterable
    {
        foreach (EyeColor::cases() as $eyeColor) {
            if (in_array($eyeColor, [EyeColor::DEEP_BLUE, EyeColor::ODD, EyeColor::GREEN], true)) {
                continue;
            }

            yield [$eyeColor, Breed::OLH];
            yield [$eyeColor, Breed::OSH];
        }
    }

    #[DataProvider('forbiddenEyeColorAndOlhOrOshProvider')]
    public function testForbiddenEyeColorAndOlhOrOsh(EyeColor $eyeColor, Breed $breed): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::GENETIC_FORBIDDEN_EYE_COLOR_AND_BREED_ERR);

        Genetics::fromCharacteristics($breed, Sex::MALE, Color::WHITE, eyeColor: $eyeColor);
    }

    public static function forbiddenNoEyeColorAndWhiteColorProvider(): iterable
    {
        foreach (Breed::cases() as $breed) {
            yield [$breed];
        }
    }

    #[DataProvider('forbiddenNoEyeColorAndWhiteColorProvider')]
    public function testForbiddenNoEyeColorAndWhiteColor(Breed $breed): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::GENETIC_FORBIDDEN_NO_EYE_COLOR_AND_WHITE_COLOR_ERR);

        Genetics::fromCharacteristics($breed, Sex::MALE, Color::WHITE);
    }

    public static function forbiddenEyeColorAndSiamesePointedProvider(): iterable
    {
        foreach (EyeColor::cases() as $eyeColor) {
            if (in_array($eyeColor, [EyeColor::DEEP_BLUE, EyeColor::INTENSE_DEEP_BLUE, EyeColor::ODD], true)) {
                continue;
            }

            yield [$eyeColor];
        }
    }

    #[DataProvider('forbiddenEyeColorAndSiamesePointedProvider')]
    public function testForbiddenEyeColorAndSiamesePointed(EyeColor $eyeColor): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::GENETIC_FORBIDDEN_EYE_COLOR_AND_SIAMESE_POINTED_ERR);

        Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK, true, eyeColor: $eyeColor);
    }

    public static function forbiddenEyeColorAndPatternProvider(): iterable
    {
        foreach (EyeColor::cases() as $eyeColor) {
            if (in_array($eyeColor, [EyeColor::DEEP_BLUE, EyeColor::ODD, EyeColor::GREEN], true)) {
                continue;
            }

            foreach (Pattern::cases() as $pattern) {
                if (Pattern::UNSPECIFIED_TABBY === $pattern) {
                    continue;
                }

                yield [$eyeColor, $pattern];
            }
        }
    }

    #[DataProvider('forbiddenEyeColorAndPatternProvider')]
    public function testForbiddenEyeColorAndPattern(EyeColor $eyeColor, Pattern $pattern): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::GENETIC_FORBIDDEN_EYE_COLOR_AND_PATTERN_ERR);

        Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK, pattern: $pattern, eyeColor: $eyeColor);
    }
}
