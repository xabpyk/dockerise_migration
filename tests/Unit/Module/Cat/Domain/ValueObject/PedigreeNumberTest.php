<?php

declare(strict_types=1);

namespace App\Tests\Unit\Module\Cat\Domain\ValueObject;

use App\Module\Cat\Domain\Exception\CatValidationException;
use App\Module\Cat\Domain\ValueObject\PedigreeNumber;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

final class PedigreeNumberTest extends TestCase
{
    public function testValidNumber(): void
    {
        $pedigreeNumber = new PedigreeNumber('(USA) ABZ 123456-7890-Q');

        self::assertSame('(USA) ABZ 123456-7890-Q', $pedigreeNumber->number);
    }

    public function testShortestValidNumber(): void
    {
        $pedigreeNumber = new PedigreeNumber('123');

        self::assertSame('123', $pedigreeNumber->number);
    }

    public function testLongestValidNumber(): void
    {
        $pedigreeNumber = new PedigreeNumber('123456789 123456789 123456789 12');

        self::assertSame('123456789 123456789 123456789 12', $pedigreeNumber->number);
    }

    public static function tooShortNumberProvider(): iterable
    {
        yield [''];
        yield ['  '];
        yield ['12'];
    }

    #[DataProvider('tooShortNumberProvider')]
    public function testTooShortNumber(string $number): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::PEDIGREE_NUMBER_MIN_LENGTH_ERR);

        new PedigreeNumber($number);
    }

    public function testTooLongNumber(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::PEDIGREE_NUMBER_MAX_LENGTH_ERR);

        new PedigreeNumber('123456789 123456789 123456789 123');
    }

    public static function whitespaceOnEdgeProvider(): iterable
    {
        return [
            ['    '],
            [' 123'],
            ["\n123"],
            ["\t123"],
            ['123 '],
            ["123\n"],
            ["123\t"],
            [' 123 '],
            ["\n123\n"],
            ["\t123\t"],
        ];
    }

    #[DataProvider('whitespaceOnEdgeProvider')]
    public function testWhitespaceOnEdge(string $number): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::PEDIGREE_NUMBER_WHITESPACE_ON_EDGE_ERR);

        new PedigreeNumber($number);
    }

    public static function NumberContainsInvalidCharacterProvider(): iterable
    {
        $invalidCharacters = "\"\\§=±!@#$%^&*+[]{};:'|`~,<.>/?й";

        foreach(mb_str_split($invalidCharacters) as $char) {
            yield ["123{$char}"];
        }
    }

    #[DataProvider('NumberContainsInvalidCharacterProvider')]
    public function testNumberContainsInvalidCharacter(string $number): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::PEDIGREE_NUMBER_CONTAINS_INVALID_CHARACTER_ERR);

        new PedigreeNumber($number);
    }

    public function testNumberDoesNotContainAnyDigits(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::PEDIGREE_NUMBER_DOES_NOT_CONTAIN_ANY_DIGITS_ERR);

        new PedigreeNumber('()  --Q');
    }
}
