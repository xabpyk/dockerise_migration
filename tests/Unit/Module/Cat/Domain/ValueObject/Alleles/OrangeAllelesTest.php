<?php

declare(strict_types=1);

namespace App\Tests\Unit\Module\Cat\Domain\ValueObject\Alleles;

use App\Module\Cat\Domain\Exception\CatValidationException;
use App\Module\Cat\Domain\ValueObject\Allele\OrangeAllele;
use App\Module\Cat\Domain\ValueObject\Alleles\OrangeAlleles;
use PHPUnit\Framework\TestCase;

final class OrangeAllelesTest extends TestCase
{
    public function testValidFemale(): void
    {
        $alleles = new OrangeAlleles(OrangeAllele::O, OrangeAllele::o);

        self::assertSame(OrangeAllele::O, $alleles->allele1);
        self::assertSame(OrangeAllele::o, $alleles->allele2);
    }

    public function testValidMale(): void
    {
        $alleles = new OrangeAlleles(OrangeAllele::o, OrangeAllele::Y);

        self::assertSame(OrangeAllele::o, $alleles->allele1);
        self::assertSame(OrangeAllele::Y, $alleles->allele2);
    }

    public function testTwoY(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::ALLELES_ORANGE_TWO_Y_ERR);

        new OrangeAlleles(OrangeAllele::Y, OrangeAllele::Y);
    }
}
