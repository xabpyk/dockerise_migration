<?php

declare(strict_types=1);

namespace App\Tests\Unit\Module\Cat\Domain\Entity;

use App\Module\Cat\Domain\Entity\Cat;
use App\Module\Cat\Domain\Exception\CatValidationException;
use App\Module\Cat\Domain\ValueObject\CatName;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Breed;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Color;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Sex;
use App\Module\Cat\Domain\ValueObject\Genetics;
use App\Module\Cat\Domain\ValueObject\PedigreeNumber;
use DateTimeImmutable;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\DoesNotPerformAssertions;
use PHPUnit\Framework\TestCase;

final class CatTest extends TestCase
{
    public function testMinCreate(): void
    {
        $name = new CatName('Mur zik');
        $genetics = Genetics::fromCharacteristics(Breed::SIA, Sex::MALE, Color::BLACK);

        $cat = new Cat(1, $name, $genetics);

        self::assertSame(1, $cat->id);
        self::assertSame($name, $cat->name);
        self::assertSame($genetics, $cat->genetics);
    }

    public function testMaxCreate(): void
    {
        $name = new CatName('Alisa 123');
        $genetics = Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLUE_TORTIE);
        $pedigreeNumber = new PedigreeNumber('123345678');

        $cat = new Cat(
            1,
            $name,
            $genetics,
            new DateTimeImmutable('2000-01-01'),
            $pedigreeNumber,
            2,
            3,
        );

        self::assertSame(1, $cat->id);
        self::assertSame($name, $cat->name);
        self::assertSame($genetics, $cat->genetics);
        self::assertEquals(new DateTimeImmutable('2000-01-01'), $cat->birthDate);
        self::assertSame($pedigreeNumber, $cat->pedigreeNumber);
        self::assertSame(2, $cat->getFatherId());
        self::assertSame(3, $cat->getMotherId());
    }

    public function testBirthDateIsBefore1800(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::BIRTH_DATE_IS_BEFORE_1800_ERR);

        new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            new DateTimeImmutable('1799-12-31'),
        );
    }

    public static function birthDateIsAfter1800Provider(): iterable
    {
        yield [new DateTimeImmutable('1800-01-01')];
        yield [new DateTimeImmutable('1990-12-31')];
    }

    #[DataProvider('birthDateIsAfter1800Provider')]
    #[DoesNotPerformAssertions]
    public function testBirthDateIsAfter1800(DateTimeImmutable $birthDate): void
    {
        new Cat(
            1,
            new CatName('Murzik Vasyliovych'),
            Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK),
            $birthDate,
        );
    }

    public static function catParentIdIsSameAsCatIdProvider(): iterable
    {
        return [
            [2, 2, null],
            [2, 2, 3],
            [2, null, 2],
            [2, 3, 2],
        ];
    }

    #[DataProvider('catParentIdIsSameAsCatIdProvider')]
    public function testCatParentIdIsSameAsCatId(int $id, ?int $fatherId, ?int $motherId): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::PARENT_ID_IS_SAME_AS_CAT_ID_ERR);

        $genetics = Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLUE_TORTIE);

        new Cat($id, new CatName('Alisa 123'), $genetics, fatherId: $fatherId, motherId: $motherId);
    }

    public function testCatParentIdsAreSame(): void
    {
        self::expectException(CatValidationException::class);
        self::expectExceptionCode(CatValidationException::PARENT_IDS_ARE_SAME_ERR);

        $genetics = Genetics::fromCharacteristics(Breed::PEB, Sex::FEMALE, Color::BLUE_TORTIE);

        new Cat(1, new CatName('Alisa 123'), $genetics, fatherId: 2, motherId: 2);
    }
}
