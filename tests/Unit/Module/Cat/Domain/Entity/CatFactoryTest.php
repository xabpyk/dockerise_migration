<?php

declare(strict_types=1);

namespace App\Tests\Unit\Module\Cat\Domain\Entity;

use App\Module\Cat\Domain\Criteria\CatBirthDateHasArrivedCriterionInterface;
use App\Module\Cat\Domain\Criteria\CatHasCorrectChildrenCriterionInterface;
use App\Module\Cat\Domain\Criteria\CatHasCorrectParentsCriterionInterface;
use App\Module\Cat\Domain\Criteria\UniqueCatNameCriterionInterface;
use App\Module\Cat\Domain\Criteria\UniquePedigreeNumberCriterionInterface;
use App\Module\Cat\Domain\Entity\Cat;
use App\Module\Cat\Domain\Entity\CatFactory;
use App\Module\Cat\Domain\ValueObject\CatName;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Breed;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Color;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Sex;
use App\Module\Cat\Domain\ValueObject\Genetics;
use App\Module\Cat\Domain\ValueObject\PedigreeNumber;
use DateTimeImmutable;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;

final class CatFactoryTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    private const int CAT_ID = 10;
    private const int FATHER_ID = 20;
    private const int MOTHER_ID = 30;

    public function testCreateShort(): void
    {
        $name = new CatName('Murzik Vasyliovych');
        $genetics = Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK);

        $catMatcher = fn(Cat $cat) => $cat->id === self::CAT_ID && $cat->name === $name && $cat->genetics === $genetics;

        $birthDateCriterion = Mockery::mock(CatBirthDateHasArrivedCriterionInterface::class);
        $birthDateCriterion->shouldReceive('ensureIsSatisfiedBy')->with(Mockery::on($catMatcher))->once();

        $nameCriterion = Mockery::mock(UniqueCatNameCriterionInterface::class);
        $nameCriterion->shouldReceive('ensureIsSatisfiedBy')->with(Mockery::on($catMatcher))->once();

        $pedigreeNumberCriterion = Mockery::mock(UniquePedigreeNumberCriterionInterface::class);
        $pedigreeNumberCriterion->shouldReceive('ensureIsSatisfiedBy')->with(Mockery::on($catMatcher))->once();

        $parentsCriterion = Mockery::mock(CatHasCorrectParentsCriterionInterface::class);
        $parentsCriterion->shouldReceive('ensureIsSatisfiedBy')->with(Mockery::on($catMatcher))->once();

        $childrenCriterion = Mockery::mock(CatHasCorrectChildrenCriterionInterface::class);
        $childrenCriterion->shouldReceive('ensureIsSatisfiedBy')->with(Mockery::on($catMatcher))->once();

        $factory = new CatFactory(
            $birthDateCriterion,
            $nameCriterion,
            $pedigreeNumberCriterion,
            $parentsCriterion,
            $childrenCriterion,
        );
        $cat = $factory->create(self::CAT_ID, $name, $genetics);

        self::assertSame(self::CAT_ID, $cat->id);
        self::assertSame($name, $cat->name);
        self::assertSame($genetics, $cat->genetics);
        self::assertNull($cat->birthDate);
        self::assertNull($cat->pedigreeNumber);
        self::assertNull($cat->getFatherId());
        self::assertNull($cat->getMotherId());
    }

    public function testCreateFull(): void
    {
        $name = new CatName('Murzik Vasyliovych');
        $genetics = Genetics::fromCharacteristics(Breed::PEB, Sex::MALE, Color::BLACK);
        $birthDate = new DateTimeImmutable('2020-01-01');
        $pedigreeNumber = new PedigreeNumber('1234567');

        $birthDateCriterion = self::createStub(CatBirthDateHasArrivedCriterionInterface::class);
        $nameCriterion = self::createStub(UniqueCatNameCriterionInterface::class);
        $pedigreeNumberCriterion = self::createStub(UniquePedigreeNumberCriterionInterface::class);
        $parentsCriterion = self::createStub(CatHasCorrectParentsCriterionInterface::class);
        $childrenCriterion = self::createStub(CatHasCorrectChildrenCriterionInterface::class);

        $factory = new CatFactory(
            $birthDateCriterion,
            $nameCriterion,
            $pedigreeNumberCriterion,
            $parentsCriterion,
            $childrenCriterion,
        );
        $cat = $factory->create(
            self::CAT_ID,
            $name,
            $genetics,
            $birthDate,
            $pedigreeNumber,
            self::FATHER_ID,
            self::MOTHER_ID,
        );

        self::assertSame(self::CAT_ID, $cat->id);
        self::assertSame($name, $cat->name);
        self::assertSame($genetics, $cat->genetics);
        self::assertSame($birthDate, $cat->birthDate);
        self::assertSame($pedigreeNumber, $cat->pedigreeNumber);
        self::assertSame(self::FATHER_ID, $cat->getFatherId());
        self::assertSame(self::MOTHER_ID, $cat->getMotherId());
    }
}
