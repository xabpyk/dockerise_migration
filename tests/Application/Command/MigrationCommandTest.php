<?php

declare(strict_types=1);

namespace App\Tests\Application\Command;

use App\Common\Db\DbConnection;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Depends;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

class MigrationCommandTest extends KernelTestCase
{
    private const MIGRATION_DIR = 'tests/Application/Command/migrations/';
    private const FIRST_MIGRATION_FILE = '00001.00__.sql';
    private const SECOND_MIGRATION_FILE = '00002.00__.sql';
    private const THIRD_MIGRATION_FILE = '00003.00__.sql';

    private string $schema;

    private CommandTester $commandTester;

    private DbConnection $db;

    public function setUp(): void
    {
        parent::setUp();

        $kernel = static::createKernel();
        $application = new Application($kernel);

        $serviceContainer = static::getContainer();

        if (!isset($this->schema)) {
            $this->schema = $serviceContainer->getParameter('app.db.schema');
        }

        $this->db = $serviceContainer->get('test.App\Common\Db\DbConnection');
        $this->db->query("DROP SCHEMA IF EXISTS {$this->schema} CASCADE");

        $command = $application->find('app:migration');
        $this->commandTester = new CommandTester($command);
    }

    public function tearDown(): void
    {
        $rmDirCommand = 'rm -rf ' . __DIR__ . '/migrations';
        `{$rmDirCommand}`;

        unset($this->commandTester, $this->db);

        parent::tearDown();
    }

    public function testNewWithEmptyStart(): void
    {
        $exitCode = $this->commandTester->execute([
            'action' => 'new',
        ]);

        self::assertSame(Command::SUCCESS, $exitCode);
        self::assertFileExists(self::MIGRATION_DIR . self::FIRST_MIGRATION_FILE);
    }

    public function testNewContinue(): void
    {
        mkdir(self::MIGRATION_DIR);
        file_put_contents(self::MIGRATION_DIR . self::FIRST_MIGRATION_FILE, '');

        $exitCode = $this->commandTester->execute([
            'action' => 'new',
        ]);

        self::assertSame(Command::SUCCESS, $exitCode);
        self::assertFileExists(self::MIGRATION_DIR . self::SECOND_MIGRATION_FILE);
    }

    public function testMigrateWithEmptyStart(): void
    {
        $exitCode = $this->commandTester->execute([
            'action' => 'migrate',
        ]);

        self::assertSame(Command::SUCCESS, $exitCode);

        $output = $this->commandTester->getDisplay();
        self::assertStringContainsString('Migrations not found', $output);
    }

    #[DataProvider('migrateFirstDataPrivider')]
    public function testMigrateFirst(array $commandParams): void
    {
        mkdir(self::MIGRATION_DIR);
        file_put_contents(
            self::MIGRATION_DIR . self::FIRST_MIGRATION_FILE,
            "CREATE TABLE {$this->schema}.first_table (id INT);\n",
        );

        $exitCode = $this->commandTester->execute($commandParams);

        self::assertSame(Command::SUCCESS, $exitCode);

        $sql = "SELECT EXISTS (
                    SELECT table_name
                    FROM information_schema.tables
                    WHERE
                        table_name = 'first_table' AND
                        table_schema = '{$this->schema}'
                )";
        $tableExists = $this->db->getOne($sql);

        self::assertTrue($tableExists);
    }

    public static function migrateFirstDataPrivider(): array
    {
        return [
            [['action' => 'migrate']],
            [[]],
        ];
    }

    #[Depends('testMigrateFirst')]
    public function testMigrateNext(): void
    {
        mkdir(self::MIGRATION_DIR);
        file_put_contents(
            self::MIGRATION_DIR . self::FIRST_MIGRATION_FILE,
            "CREATE TABLE {$this->schema}.first_table (id INT);\n",
        );

        $this->commandTester->execute([
            'action' => 'migrate',
        ]);

        file_put_contents(
            self::MIGRATION_DIR . self::SECOND_MIGRATION_FILE,
            "ALTER TABLE {$this->schema}.first_table ADD COLUMN val INT;\n",
        );
        file_put_contents(
            self::MIGRATION_DIR . self::THIRD_MIGRATION_FILE,
            "ALTER TABLE {$this->schema}.first_table ALTER COLUMN val TYPE VARCHAR(32);\n",
        );

        $exitCode = $this->commandTester->execute([
            'action' => 'migrate',
        ]);
        self::assertSame(Command::SUCCESS, $exitCode);

        $sql = "SELECT data_type
                FROM information_schema.columns
                WHERE
                    table_name = 'first_table' AND
                    table_schema = '{$this->schema}' AND
                    column_name = 'val'";
        $dataType = $this->db->getOne($sql);
        self::assertSame('character varying', $dataType);
    }

    public function testMigrateError(): void
    {
        mkdir(self::MIGRATION_DIR);
        file_put_contents(
            self::MIGRATION_DIR . self::FIRST_MIGRATION_FILE,
            "CREATE TABLE {$this->schema}.first_table (id INT);\n",
        );
        file_put_contents(
            self::MIGRATION_DIR . self::SECOND_MIGRATION_FILE,
            "ALTER TABLE {$this->schema}.first_table ADD COLUMN val INT;
                ALTER TABLE {$this->schema}.first_table ALTER COLUMN val TYPE BAD_DATA_TYPE;",
        );

        $exitCode = $this->commandTester->execute([]);

        self::assertSame(Command::FAILURE, $exitCode);

        $output = $this->commandTester->getDisplay();
        self::assertStringContainsString('SQL ERROR', $output);

        $sql = "SELECT EXISTS (
                    SELECT table_name
                    FROM information_schema.tables
                    WHERE
                        table_name = 'first_table' AND
                        table_schema = '{$this->schema}'
                )";
        $tableExists = $this->db->getOne($sql);

        self::assertFalse($tableExists);
    }

    #[Depends('testMigrateFirst')]
    public function testMigrateNothingNew(): void
    {
        mkdir(self::MIGRATION_DIR);
        file_put_contents(
            self::MIGRATION_DIR . self::FIRST_MIGRATION_FILE,
            "CREATE TABLE {$this->schema}.first_table (id INT);\n",
        );

        $this->commandTester->execute([]);

        $exitCode = $this->commandTester->execute([]);

        self::assertSame(Command::SUCCESS, $exitCode);
    }

    #[Depends('testMigrateFirst')]
    public function testReset(): void
    {
        mkdir(self::MIGRATION_DIR);
        file_put_contents(
            self::MIGRATION_DIR . self::FIRST_MIGRATION_FILE,
            "CREATE TABLE {$this->schema}.first_table (id INT);\n",
        );

        $this->commandTester->execute([
            'action' => 'migrate',
        ]);

        $exitCode = $this->commandTester->execute([
            'action' => 'reset',
        ]);

        self::assertSame(Command::SUCCESS, $exitCode);

        $sql = "SELECT EXISTS (
                    SELECT TRUE
                    FROM information_schema.schemata
                    WHERE schema_name = '{$this->schema}'
                )";
        $schemaExists = $this->db->getOne($sql);

        self::assertFalse($schemaExists);
    }

    public function testBadAction(): void
    {
        mkdir(self::MIGRATION_DIR);
        file_put_contents(
            self::MIGRATION_DIR . self::FIRST_MIGRATION_FILE,
            "CREATE TABLE {$this->schema}.first_table (id INT);\n",
        );

        $exitCode = $this->commandTester->execute([
            'action' => 'aksjdhvbaoiadg',
        ]);

        self::assertSame(Command::FAILURE, $exitCode);

        $output = $this->commandTester->getDisplay();
        self::assertStringContainsString('Unknown action', $output);

        $sql = "SELECT EXISTS (
                    SELECT table_name
                    FROM information_schema.tables
                    WHERE
                        table_name = 'first_table' AND
                        table_schema = '{$this->schema}'
                )";
        $tableExists = $this->db->getOne($sql);

        self::assertFalse($tableExists);
    }
}
