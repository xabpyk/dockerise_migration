<?php

declare(strict_types=1);

namespace App\Tests\Integration\Common\Db;

use App\Common\Db\DbConnection;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DbConnectionTest extends KernelTestCase
{
    private DbConnection $db;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->db = static::getContainer()->get('test.App\Common\Db\DbConnection');
    }

    public function tearDown(): void
    {
        unset($this->db);

        parent::tearDown();
    }

    public function testGetOne(): void
    {
        $result = $this->db->getOne("SELECT 'some_text'");

        self::assertSame('some_text', $result);
    }

    public function testGetRow(): void
    {
        $result = $this->db->getRow("SELECT 'some_text1' AS one, TRUE AS second");
        $expected = ['one' => 'some_text1', 'second' => true];
        self::assertSame($expected, $result);
    }

    public function testGetCol(): void
    {
        $sql = "WITH vals (col1) AS (VALUES
                    ('a'),
                    ('b')
                )
                SELECT col1
                FROM vals";
        $result = $this->db->getCol($sql);
        self::assertSame(['a', 'b'], $result);
    }

    public function testGetAssocTwoColumns(): void
    {
        $sql = "WITH vals (col1, col2) AS (VALUES
                    ('a', 1),
                    ('b', 2)
                )
                SELECT col1, col2
                FROM vals";
        $result = $this->db->getAssoc($sql);
        self::assertSame(['a' => 1, 'b' => 2], $result);
    }

    public function testGetAssocThreeColumns(): void
    {
        $sql = "WITH vals (id, a, b) AS (VALUES
                    (10, 100, TRUE),
                    (20, 200, FALSE)
                )
                SELECT id, a, b
                FROM vals";
        $result = $this->db->getAssoc($sql);
        $expected = [
            10 => ['a' => 100, 'b' => true],
            20 => ['a' => 200, 'b' => false],
        ];
        self::assertSame($expected, $result);
    }

    public function testGetAll(): void
    {
        $sql = "WITH vals (id, a, b) AS (VALUES
                    (10, 100, TRUE),
                    (20, 200, FALSE)
                )
                SELECT id, a, b
                FROM vals";
        $result = $this->db->getAll($sql);
        $expected = [
            ['id' => 10, 'a' => 100, 'b' => true],
            ['id' => 20, 'a' => 200, 'b' => false],
        ];
        self::assertSame($expected, $result);
    }

    public function testQuery(): void
    {
        $sql = "WITH vals (col1, col2) AS (VALUES
                    (10, 'a'),
                    (20, 'b')
                )
                SELECT col1, col2
                FROM vals";
        $result = $this->db->query($sql);

        $actual = [];
        foreach ($result->iterateAssociative() as $row) {
            $actual[] = $row;
        }

        $expected = [
            ['col1'=> 10, 'col2' => 'a'],
            ['col1'=> 20, 'col2' => 'b'],
        ];
        self::assertSame($expected, $actual);
    }
}
