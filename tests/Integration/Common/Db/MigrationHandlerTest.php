<?php

declare(strict_types=1);

namespace App\Tests\Integration\Common\Db;

use App\Common\Db\DbConnection;
use App\Common\Db\MigrationException;
use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class MigrationHandlerTest extends KernelTestCase
{
    private const SOME_TABLE_NAME = 'some_table';

    private string $schema;
    private string $migrationTable;
    private string $someTable;

    private string $tmpDir;

    private DbConnection $db;

    protected function setUp(): void
    {
        $this->tmpDir = sys_get_temp_dir()."/c2b/tests/migration_test_".safeUniqId();
        mkdir($this->tmpDir, 0777, true);

        self::bootKernel();

        if (!isset($this->schema)) {
            $this->schema = static::getContainer()->getParameter('app.db.schema');
            $this->migrationTable = static::getContainer()->getParameter('app.db.migration_table');
            $this->someTable = $this->schema.'.'.self::SOME_TABLE_NAME;
        }

        $this->db = static::getContainer()->get('test.App\Common\Db\DbConnection');

        $this->db->query("DROP SCHEMA IF EXISTS {$this->schema} CASCADE");
    }

    protected function tearDown(): void
    {
        `rm -Rf {$this->tmpDir}`;

        unset($this->db);

        parent::tearDown();
    }

    public function testNewFirstMigration(): void
    {
        $migrationHandler = static::getContainer()->get('test.App\Common\Db\MigrationHandler');
        $migrationHandler->setMigrationDir($this->tmpDir);
        $migrationHandler->new('first');

        self::assertFileExists("{$this->tmpDir}/00001.00__first.sql");
    }

    public function testNewNextMigration(): void
    {
        $table = $this->schema.'.first';

        file_put_contents("{$this->tmpDir}/00001.00__col1.sql", "CREATE TABLE {$table} (col1 INT);");
        file_put_contents("{$this->tmpDir}/00002.00__col2.sql", "ALTER TABLE {$table} RENAME COLUMN col1 TO col2;");
        file_put_contents("{$this->tmpDir}/00002.01__col3.sql", "ALTER TABLE {$table} RENAME COLUMN col2 TO col3;");

        $migrationHandler = static::getContainer()->get('test.App\Common\Db\MigrationHandler');
        $migrationHandler->setMigrationDir($this->tmpDir);
        $migrationHandler->new('next');

        self::assertFileExists("{$this->tmpDir}/00003.00__next.sql");
    }

    public function testMigrateCreateSchema(): void
    {
        self::expectException(MigrationException::class);
        self::expectExceptionCode(MigrationException::MIGRATIONS_NOT_FOUND);

        $migrationHandler = static::getContainer()->get('test.App\Common\Db\MigrationHandler');
        $migrationHandler->setMigrationDir("{$this->tmpDir}/non_exist_path_test");
        $migrationHandler->migrate();

        $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name = '{$this->schema}'";
        $schema = $this->db->getOne($sql);

        self::assertSame($this->schema, $schema);
    }

    public function testMigrateCreateMigrationTable(): void
    {
        self::expectException(MigrationException::class);
        self::expectExceptionCode(MigrationException::MIGRATIONS_NOT_FOUND);

        $this->db->query("CREATE SCHEMA {$this->schema}");

        $migrationHandler = static::getContainer()->get('test.App\Common\Db\MigrationHandler');
        $migrationHandler->setMigrationDir("{$this->tmpDir}/non_exist_path_test");
        $migrationHandler->migrate();

        $sql = "SELECT EXISTS (
                    SELECT *
                    FROM information_schema.tables
                    WHERE
                        table_schema = '{$this->schema}' AND
                        table_name   = '{$this->migrationTable}'
                )";
        $tableExist = $this->db->getOne($sql);

        self::assertTrue($tableExist);

        $sql = "SELECT column_name
                FROM information_schema.columns
                WHERE
                    table_schema = '{$this->schema}' AND
                    table_name = '{$this->migrationTable}'
                ORDER BY column_name ASC";
        $fields = $this->db->getCol($sql);

        self::assertSame(['hash', 'version'], $fields);
    }

    public function testMigrateNoMigrationsExist(): void
    {
        self::expectException(MigrationException::class);
        self::expectExceptionCode(MigrationException::MIGRATIONS_NOT_FOUND);

        $migrationHandler = static::getContainer()->get('test.App\Common\Db\MigrationHandler');
        $migrationHandler->setMigrationDir("{$this->tmpDir}/non_exist_path_test");
        $migrationHandler->migrate();

        $migration = $this->db->getOne("SELECT version FROM {$this->migrationTable}");

        self::assertFalse($migration);
    }

    public function testMigrateFirstMigration(): void
    {
        $migrationHandler = static::getContainer()->get('test.App\Common\Db\MigrationHandler');
        $migrationHandler->setMigrationDir(__DIR__."/migrations/first_migration_test");
        $migrationHandler->migrate();

        // new table exists
        $sql = "SELECT EXISTS (
                    SELECT *
                    FROM information_schema.tables
                    WHERE
                        table_schema = '{$this->schema}' AND
                        table_name   = 'first'
                )";
        $tableExist = $this->db->getOne($sql);
        self::assertTrue($tableExist);

        // new record exists in migration table
        $version = $this->db->getOne("SELECT version FROM {$this->migrationTable}");
        self::assertSame('00001.00', $version);
    }

    public function testMigrateNextMigration(): void
    {
        file_put_contents("{$this->tmpDir}/00001.00__col1.sql", "CREATE TABLE {$this->someTable} (col1 INT);");
        file_put_contents(
            "{$this->tmpDir}/00001.01__col2.sql",
            "ALTER TABLE {$this->someTable} RENAME COLUMN col1 TO col2;",
        );

        $migrationHandler = static::getContainer()->get('test.App\Common\Db\MigrationHandler');
        $migrationHandler->setMigrationDir($this->tmpDir);
        $migrationHandler->migrate();

        file_put_contents("{$this->tmpDir}/00002.00__col3.sql", "INSERT INTO {$this->someTable} (col2) VALUES (99);");

        $migrationHandler->migrate();

        $value = $this->db->getOne("SELECT col2 FROM {$this->someTable}");
        self::assertSame(99, $value);
    }

    public function testMigrateErrorBadHash(): void
    {
        file_put_contents("{$this->tmpDir}/00001.00__sql1.sql", "CREATE TABLE {$this->someTable} (col1 INT);");
        file_put_contents(
            "{$this->tmpDir}/00001.01__sql2.sql",
            "ALTER TABLE {$this->someTable} RENAME COLUMN col1 TO col2;",
        );

        $migrationHandler = static::getContainer()->get('test.App\Common\Db\MigrationHandler');
        $migrationHandler->setMigrationDir($this->tmpDir);
        $migrationHandler->migrate();

        file_put_contents(
            "{$this->tmpDir}/00001.01__sql2.sql",
            "ALTER TABLE {$this->someTable} RENAME COLUMN col1 TO col3;",
        );

        self::expectException(MigrationException::class);
        self::expectExceptionCode(MigrationException::BAD_HASH);
        self::expectExceptionMessageMatches('/00001.01__sql2.sql/');

        $migrationHandler->migrate();
    }

    public function testMigrateErrorMissedMigration(): void
    {
        file_put_contents("{$this->tmpDir}/00001.00__sql1.sql", "CREATE TABLE {$this->someTable} (col1 INT);");
        file_put_contents(
            "{$this->tmpDir}/00002.00__sql2.sql",
            "ALTER TABLE {$this->someTable} RENAME COLUMN col1 TO col2;",
        );

        $migrationHandler = static::getContainer()->get('test.App\Common\Db\MigrationHandler');
        $migrationHandler->setMigrationDir($this->tmpDir);
        $migrationHandler->migrate();

        file_put_contents(
            "{$this->tmpDir}/00001.01__sql1.1.sql",
            "ALTER TABLE {$this->someTable} ADD COLUMN col3 INT;",
        );

        self::expectException(MigrationException::class);
        self::expectExceptionCode(MigrationException::MISSED_MIGRATION);
        self::expectExceptionMessageMatches('/00001.01__sql1.1.sql/');

        $migrationHandler->migrate();
    }

    public function testMigrateErrorMissedFile(): void
    {
        file_put_contents("{$this->tmpDir}/00001.00__sql1.sql", "CREATE TABLE {$this->someTable} (col1 INT);");
        file_put_contents(
            "{$this->tmpDir}/00002.00__sql2.sql",
            "ALTER TABLE {$this->someTable} RENAME COLUMN col1 TO col2;",
        );

        $migrationHandler = static::getContainer()->get('test.App\Common\Db\MigrationHandler');
        $migrationHandler->setMigrationDir($this->tmpDir);
        $migrationHandler->migrate();

        unlink("{$this->tmpDir}/00002.00__sql2.sql");

        self::expectException(MigrationException::class);
        self::expectExceptionCode(MigrationException::MISSED_FILE);
        self::expectExceptionMessageMatches('/00002.00/');

        $migrationHandler->migrate();
    }

    public function testMigrateErrorNewFilesConflict(): void
    {
        file_put_contents("{$this->tmpDir}/00001.00__sql.sql", "CREATE TABLE {$this->someTable} (col1 INT);");

        $migrationHandler = static::getContainer()->get('test.App\Common\Db\MigrationHandler');
        $migrationHandler->setMigrationDir($this->tmpDir);
        $migrationHandler->migrate();

        file_put_contents("{$this->tmpDir}/00002.00__1.sql", "ALTER TABLE {$this->someTable} ADD COLUMN col2 INT;");
        file_put_contents("{$this->tmpDir}/00002.00__2.sql", "ALTER TABLE {$this->someTable} ADD COLUMN col3 INT;");

        self::expectException(MigrationException::class);
        self::expectExceptionCode(MigrationException::NEW_FILES_CONFLICT);
        self::expectExceptionMessageMatches('/00002.00__1.sql/');
        self::expectExceptionMessageMatches('/00002.00__2.sql/');

        $migrationHandler->migrate();
    }

    public function testMigrateSqlErrorTransactionRollback(): void
    {
        self::expectException(MigrationException::class);
        self::expectExceptionCode(MigrationException::SQL_EXEC_ERROR);
        self::expectExceptionMessageMatches('/CREATE TABLE.*ERROR.*column.*col2.*not exist/s');

        $this->runMigrationWithSqlError();
    }

    public function testMigrateSqlErrorExeption(): void
    {
        try {
            $this->runMigrationWithSqlError();
        } catch (MigrationException $e) {
        }

        $sql = "SELECT EXISTS (
                    SELECT *
                    FROM information_schema.tables
                    WHERE
                        table_schema = '{$this->schema}' AND
                        table_name   = '".self::SOME_TABLE_NAME."'
                )";
        $tableExist = $this->db->getOne($sql);

        self::assertFalse($tableExist);

        $dbVersions = $this->db->getCol("SELECT version FROM {$this->schema}.{$this->migrationTable}");

        self::assertSame([], $dbVersions);
    }

    private function runMigrationWithSqlError(): void
    {
        file_put_contents("{$this->tmpDir}/00001.00__sql.sql", "CREATE TABLE {$this->someTable} (col1 INT);");
        file_put_contents(
            "{$this->tmpDir}/00002.00__1.sql",
            "ALTER TABLE {$this->someTable} RENAME COLUMN col2 TO col1;",
        );

        $migrationHandler = static::getContainer()->get('test.App\Common\Db\MigrationHandler');
        $migrationHandler->setMigrationDir($this->tmpDir);
        $migrationHandler->migrate();
    }

    public function testMigrateIgnorNonSqlFiles(): void
    {
        file_put_contents("{$this->tmpDir}/00001.00__1.sql", "CREATE TABLE {$this->someTable} (col1 INT);");
        file_put_contents(
            "{$this->tmpDir}/00002.00__2.sql",
            "ALTER TABLE {$this->someTable} RENAME COLUMN col1 TO col2;",
        );
        file_put_contents(
            "{$this->tmpDir}/00003.00__3.txt",
            "ALTER TABLE {$this->someTable} RENAME TO another_table;",
        );

        $migrationHandler = static::getContainer()->get('test.App\Common\Db\MigrationHandler');
        $migrationHandler->setMigrationDir($this->tmpDir);
        $migrationHandler->migrate();

        $sql = "SELECT EXISTS (
                    SELECT *
                    FROM information_schema.tables
                    WHERE
                        table_schema = '{$this->schema}' AND
                        table_name   = 'another_table'
                )";
        $tableExist = $this->db->getOne($sql);
        self::assertFalse($tableExist);

        $maxVersion = $this->db->getOne("SELECT MAX(version) FROM {$this->migrationTable}");
        self::assertSame('00002.00', $maxVersion);
    }

    #[DataProvider('badFileNameProvider')]
    public function testMigrateBadFileName($file): void
    {
        self::expectException(MigrationException::class);
        self::expectExceptionCode(MigrationException::BAD_FILENAME);
        self::expectExceptionMessageMatches("/{$file}.sql/");

        file_put_contents("{$this->tmpDir}/{$file}.sql", "CREATE TABLE {$this->someTable} (col1 INT);");

        $migrationHandler = static::getContainer()->get('test.App\Common\Db\MigrationHandler');
        $migrationHandler->setMigrationDir($this->tmpDir);
        $migrationHandler->migrate();
    }

    public static function badFileNameProvider(): array
    {
        return [
            ['00001.00_'],
            ['00001.0__'],
            ['0000100__'],
            ['0001.00__'],
            ['00001.0s__'],
            ['000s1.00__'],
        ];
    }

    public function testReset(): void
    {
        file_put_contents("{$this->tmpDir}/00001.00__sql.sql", "CREATE TABLE {$this->someTable} (col1 INT);");

        $migrationHandler = static::getContainer()->get('test.App\Common\Db\MigrationHandler');
        $migrationHandler->setMigrationDir($this->tmpDir);
        $migrationHandler->migrate();

        $migrationHandler->reset();

        $sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name = '{$this->schema}'";
        self::assertSame(false, $this->db->getOne($sql));
    }

}
