DOCKER_COMPOSE_COMMAND = docker compose

help:
	@echo 'start stop restart test testunit install update bash'

start:
	$(DOCKER_COMPOSE_COMMAND) up -d --build
	$(DOCKER_COMPOSE_COMMAND) exec php composer install
stop:
	$(DOCKER_COMPOSE_COMMAND) stop
restart:
	$(DOCKER_COMPOSE_COMMAND) stop
	$(DOCKER_COMPOSE_COMMAND) up -d --build
	$(DOCKER_COMPOSE_COMMAND) exec php composer install
test:
	$(DOCKER_COMPOSE_COMMAND) exec php bin/phpunit --do-not-cache-result
testunit:
	$(DOCKER_COMPOSE_COMMAND) exec php bin/phpunit --do-not-cache-result tests/Unit
install:
	$(DOCKER_COMPOSE_COMMAND) exec php composer install
update:
	$(DOCKER_COMPOSE_COMMAND) exec php composer update
bash:
	$(DOCKER_COMPOSE_COMMAND) exec php bash
