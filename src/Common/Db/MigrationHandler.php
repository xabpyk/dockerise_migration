<?php

declare(strict_types=1);

namespace App\Common\Db;

class MigrationHandler
{
    private const DELIMITER = '__';

    private DbConnection $db;

    private string $schema;
    private string $migrationTable;
    private string $migrationDir;

    public function __construct(DbConnection $db, string $schema, string $migrationTable, string $migrationDir)
    {
        $this->db = $db;
        $this->schema = $schema;
        $this->migrationTable = $migrationTable;
        $this->migrationDir = $migrationDir;
    }

    public function setMigrationDir(string $dir): void
    {
        $this->migrationDir = $dir;
    }

    public function new(string $description = ''): void
    {
        if (!is_dir($this->migrationDir)) {
            mkdir($this->migrationDir, 0777, true);
        }

        $files = $this->getAllFiles();

        if (empty($files)) {
            $version = '00001.00';
        } else {
            $lastFile = end($files);
            $lastVersion = explode(self::DELIMITER, $lastFile)[0];
            $mainNumber = (int)ltrim(explode('.', $lastVersion)[0], '0');
            $mainNumber++;
            $version = str_pad((string)$mainNumber, 5, "0", STR_PAD_LEFT).'.00';
        }

        file_put_contents("{$this->migrationDir}/{$version}".self::DELIMITER."{$description}.sql", '');
    }

    public function migrate(): void
    {
        $this->initDbIfNeeded();

        $migrations = $this->getActualMigrations();

        if (empty($migrations)) {
            throw new MigrationException('', MigrationException::MIGRATIONS_NOT_FOUND);
        }

        $unitedFile = $this->getUnitedFile($migrations);

        $this->run($unitedFile);
    }

    private function initDbIfNeeded(): void
    {
        $this->db->query("CREATE SCHEMA IF NOT EXISTS {$this->schema}");

        $sql = "CREATE TABLE IF NOT EXISTS {$this->schema}.{$this->migrationTable} (
                    version VARCHAR(8) UNIQUE NOT NULL,
                    hash VARCHAR(32) NOT NULL
                )";
        $this->db->query($sql);
    }

    private function getActualMigrations(): array
    {
        $files = $this->getAllFiles();

        if (empty($files)) {
            return [];
        }

        $executedMigrations = $this->getExecutedMigrations();

        return $this->getUnexecutedMigrations($files, $executedMigrations);
    }

    private function getAllFiles(): array
    {
        if (!is_dir($this->migrationDir)) {
            return [];
        }

        $files = scandir($this->migrationDir);

        if ($files === false) {
            throw new MigrationException('Can`t read migration files', MigrationException::READ_MIGRATION_DIR);
        }

        if (count($files) === 2) {
            return [];
        }

        unset($files[0], $files[1]);

        return $files;
    }

    private function getExecutedMigrations(): array
    {
        $sql = "SELECT version, hash FROM {$this->schema}.{$this->migrationTable} ORDER BY version ASC";

        return $this->db->getAssoc($sql);
    }

    private function getUnexecutedMigrations(array $files, array $executedMigrations): array
    {
        $lastExecutedVersion = array_key_last($executedMigrations);

        $migrations = [];

        foreach ($files as $file) {
            $pathParts = pathinfo($file);

            if ($pathParts['extension'] !== 'sql') {
                continue;
            }

            if (!preg_match('/^\\d{5}.\\d{2}__/', $pathParts['filename'])) {
                throw new MigrationException("Bad filemane: '{$file}'", MigrationException::BAD_FILENAME);
            }

            $version = explode(self::DELIMITER, $file)[0];
            $hash = md5_file($this->migrationDir.'/'.$file);

            if (!empty($executedMigrations[$version]) && $executedMigrations[$version] !== $hash) {
                throw new MigrationException("Modified migration found: '{$file}'", MigrationException::BAD_HASH);
            }

            if (empty($executedMigrations[$version]) && $version <= $lastExecutedVersion) {
                throw new MigrationException("Missed migration found: '{$file}'", MigrationException::MISSED_MIGRATION);
            }

            if ($version <= $lastExecutedVersion) {
                unset($executedMigrations[$version]);
                continue;
            }

            if (!empty($migrations[$version])) {
                $msg = "Files conflict found: '{$migrations[$version]['file']}', '{$file}'";
                throw new MigrationException($msg, MigrationException::NEW_FILES_CONFLICT);
            }

            $migrations[$version] = [
                'file'    => $file,
                'hash'    => $hash,
            ];
        }

        if (!empty($executedMigrations)) {
            $missedFileVersion = array_key_first($executedMigrations);
            throw new MigrationException("Missed file found: '{$missedFileVersion}'", MigrationException::MISSED_FILE);
        }

        return $migrations;
    }

    private function getUnitedFile(array $migrations): string
    {
        $file = sys_get_temp_dir().'/mirgation_'.safeUniqId().'.sql';
        $fp = fopen($file, 'w');

        foreach ($migrations as $version => $migration) {
            fwrite($fp, file_get_contents($this->migrationDir.'/'.$migration['file']));

            $sql = "INSERT INTO {$this->schema}.{$this->migrationTable} (version, hash)
                    VALUES ('{$version}', '{$migration['hash']}')";
            fwrite($fp, "{$sql};\n");
        }
        fclose($fp);

        return $file;
    }

    private function run($file): void
    {
        $cmdParts = [
            'PGPASSWORD=' . $this->db->getPassword(),
            'psql',
            '--host='     . $this->db->getHost(),
            '--dbname='   . $this->db->getDatabase(),
            '--username=' . $this->db->getUsername(),
            '--port='     . $this->db->getPort(),
            '--single-transaction',
            '--file='     . $file,
            '--variable=ON_ERROR_STOP=1',
            '2>&1',
        ];

        $cmd = join(' ', $cmdParts);

        $output = [];
        $status = null;
        exec($cmd, $output, $status);

        if ($status !== 0) {
            $msg = join("\n", $output)."\npsql exit status = {$status}";
            throw new MigrationException($msg, MigrationException::SQL_EXEC_ERROR);
        }
    }

    public function reset(): void
    {
        $this->db->query("DROP SCHEMA IF EXISTS {$this->schema} CASCADE");
    }
}
