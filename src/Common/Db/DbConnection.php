<?php

declare(strict_types=1);

namespace App\Common\Db;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Result;
use PDO;

class DbConnection
{
    private $connection;

    public function __construct(Connection $connection, string $schema, string $timezone)
    {
        $this->connection = $connection;

        $this->query("SET search_path TO {$schema},public");
        $this->query("SET timezone TO '{$timezone}'");
    }

    public function query(string $sql): Result
    {
        return $this->connection->executeQuery($sql);
    }

    public function getAll(string $sql): array
    {
        return $this->connection->fetchAllAssociative($sql);
    }

    public function getRow(string $sql): array
    {
        return $this->connection->fetchAssociative($sql);
    }

    public function getCol(string $sql): array
    {
        return $this->connection->executeQuery($sql)->fetchFirstColumn();
    }

    public function getOne(string $sql)
    {
        return $this->connection->fetchOne($sql);
    }

    public function getAssoc(string $sql): array
    {
        $res = $this->connection->executeQuery($sql);

        $rows = [];

        foreach ($res->iterateAssociative() as $row) {
            $key = current($row);
            if (count($row) == 2) {
                $row = array_pop($row);
            } else {
                unset($row[key($row)]);
            }

            $rows[$key] = $row;
        }

        return $rows;
    }

    public function quote($value)
    {
        return $this->connection->quote($value);
    }

    public function getHost(): string
    {
        return $this->connection->getParams()['host'];
    }

    public function getPort(): string
    {
        return $this->connection->getParams()['port'];
    }

    public function getDatabase(): string
    {
        return $this->connection->getParams()['dbname'];
    }

    public function getUsername(): string
    {
        return $this->connection->getParams()['user'];
    }

    public function getPassword(): string
    {
        return $this->connection->getParams()['password'];
    }
}
