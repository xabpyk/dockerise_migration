<?php

declare(strict_types=1);

namespace App\Common\Db;

use RuntimeException;

class MigrationException extends RuntimeException
{
    public const MIGRATIONS_NOT_FOUND = 1;
    public const BAD_FILENAME = 2;
    public const BAD_HASH = 3;
    public const MISSED_FILE = 4;
    public const MISSED_MIGRATION = 5;
    public const NEW_FILES_CONFLICT = 6;
    public const READ_MIGRATION_DIR = 7;
    public const SQL_EXEC_ERROR = 8;
}
