<?php

function safeUniqId(): string
{
    return str_replace('.', '_', uniqid('', true));
}
