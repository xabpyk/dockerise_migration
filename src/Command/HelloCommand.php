<?php

declare(strict_types=1);

namespace App\Command;

use App\Common\Db\DbConnection;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:hello')]
class HelloCommand extends Command
{
    private DbConnection $db;

    public function __construct(DbConnection $db)
    {
        parent::__construct();

        $this->db = $db;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        echo $this->db->getOne("SELECT NOW()")."\n";

        return 0;
    }
}
