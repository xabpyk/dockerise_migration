<?php

declare(strict_types=1);

namespace App\Command;

use App\Command\Actions\MigrationAction;
use App\Common\Db\MigrationException;
use App\Common\Db\MigrationHandler;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:migration')]
class MigrationCommand extends Command
{
    private const ARGUMENT_ACTION = 'action';

    public function __construct(
        private MigrationHandler $migrationHandler,
    ){
        parent::__construct();
    }

    protected function configure()
    {
        $actions = array_map(
            fn($action) => $action->value,
            MigrationAction::cases(),
        );

        $this->addArgument(
            self::ARGUMENT_ACTION,
            InputArgument::OPTIONAL,
            "What action do you want to do? (" . join(', ', $actions) . ")",
            MigrationAction::MIGRATE->value,
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $action = MigrationAction::tryFrom($input->getArgument(self::ARGUMENT_ACTION));

        if (null === $action) {
            $output->writeln('Unknown action');

            return Command::FAILURE;
        }

        $success = match ($action) {
            MigrationAction::MIGRATE => $this->migrate($output),
            MigrationAction::NEW => $this->migrationHandler->new(),
            MigrationAction::RESET => $this->migrationHandler->reset(),
        };

        $success = $success ?? true;

        return $success ? Command::SUCCESS : Command::FAILURE;
    }

    private function migrate(OutputInterface $output): bool
    {
        try {
            $this->migrationHandler->migrate();
        } catch (MigrationException $e) {

            switch ($e->getCode()) {
                case MigrationException::MIGRATIONS_NOT_FOUND:
                    $msg = 'Migrations not found';
                    $success = true;
                    break;
                case MigrationException::SQL_EXEC_ERROR:
                    $msg = 'SQL ERROR: ' . $e->getMessage();
                    $success = false;
                    break;
                default:
                    throw $e;
            }

            $output->writeln($msg);

            return $success;
        }

        return true;
    }
}
