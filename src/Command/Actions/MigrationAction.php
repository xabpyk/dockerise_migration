<?php

declare(strict_types=1);

namespace App\Command\Actions;

enum MigrationAction: string
{
    case NEW     = 'new';
    case MIGRATE = 'migrate';
    case RESET   = 'reset';
}
