<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\Criteria;

use App\Module\Cat\Domain\Entity\Cat;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Breed;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Sex;
use DateInterval;

abstract readonly class AbstractCatRelationCriterion
{
    final protected const string MIN_FATHER_AGE = 'P6M';
    final protected const string MIN_MOTHER_AGE = 'P8M';
    final protected const string MAX_FATHER_AGE = 'P15Y';
    final protected const string MAX_MOTHER_AGE = 'P10Y';

    final protected static function birthDatesOfParentAndChildAreCompatible(Cat $parent, Cat $child) : bool
    {
        if (null === $parent->birthDate || null === $child->birthDate) {
            return true;
        }

        if (Sex::MALE === $parent->genetics->sex) {
            $maxParentAge = self::MAX_FATHER_AGE;
            $minParentAge = self::MIN_FATHER_AGE;
        } else {
            $maxParentAge = self::MAX_MOTHER_AGE;
            $minParentAge = self::MIN_MOTHER_AGE;
        }

        return $parent->birthDate >= $child->birthDate->sub(new DateInterval($maxParentAge)) &&
            $parent->birthDate <= $child->birthDate->sub(new DateInterval($minParentAge));
    }

    final protected static function birthDatesOfBothParentAreCompatible(Cat $father, Cat $mother): bool
    {
        if (null === $father->birthDate || null === $mother->birthDate) {
            return true;
        }

        $minCompatibleFatherBirthDate = $mother->birthDate
            ->add(new DateInterval(self::MIN_MOTHER_AGE))
            ->sub(new DateInterval(self::MAX_FATHER_AGE));
        $maxCompatibleFatherBirthDate = $mother->birthDate
            ->add(new DateInterval(self::MAX_MOTHER_AGE))
            ->sub(new DateInterval(self::MIN_FATHER_AGE));

        return $father->birthDate >= $minCompatibleFatherBirthDate &&
            $father->birthDate <= $maxCompatibleFatherBirthDate;
    }

    protected static function parentsBreedsAreCompatible(
        Breed $childBreed,
        ?Breed $firstParentBreed,
        ?Breed $secondParentBreed = null,
    ): bool {
        return match ($childBreed) {
            Breed::PEB => self::pebParentsBreedsAreCompatible($firstParentBreed, $secondParentBreed),
            Breed::SIA, Breed::OSH => self::shortHairOrientalParentsBreedsAreCompatible(
                $childBreed,
                $firstParentBreed,
                $secondParentBreed,
            ),
            Breed::BAL, Breed::OLH => self::longHairOrientalParentsBreedsAreCompatible(
                $childBreed,
                $firstParentBreed,
                $secondParentBreed,
            ),
        };
    }

    private static function pebParentsBreedsAreCompatible(?Breed $firstParentBreed, ?Breed $secondParentBreed): bool
    {
        if (
            null !== $firstParentBreed && !in_array($firstParentBreed, [Breed::PEB, Breed::SIA, Breed::OSH], true) ||
            null !== $secondParentBreed && !in_array($secondParentBreed, [Breed::PEB, Breed::SIA, Breed::OSH], true)
        ) {
            return false;
        }

        if (in_array(null, [$firstParentBreed, $secondParentBreed], true)) {
            return true;
        }

        return in_array(Breed::PEB, [$firstParentBreed, $secondParentBreed], true);
    }

    private static function shortHairOrientalParentsBreedsAreCompatible(
        Breed $childBreed,
        ?Breed $firstParentBreed,
        ?Breed $secondParentBreed,
    ): bool {
        if (
            null !== $firstParentBreed && !$firstParentBreed->isShortHairOriental() ||
            null !== $secondParentBreed && !$secondParentBreed->isShortHairOriental()
        ) {
            return false;
        }

        if (in_array(null, [$firstParentBreed, $secondParentBreed], true)) {
            return true;
        }

        return !(Breed::OSH === $childBreed && Breed::SIA === $firstParentBreed && Breed::SIA === $secondParentBreed);
    }

    private static function longHairOrientalParentsBreedsAreCompatible(
        Breed $childBreed,
        ?Breed $firstParentBreed,
        ?Breed $secondParentBreed,
    ): bool {
        if (
            null !== $firstParentBreed && !$firstParentBreed->isOriental() ||
            null !== $secondParentBreed && !$secondParentBreed->isOriental()
        ) {
            return false;
        }

        if (in_array(null, [$firstParentBreed, $secondParentBreed], true)) {
            return true;
        }

        return ($firstParentBreed->isLongHairOriental() || $secondParentBreed->isLongHairOriental()) &&
            !(Breed::OLH === $childBreed && $firstParentBreed->isBalOrSia() && $secondParentBreed->isBalOrSia());
    }
}
