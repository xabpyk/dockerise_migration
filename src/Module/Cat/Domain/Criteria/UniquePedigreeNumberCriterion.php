<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\Criteria;

use App\Module\Cat\Domain\DependencyInterface\CatRepositoryInterface;
use App\Module\Cat\Domain\Entity\Cat;
use App\Module\Cat\Domain\Exception\CatValidationException;

final readonly class UniquePedigreeNumberCriterion implements UniquePedigreeNumberCriterionInterface
{
    public function __construct(
        private CatRepositoryInterface $repository,
    ) {
    }

    public function ensureIsSatisfiedBy(Cat $cat): void
    {
        if ($this->repository->catWithSamePedigreeNumberExists($cat)) {
            CatValidationException::throwPedigreeNumberIsNotUniqueException();
        }
    }
}
