<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\Criteria;

interface CatHasCorrectChildrenCriterionInterface extends CatCriterionInterface
{
}
