<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\Criteria;

use App\Module\Cat\Domain\DependencyInterface\CatRepositoryInterface;
use App\Module\Cat\Domain\Entity\Cat;
use App\Module\Cat\Domain\Exception\CatValidationException;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Breed;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Sex;

final readonly class CatHasCorrectChildrenCriterion
    extends AbstractCatRelationCriterion
    implements CatHasCorrectParentsCriterionInterface
{
    public function __construct(
        private CatRepositoryInterface $repository,
    ) {
    }

    public function ensureIsSatisfiedBy(Cat $cat): void
    {
        $otherParentIds = [];
        $childrenWithOtherParent = [];
        foreach ($this->repository->getChildren($cat->id) as $child) {
            self::ensureChildParentHasCorrectSex($cat, $child);
            self::ensureChildHasCompatibleBirthDate($cat, $child);
            self::ensureChildHasCompatibleBreed($cat->genetics->breed, $child->genetics->breed);

            $otherParentId = Sex::MALE === $cat->genetics->sex ? $child->getMotherId() : $child->getFatherId();
            if (null !== $otherParentId) {
                $otherParentIds[] = $otherParentId;
                $childrenWithOtherParent[] = $child;
            }
        }

        if (empty($otherParentIds)) {
            return;
        }

        $otherParents = [];
        foreach ($this->repository->getCatsByIds(...array_unique($otherParentIds)) as $otherParent) {
            $otherParents[$otherParent->id] = $otherParent;
        }

        self::ensureChildrenHasCompatibleOtherParents($cat, $childrenWithOtherParent, $otherParents);
    }

    private static function ensureChildParentHasCorrectSex(Cat $cat, Cat $child): void
    {
        if (
            Sex::MALE === $cat->genetics->sex && $child->getMotherId() === $cat->id ||
            Sex::FEMALE === $cat->genetics->sex && $child->getFatherId() === $cat->id
        ) {
            CatValidationException::throwForbiddenToChangeSexOfParentWhoHasChildException();
        }
    }

    private static function ensureChildHasCompatibleBirthDate(Cat $cat, Cat $child): void
    {
        if (null === $child->birthDate || null === $cat->birthDate) {
            return;
        }

        if (!self::birthDatesOfParentAndChildAreCompatible($cat, $child)) {
            CatValidationException::throwChildrenHasIncompatibleBirthDateException();
        }
    }

    private static function ensureChildHasCompatibleBreed(Breed $parentBreed, Breed $childBreed): void
    {
        if (!self::parentsBreedsAreCompatible($childBreed, $parentBreed)) {
            CatValidationException::throwChildHasIncompatibleBreedException();
        }
    }

    /**
     * @param Cat $cat
     * @param Cat[] $childrenWithOtherParent
     * @param Cat[] $otherParents
     */
    private static function ensureChildrenHasCompatibleOtherParents(
        Cat $cat,
        array $childrenWithOtherParent,
        array $otherParents,
    ): void {
        foreach ($childrenWithOtherParent as $child) {
            $father = Sex::MALE === $cat->genetics->sex ? $cat : $otherParents[$child->getFatherId()];
            $mother = Sex::FEMALE === $cat->genetics->sex ? $cat : $otherParents[$child->getMotherId()];

            if (!self::birthDatesOfBothParentAreCompatible($father, $mother)) {
                CatValidationException::throwOtherParentOfChildHasIncompatibleBirthDateException();
            }

            if (
                !self::parentsBreedsAreCompatible(
                    $child->genetics->breed,
                    $father->genetics->breed,
                    $mother->genetics->breed,
                )
            ) {
                CatValidationException::throwChildAndOtherParentHasIncompatibleBreedsException();
            }
        }
    }
}
