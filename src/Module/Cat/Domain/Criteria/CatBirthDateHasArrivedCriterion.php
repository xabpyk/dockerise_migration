<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\Criteria;

use App\Module\Cat\Domain\Entity\Cat;
use App\Module\Cat\Domain\Exception\CatValidationException;
use DateTimeImmutable;

final readonly class CatBirthDateHasArrivedCriterion implements CatBirthDateHasArrivedCriterionInterface
{
    public function __construct(
        private DateTimeImmutable $today
    ) {
    }

    public function ensureIsSatisfiedBy(Cat $cat): void
    {
        if (null !== $cat->birthDate && $cat->birthDate > $this->today) {
            CatValidationException::throwBirthDateHasNotArrivedException();
        }
    }
}
