<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\Criteria;

use App\Module\Cat\Domain\Entity\Cat;

interface CatCriterionInterface
{
    public function ensureIsSatisfiedBy(Cat $cat): void;
}
