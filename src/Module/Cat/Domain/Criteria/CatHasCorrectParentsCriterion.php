<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\Criteria;

use App\Module\Cat\Domain\DependencyInterface\CatRepositoryInterface;
use App\Module\Cat\Domain\Entity\Cat;
use App\Module\Cat\Domain\Exception\CatValidationException;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Breed;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Sex;

final readonly class CatHasCorrectParentsCriterion
    extends AbstractCatRelationCriterion
    implements CatHasCorrectParentsCriterionInterface
{
    public function __construct(
        private CatRepositoryInterface $repository,
    ) {
    }

    public function ensureIsSatisfiedBy(Cat $cat): void
    {
        if (null === $cat->getFatherId() && null === $cat->getMotherId()) {
            return;
        }

        /**
         * @var Cat $father
         * @var Cat $mother
         */
        ['father' => $father, 'mother' => $mother] = $this->getParents($cat);

        self::ensureParentsHaveCorrectSex($father, $mother);
        self::ensureParentsHaveCompatibleBirthDate($cat, $father, $mother);
        self::ensureParentsHaveCompatibleBreed(
            $cat->genetics->breed,
            $father?->genetics->breed,
            $mother?->genetics->breed,
        );
    }

    /**
     * @return Cat[]
     */
    private function getParents(Cat $cat): array
    {
        $parentIds = array_filter([$cat->getFatherId(), $cat->getMotherId()]);

        $parentIterator = $this->repository->getCatsByIds(...$parentIds);

        $existingParentIds = [];
        $parents = ['father' => null, 'mother' => null];
        foreach ($parentIterator as $parent) {
            $key = $cat->getFatherId() === $parent->id ? 'father' : 'mother';
            $parents[$key] = $parent;
            $existingParentIds[] = $parent->id;
        }

        if (count($parentIds) !== count($existingParentIds)) {
            CatValidationException::throwParentDoesNotExistException(...array_diff($parentIds, $existingParentIds));
        }

        return $parents;
    }

    private static function ensureParentsHaveCorrectSex(?Cat $father, ?Cat $mother): void
    {
        if (
            null !== $father && Sex::MALE !== $father->genetics->sex ||
            null !== $mother && Sex::FEMALE !== $mother->genetics->sex
        ) {
            CatValidationException::throwParentHasIncorrectSexException();
        }
    }

    private static function ensureParentsHaveCompatibleBirthDate(Cat $cat, ?Cat $father, ?Cat $mother): void
    {
        if (
            null !== $father && !self::birthDatesOfParentAndChildAreCompatible($father, $cat) ||
            null !== $mother && !self::birthDatesOfParentAndChildAreCompatible($mother, $cat)
        ) {
            CatValidationException::throwParentHasIncompatibleBirthDateException();
        }

        if (null === $father || null === $mother) {
            return;
        }

        if (!self::birthDatesOfBothParentAreCompatible($father, $mother)) {
            CatValidationException::throwParentHasIncompatibleBirthDateException();
        }
    }

    private static function ensureParentsHaveCompatibleBreed(
        Breed $childBreed,
        ?Breed $firstParentBreed,
        ?Breed $secondParentBreed,
    ): void {
        if (!self::parentsBreedsAreCompatible($childBreed, $firstParentBreed, $secondParentBreed)) {
            CatValidationException::throwParentHasIncompatibleBreedException();
        }
    }
}
