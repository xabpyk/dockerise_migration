<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\Exception;

use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Breed;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Color;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\EyeColor;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Pattern;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\SilverSmoke;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\WhiteAmount;
use RuntimeException;

final class CatValidationException extends RuntimeException
{
    public const int NAME_MIN_LENGTH_ERR = 101;
    public const int NAME_MAX_LENGTH_ERR = 102;
    public const int NAME_STARTED_OR_ENDED_WITH_SPACE_ERR = 103;
    public const int NAME_DOES_NOT_CONTAIN_SPACE_ERR = 104;
    public const int NAME_SPACE_IN_FIRST_FEW_CHARACTERS_ERR = 105;
    public const int NAME_IS_NOT_UNIQUE_ERR = 106;

    public const int PEDIGREE_NUMBER_MIN_LENGTH_ERR = 151;
    public const int PEDIGREE_NUMBER_MAX_LENGTH_ERR = 152;
    public const int PEDIGREE_NUMBER_WHITESPACE_ON_EDGE_ERR = 153;
    public const int PEDIGREE_NUMBER_CONTAINS_INVALID_CHARACTER_ERR = 154;
    public const int PEDIGREE_NUMBER_DOES_NOT_CONTAIN_ANY_DIGITS_ERR = 155;
    public const int PEDIGREE_NUMBER_IS_NOT_UNIQUE_ERR = 156;

    public const int ALLELES_ORANGE_TWO_Y_ERR = 206;

    public const int GENETIC_MALE_CANT_BE_TORTIE_ERR = 251;
    public const int GENETIC_FORBIDDEN_SIAMESE_POINTED_AND_ORIENTAL_BREED_ERR = 252;
    public const int GENETIC_FORBIDDEN_SIAMESE_POINTED_AND_WHITE_COLOR_ERR = 253;
    public const int GENETIC_FORBIDDEN_CARAMEL_AND_COLOR_ERR = 254;
    public const int GENETIC_FORBIDDEN_SILVER_AND_BREED_ERR = 255;
    public const int GENETIC_FORBIDDEN_SILVER_AND_WHITE_COLOR_ERR = 256;
    public const int GENETIC_FORBIDDEN_UNSPECIFIED_WHITE_AMOUNT_AND_ORIENTAL_BREED_ERR = 257;
    public const int GENETIC_FORBIDDEN_WHITE_AMOUNT_AND_WHITE_COLOR_ERR = 258;
    public const int GENETIC_FORBIDDEN_SPECIFIED_PATTERN_AND_SIA_OR_BAL_ERR = 259;
    public const int GENETIC_FORBIDDEN_WHITE_AMOUNT_AND_PATTERN_ERR = 260;
    public const int GENETIC_FORBIDDEN_SPECIFIED_PATTERN_AND_SIAMESE_POINTED_ERR = 261;
    public const int GENETIC_FORBIDDEN_UNSPECIFIED_PATTERN_AND_BICOLOUR_WHITE_AMOUNT_AND_OLH_OR_OSH_ERR = 262;
    public const int GENETIC_FORBIDDEN_EYE_COLOR_AND_BREED_ERR = 263;
    public const int GENETIC_FORBIDDEN_NO_EYE_COLOR_AND_WHITE_COLOR_ERR = 264;
    public const int GENETIC_FORBIDDEN_EYE_COLOR_AND_SIAMESE_POINTED_ERR = 265;
    public const int GENETIC_FORBIDDEN_EYE_COLOR_AND_PATTERN_ERR = 266;

    public const int BIRTH_DATE_IS_BEFORE_1800_ERR = 301;
    public const int BIRTH_DATE_HAS_NOT_ARRIVED_ERR = 302;

    public const int PARENT_ID_IS_SAME_AS_CAT_ID_ERR = 401;
    public const int PARENT_IDS_ARE_SAME_ERR = 402;
    public const int PARENT_DOES_NOT_EXIST = 403;
    public const int PARENT_HAS_INCORRECT_SEX = 404;
    public const int PARENT_HAS_INCOMPATIBLE_BIRTH_DATE = 405;
    public const int PARENT_HAS_INCOMPATIBLE_BREED = 406;

    public const int FORBIDDEN_TO_CHANGE_SEX_OF_PARENT_WHO_HAS_CHILD = 451;
    public const int CHILDREN_HAS_INCOMPATIBLE_BIRTH_DATE = 452;
    public const int OTHER_PARENT_OF_CHILD_HAS_INCOMPATIBLE_BIRTH_DATE = 453;
    public const int CHILD_HAS_INCOMPATIBLE_BREED = 454;
    public const int CHILD_AND_OTHER_PARENT_HAS_INCOMPATIBLE_BREEDS = 455;

    public static function throwCatNameMinLengthViolatedException(int $minLength): void
    {
        throw new self("Min length of cat name must be {$minLength}", self::NAME_MIN_LENGTH_ERR);
    }

    public static function throwCatNameMaxLengthViolatedException(int $maxLength): void
    {
        throw new self("Max length of cat name must be {$maxLength}", self::NAME_MAX_LENGTH_ERR);
    }

    public static function throwCatNameHasWhitespaceOnEdgeException(): void
    {
        throw new self("Cat name can't start or end with a space", self::NAME_STARTED_OR_ENDED_WITH_SPACE_ERR);
    }

    public static function throwCatNameDoesNotContainSpaceException(): void
    {
        throw new self('Cat name must contain space', self::NAME_DOES_NOT_CONTAIN_SPACE_ERR);
    }

    public static function throwCatNameWithSpaceInFirstFewCharactersException(int $minLengthWithoutSpace): void
    {
        throw new self(
            "Cat name must not contain a space in the first {$minLengthWithoutSpace} characters",
            self::NAME_SPACE_IN_FIRST_FEW_CHARACTERS_ERR,
        );
    }

    public static function throwCatNameIsNotUniqueException(): void
    {
        throw new self("Cat name must be unique", self::NAME_IS_NOT_UNIQUE_ERR);
    }

    public static function throwPedigreeNumberMinLengthViolatedException(int $minLength): void
    {
        throw new self("Min length of pedigree number must be {$minLength}", self::PEDIGREE_NUMBER_MIN_LENGTH_ERR);
    }

    public static function throwPedigreeNumberMaxLengthViolatedException(int $maxLength): void
    {
        throw new self("Max length of pedigree number must be {$maxLength}", self::PEDIGREE_NUMBER_MAX_LENGTH_ERR);
    }

    public static function throwPedigreeNumberHasWhitespaceOnEdgeException(): void
    {
        throw new self("Pedigree number can't start or end with a space", self::PEDIGREE_NUMBER_WHITESPACE_ON_EDGE_ERR);
    }

    public static function throwPedigreeNumberContainsInvalidCharacterException(): void
    {
        throw new self(
            "Pedigree number contains invalid character",
            self::PEDIGREE_NUMBER_CONTAINS_INVALID_CHARACTER_ERR,
        );
    }

    public static function throwPedigreeNumberDoesNotContainAnyDigitsException(): void
    {
        throw new self(
            "Pedigree number must contain at least one digit",
            self::PEDIGREE_NUMBER_DOES_NOT_CONTAIN_ANY_DIGITS_ERR,
        );
    }

    public static function throwPedigreeNumberIsNotUniqueException(): void
    {
        throw new self("Pedigree number must be unique", self::PEDIGREE_NUMBER_IS_NOT_UNIQUE_ERR);
    }

    public static function throwAllelesOrangeHasTwoYException(): void
    {
        throw new self("Orange alleles can't contain two 'Y'", self::ALLELES_ORANGE_TWO_Y_ERR);
    }

    public static function throwMaleCantBeTortieException(): void
    {
        throw new self("Male can't be tortie", self::GENETIC_MALE_CANT_BE_TORTIE_ERR);
    }

    public static function throwForbiddenColorPointAndOrientalBreedException(Breed $breed): void
    {
        throw new self(
            "{$breed->name} can't be siamese pointed",
            self::GENETIC_FORBIDDEN_SIAMESE_POINTED_AND_ORIENTAL_BREED_ERR,
        );
    }

    public static function throwForbiddenColorPointAndWhiteColorException(): void
    {
        throw new self(
            "White color and siamese pointed can't be together",
            self::GENETIC_FORBIDDEN_SIAMESE_POINTED_AND_WHITE_COLOR_ERR,
        );
    }

    public static function throwForbiddenCaramelAndColorException(Color $color): void
    {
        throw new self(
            "{$color->name} and caramel can't be together",
            self::GENETIC_FORBIDDEN_CARAMEL_AND_COLOR_ERR,
        );
    }

    public static function throwForbiddenSilverAndBreedException(Breed $breed): void
    {
        throw new self(
            "{$breed->name} can't be silver or smoke",
            self::GENETIC_FORBIDDEN_SILVER_AND_BREED_ERR,
        );
    }

    public static function throwForbiddenSilverAndWhiteColorException(SilverSmoke $silverSmoke): void
    {
        throw new self(
            "White color and {$silverSmoke->name} can't be together",
            self::GENETIC_FORBIDDEN_SILVER_AND_WHITE_COLOR_ERR,
        );
    }

    public static function throwForbiddenUnspecifiedWhiteAmountAndOrientalBreedException(Breed $breed): void
    {
        throw new self(
            "{$breed->name} can't be with unspecified white amount",
            self::GENETIC_FORBIDDEN_UNSPECIFIED_WHITE_AMOUNT_AND_ORIENTAL_BREED_ERR,
        );
    }

    public static function throwForbiddenWhiteAmountAndWhiteColorException(WhiteAmount $whiteAmount): void
    {
        throw new self(
            "{$whiteAmount->name} can't be with white color",
            self::GENETIC_FORBIDDEN_WHITE_AMOUNT_AND_WHITE_COLOR_ERR,
        );
    }

    public static function throwForbiddenSpecifiedPatternAndSiaOrBalException(Pattern $pattern, Breed $breed): void
    {
        throw new self(
            "{$breed->name} can't be {$pattern->name}",
            self::GENETIC_FORBIDDEN_SPECIFIED_PATTERN_AND_SIA_OR_BAL_ERR,
        );
    }

    public static function throwForbiddenWhiteAmountAndPatternException(Pattern $pattern): void
    {
        throw new self(
            "{$pattern->name} can't be with white color",
            self::GENETIC_FORBIDDEN_WHITE_AMOUNT_AND_PATTERN_ERR,
        );
    }

    public static function throwForbiddenSpecifiedPatternAndSiamesePointedException(Pattern $pattern): void
    {
        throw new self(
            "{$pattern->name} can't be with siamese pointed",
            self::GENETIC_FORBIDDEN_SPECIFIED_PATTERN_AND_SIAMESE_POINTED_ERR,
        );
    }

    public static function throwForbiddenUnspecifiedPatternAndBicolourWhiteAmountAndOlhOrOshException(
        Breed $breed,
    ): void {
        throw new self(
            "{$breed->name} can't be with bicolour white amount and with unspecified pattern",
            self::GENETIC_FORBIDDEN_UNSPECIFIED_PATTERN_AND_BICOLOUR_WHITE_AMOUNT_AND_OLH_OR_OSH_ERR,
        );
    }

    public static function throwForbiddenEyeColorAndBreedException(EyeColor $eyeColor, Breed $breed): void
    {
        throw new self("{$breed->name} can't be {$eyeColor->name}", self::GENETIC_FORBIDDEN_EYE_COLOR_AND_BREED_ERR);
    }

    public static function throwForbiddenNoEyeColorAndWhiteColorException(): void
    {
        throw new self(
            "Eye color must be specified if white color is specified",
            self::GENETIC_FORBIDDEN_NO_EYE_COLOR_AND_WHITE_COLOR_ERR,
        );
    }

    public static function throwForbiddenEyeColorAndSiamesePointedException(EyeColor $eyeColor): void
    {
        throw new self(
            "{$eyeColor->name} can't be with siamese pointed",
            self::GENETIC_FORBIDDEN_EYE_COLOR_AND_SIAMESE_POINTED_ERR,
        );
    }

    public static function throwForbiddenEyeColorAndPatternException(EyeColor $eyeColor, Pattern $pattern): void
    {
        throw new self(
            "{$eyeColor->name} can't be with {$pattern->name}",
            self::GENETIC_FORBIDDEN_EYE_COLOR_AND_PATTERN_ERR,
        );
    }

    public static function throwBirthDateIsBefore1800Exception(): void
    {
        throw new self("Birth date can't be before 1800", self::BIRTH_DATE_IS_BEFORE_1800_ERR);
    }

    public static function throwBirthDateHasNotArrivedException(): void
    {
        throw new self("The date of birth has not yet arrived", self::BIRTH_DATE_HAS_NOT_ARRIVED_ERR);
    }

    public static function throwCatParentIdIsSameAsCatIdException(): void
    {
        throw new self("Parent id can't be the same as cat id", self::PARENT_ID_IS_SAME_AS_CAT_ID_ERR);
    }

    public static function throwCatParentIdsAreSameException(): void
    {
        throw new self("Parent's ids can't be the same", self::PARENT_IDS_ARE_SAME_ERR);
    }

    public static function throwParentDoesNotExistException(int ...$ids): void
    {
        throw new self("Parents with id: [" . join(', ', $ids) . "] do not exist", self::PARENT_DOES_NOT_EXIST);
    }

    public static function throwParentHasIncorrectSexException(): void
    {
        throw new self("One or both parents have incorrect sex", self::PARENT_HAS_INCORRECT_SEX);
    }

    public static function throwParentHasIncompatibleBirthDateException(): void
    {
        throw new self(
            "Cat and one or both its parents have incompatible dates of birth",
            self::PARENT_HAS_INCOMPATIBLE_BIRTH_DATE,
        );
    }

    public static function throwParentHasIncompatibleBreedException(): void
    {
        throw new self(
            "Cat and one or both its parents have incompatible breeds",
            self::PARENT_HAS_INCOMPATIBLE_BREED,
        );
    }

    public static function throwForbiddenToChangeSexOfParentWhoHasChildException(): void
    {
        throw new self(
            "It is forbidden to change the sex of a parent who has child",
            self::FORBIDDEN_TO_CHANGE_SEX_OF_PARENT_WHO_HAS_CHILD,
        );
    }

    public static function throwChildrenHasIncompatibleBirthDateException(): void
    {
        throw new self(
            "Cat and one or more of his children have incompatible dates of birth",
            self::CHILDREN_HAS_INCOMPATIBLE_BIRTH_DATE,
        );
    }

    public static function throwOtherParentOfChildHasIncompatibleBirthDateException(): void
    {
        throw new self(
            "Date of birth is incompatible with the date of birth of the other parent of the common child",
            self::OTHER_PARENT_OF_CHILD_HAS_INCOMPATIBLE_BIRTH_DATE,
        );
    }

    public static function throwChildHasIncompatibleBreedException(): void
    {
        throw new self("Child has incompatible breed", self::CHILD_HAS_INCOMPATIBLE_BREED);
    }

    public static function throwChildAndOtherParentHasIncompatibleBreedsException(): void
    {
        throw new self(
            "Child and its other parent has incompatible breeds",
            self::CHILD_AND_OTHER_PARENT_HAS_INCOMPATIBLE_BREEDS,
        );
    }
}
