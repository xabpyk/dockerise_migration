<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\Service;

use App\Module\Cat\Domain\DependencyInterface\CatRepositoryInterface;
use App\Module\Cat\Domain\Entity\CatFactoryInterface;
use App\Module\Cat\Domain\ValueObject\CatName;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Sex;
use App\Module\Cat\Domain\ValueObject\Genetics;
use App\Module\Cat\Domain\ValueObject\PedigreeNumber;
use DateTimeImmutable;

final class CatHandler
{
    public function __construct(
        private readonly CatRepositoryInterface $catRepository,
        private readonly CatFactoryInterface $catFactory,
    ) {
    }

    public function add(
        CatName $catName,
        Genetics $genetics,
        ?DateTimeImmutable $birthDate = null,
        ?PedigreeNumber $pedigreeNumber = null,
        ?int $fatherId = null,
        ?int $motherId = null,
    ): void {
        $cat = $this->catFactory->create(
            $this->catRepository->nextId(),
            $catName,
            $genetics,
            $birthDate,
            $pedigreeNumber,
            $fatherId,
            $motherId,
        );

        $this->catRepository->add($cat);
    }

    public function update(
        int $id,
        CatName $catName,
        Genetics $genetics,
        ?DateTimeImmutable $birthDate = null,
        ?PedigreeNumber $pedigreeNumber = null,
        ?int $fatherId = null,
        ?int $motherId = null,
    ): void {
        $cat = $this->catFactory->create(
            $id,
            $catName,
            $genetics,
            $birthDate,
            $pedigreeNumber,
            $fatherId,
            $motherId,
        );

        $this->catRepository->update($cat);
    }

    public function remove(int $id): void
    {
        $childrenIterator = $this->catRepository->getChildren($id);

        if (is_array($childrenIterator) ? !empty($childrenIterator) : $childrenIterator->valid()) {
            $cat = $this->catRepository->getCatsById($id);

            $children = [];
            foreach ($childrenIterator as $child) {
                Sex::MALE === $cat->genetics->sex ? $child->unsetFather() : $child->unsetMother();

                $children[] = $child;
            }

            $this->catRepository->update(...$children);
        }

        $this->catRepository->remove($id);
    }
}
