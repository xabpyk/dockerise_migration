<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\Entity;

use App\Module\Cat\Domain\Criteria\CatBirthDateHasArrivedCriterionInterface;
use App\Module\Cat\Domain\Criteria\CatHasCorrectChildrenCriterionInterface;
use App\Module\Cat\Domain\Criteria\CatHasCorrectParentsCriterionInterface;
use App\Module\Cat\Domain\Criteria\UniqueCatNameCriterionInterface;
use App\Module\Cat\Domain\Criteria\UniquePedigreeNumberCriterionInterface;
use App\Module\Cat\Domain\ValueObject\CatName;
use App\Module\Cat\Domain\ValueObject\Genetics;
use App\Module\Cat\Domain\ValueObject\PedigreeNumber;
use DateTimeImmutable;

final readonly class CatFactory implements CatFactoryInterface
{
    public function __construct(
        private CatBirthDateHasArrivedCriterionInterface $catBirthDateHasArrivedCriterion,
        private UniqueCatNameCriterionInterface $uniqueCatNameCriterion,
        private UniquePedigreeNumberCriterionInterface $uniquePedigreeNumberCriterion,
        private CatHasCorrectParentsCriterionInterface $catHasCorrectParentsCriterion,
        private CatHasCorrectChildrenCriterionInterface $catHasCorrectChildrenCriterion,
    ) {
    }

    public function create(
        int $id,
        CatName $name,
        Genetics $genetics,
        ?DateTimeImmutable $birthDate = null,
        ?PedigreeNumber $pedigreeNumber = null,
        ?int $fatherId = null,
        ?int $motherId = null,
    ): Cat {
        $cat = new Cat($id, $name, $genetics, $birthDate, $pedigreeNumber, $fatherId, $motherId);

        $this->catBirthDateHasArrivedCriterion->ensureIsSatisfiedBy($cat);
        $this->uniqueCatNameCriterion->ensureIsSatisfiedBy($cat);
        $this->uniquePedigreeNumberCriterion->ensureIsSatisfiedBy($cat);
        $this->catHasCorrectParentsCriterion->ensureIsSatisfiedBy($cat);
        $this->catHasCorrectChildrenCriterion->ensureIsSatisfiedBy($cat);

        return $cat;
    }
}
