<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\Entity;

use App\Module\Cat\Domain\ValueObject\CatName;
use App\Module\Cat\Domain\ValueObject\Genetics;
use App\Module\Cat\Domain\ValueObject\PedigreeNumber;
use DateTimeImmutable;

interface CatFactoryInterface
{
    public function create(
        int $id,
        CatName $name,
        Genetics $genetics,
        ?DateTimeImmutable $birthDate = null,
        ?PedigreeNumber $pedigreeNumber = null,
        ?int $fatherId = null,
        ?int $motherId = null,
    ): Cat;
}
