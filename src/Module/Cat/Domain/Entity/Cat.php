<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\Entity;

use App\Module\Cat\Domain\Exception\CatValidationException;
use App\Module\Cat\Domain\ValueObject\CatName;
use App\Module\Cat\Domain\ValueObject\Genetics;
use App\Module\Cat\Domain\ValueObject\PedigreeNumber;
use DateTimeImmutable;

final class Cat
{
    public function __construct(
        public readonly int $id,
        public readonly CatName $name,
        public readonly Genetics $genetics,
        public readonly ?DateTimeImmutable $birthDate = null,
        public readonly ?PedigreeNumber $pedigreeNumber = null,
        private ?int $fatherId = null,
        private ?int $motherId = null,
    ) {
        $this->ensureCatParamsIsValid();
    }

    private function ensureCatParamsIsValid(): void
    {
        if (null !== $this->birthDate && $this->birthDate < new DateTimeImmutable('1800-01-01')) {
            CatValidationException::throwBirthDateIsBefore1800Exception();
        }

        if (in_array($this->id, [$this->fatherId, $this->motherId], true)) {
            CatValidationException::throwCatParentIdIsSameAsCatIdException();
        }

        if (null !== $this->fatherId && null !== $this->motherId && $this->fatherId === $this->motherId) {
            CatValidationException::throwCatParentIdsAreSameException();
        }
    }

    public function getFatherId(): ?int
    {
        return $this->fatherId;
    }

    public function getMotherId(): ?int
    {
        return $this->motherId;
    }

    public function unsetFather(): void
    {
        $this->fatherId = null;
    }

    public function unsetMother(): void
    {
        $this->motherId = null;
    }
}
