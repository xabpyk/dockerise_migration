<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject;

use App\Module\Cat\Domain\Exception\CatValidationException;
use App\Module\Cat\Domain\ValueObject\Allele\AgutiAllele;
use App\Module\Cat\Domain\ValueObject\Allele\BlackAllele;
use App\Module\Cat\Domain\ValueObject\Allele\ColorAllele;
use App\Module\Cat\Domain\ValueObject\Allele\DiluteModifierAllele;
use App\Module\Cat\Domain\ValueObject\Allele\DilutorAllele;
use App\Module\Cat\Domain\ValueObject\Allele\OrangeAllele;
use App\Module\Cat\Domain\ValueObject\Allele\PraAllele;
use App\Module\Cat\Domain\ValueObject\Allele\SilverAllele;
use App\Module\Cat\Domain\ValueObject\Allele\TabbyAllele;
use App\Module\Cat\Domain\ValueObject\Allele\WhiteAllele;
use App\Module\Cat\Domain\ValueObject\Allele\WithWhiteAllele;
use App\Module\Cat\Domain\ValueObject\Alleles\AgutiAlleles;
use App\Module\Cat\Domain\ValueObject\Alleles\BlackAlleles;
use App\Module\Cat\Domain\ValueObject\Alleles\ColorAlleles;
use App\Module\Cat\Domain\ValueObject\Alleles\DiluteModifierAlleles;
use App\Module\Cat\Domain\ValueObject\Alleles\DilutorAlleles;
use App\Module\Cat\Domain\ValueObject\Alleles\OrangeAlleles;
use App\Module\Cat\Domain\ValueObject\Alleles\PraAlleles;
use App\Module\Cat\Domain\ValueObject\Alleles\SilverAlleles;
use App\Module\Cat\Domain\ValueObject\Alleles\TabbyAlleles;
use App\Module\Cat\Domain\ValueObject\Alleles\WhiteAlleles;
use App\Module\Cat\Domain\ValueObject\Alleles\WithWhiteAlleles;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Breed;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Color;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\EyeColor;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Pattern;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\PRA;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\Sex;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\SilverSmoke;
use App\Module\Cat\Domain\ValueObject\ExternalCharacteristic\WhiteAmount;

final readonly class Genetics
{
    private function __construct(
        public Breed $breed,
        public Sex $sex,
        public ?WhiteAmount $whiteAmount,
        public ?EyeColor $eyeColor,
        public WhiteAlleles $whiteAlleles,
        public OrangeAlleles $orangeAlleles,
        public BlackAlleles $blackAlleles,
        public DilutorAlleles $dilutorAlleles,
        public DiluteModifierAlleles $diluteModifierAlleles,
        public ColorAlleles $colorAlleles,
        public AgutiAlleles $agutiAlleles,
        public SilverAlleles $silverAlleles,
        public WithWhiteAlleles $withWhiteAlleles,
        public TabbyAlleles $tabbyAlleles,
        public PraAlleles $praAlleles,
    ) {
    }

    public static function fromCharacteristics(
        Breed $breed,
        Sex $sex,
        Color $color,
        bool $siamesePointed = false,
        bool $caramel = false,
        ?SilverSmoke $silverSmoke = null,
        ?WhiteAmount $whiteAmount = null,
        ?Pattern $pattern = null,
        ?EyeColor $eyeColor = null,
        ?PRA $pra = null,
    ): self {
        self::ensureCharacteristicsAreAllowable(
            $breed,
            $sex,
            $color,
            $siamesePointed,
            $caramel,
            $silverSmoke,
            $whiteAmount,
            $pattern,
            $eyeColor,
        );

        $whiteAlleles = new WhiteAlleles(
            Color::WHITE === $color ? WhiteAllele::W : WhiteAllele::w,
            Color::WHITE === $color ? null : WhiteAllele::w,
        );

        $orangeAlleles = new OrangeAlleles(
            Color::WHITE === $color ? null : ($color->containsRed() ? OrangeAllele::O : OrangeAllele::o),
            match (true) {
                Sex::MALE === $sex => OrangeAllele::Y,
                Color::WHITE === $color => null,
                $color->isOneOfRed() => OrangeAllele::O,
                default => OrangeAllele::o,
            },
        );

        $blackAlleles = new BlackAlleles(
            match (true) {
                $color->containsBlackOrBlue() => BlackAllele::B,
                $color->containsChocolateOrLilac() => BlackAllele::b,
                $color->containsCinnamonSorrelOrFawn() => BlackAllele::bl,
                default => null,
            },
            $color->containsCinnamonSorrelOrFawn() ? BlackAllele::bl : null,
        );

        $dilutorAlleles = new DilutorAlleles(
            match (true) {
                Color::WHITE === $color => null,
                $color->isRedOrContainsBlackOrChocolateOrCinnamon() => DilutorAllele::D,
                default => DilutorAllele::d,
            },
            match (true) {
                Color::WHITE === $color => null,
                $color->isRedOrContainsBlackOrChocolateOrCinnamon() => null,
                default => DilutorAllele::d,
            },
        );

        $diluteModifierAlleles = new DiluteModifierAlleles(
            $color->isBlueOrCreamOrLilacOrFawn()
                ? ($caramel ? DiluteModifierAllele::Dm : DiluteModifierAllele::dm)
                : null,
            $color->isBlueOrCreamOrLilacOrFawn() && !$caramel ? DiluteModifierAllele::dm : null,
        );

        $colorAlleles = new ColorAlleles(
            match (true) {
                $breed->isBalOrSia() || $siamesePointed => ColorAllele::cs,
                Color::WHITE === $color && $eyeColor?->isOneOfDeepBlue() => null,
                default => ColorAllele::C,
            },
            $breed->isBalOrSia() || $siamesePointed ? ColorAllele::cs : null,
        );

        $agutiAlleles = new AgutiAlleles(
            Color::WHITE === $color ? null : (null !== $pattern ? AgutiAllele::A : AgutiAllele::a),
            Color::WHITE === $color || null !== $pattern ? null : AgutiAllele::a,
        );

        $silverAlleles = new SilverAlleles(
            Color::WHITE === $color ? null : (null !== $silverSmoke ? SilverAllele::I : SilverAllele::i),
            Color::WHITE === $color || null !== $silverSmoke ? null : SilverAllele::i,
        );

        $withWhiteAlleles = new WithWhiteAlleles(
            Color::WHITE === $color ? null : (null !== $whiteAmount ? WithWhiteAllele::S : WithWhiteAllele::s),
            Color::WHITE === $color ? null : (null !== $whiteAmount ? null : WithWhiteAllele::s),
        );

        $tabbyAlleles = new TabbyAlleles(
            match (true) {
                Color::WHITE === $color || null === $pattern || Pattern::UNSPECIFIED_TABBY === $pattern => null,
                Pattern::TICKED_TABBY === $pattern => TabbyAllele::Ta,
                Pattern::MACKEREL_TABBY === $pattern => TabbyAllele::T,
                Pattern::SPOTTED_TABBY === $pattern => TabbyAllele::ts,
                Pattern::BLOTCHED_TABBY === $pattern => TabbyAllele::tb,
            },
            Color::WHITE === $color || Pattern::BLOTCHED_TABBY !== $pattern ? null : TabbyAllele::tb,
        );

        $praAlleles = new PraAlleles(
            null === $pra ? null : (PRA::MM === $pra ? PraAllele::M : PraAllele::N),
            null === $pra ? null : (PRA::NN === $pra ? PraAllele::N : PraAllele::M),
        );

        return new self(
            $breed,
            $sex,
            $whiteAmount,
            $eyeColor,
            $whiteAlleles,
            $orangeAlleles,
            $blackAlleles,
            $dilutorAlleles,
            $diluteModifierAlleles,
            $colorAlleles,
            $agutiAlleles,
            $silverAlleles,
            $withWhiteAlleles,
            $tabbyAlleles,
            $praAlleles,
        );
    }

    private static function ensureCharacteristicsAreAllowable(
        Breed $breed,
        Sex $sex,
        Color $color,
        bool $siamesePointed,
        bool $caramel,
        ?SilverSmoke $silverSmoke,
        ?WhiteAmount $whiteAmount,
        ?Pattern $pattern,
        ?EyeColor $eyeColor,
    ): void {
        if (Sex::MALE === $sex && $color->isOneOfTortie()) {
            CatValidationException::throwMaleCantBeTortieException();
        }

        if ($siamesePointed && $breed->isOriental()) {
            CatValidationException::throwForbiddenColorPointAndOrientalBreedException($breed);
        }

        if ($siamesePointed && Color::WHITE === $color) {
            CatValidationException::throwForbiddenColorPointAndWhiteColorException();
        }

        if ($caramel && !$color->isBlueOrCreamOrLilacOrFawn()) {
            CatValidationException::throwForbiddenCaramelAndColorException($color);
        }

        if (null !== $silverSmoke && $breed->isBalOrSia()) {
            CatValidationException::throwForbiddenSilverAndBreedException($breed);
        }

        if (null !== $silverSmoke && Color::WHITE === $color) {
            CatValidationException::throwForbiddenSilverAndWhiteColorException($silverSmoke);
        }

        if (WhiteAmount::UNSPECIFIED === $whiteAmount && $breed->isOriental()) {
            CatValidationException::throwForbiddenUnspecifiedWhiteAmountAndOrientalBreedException($breed);
        }

        if (null !== $whiteAmount && Color::WHITE === $color) {
            CatValidationException::throwForbiddenWhiteAmountAndWhiteColorException($whiteAmount);
        }

        if ($pattern?->isSpecified() && $breed->isBalOrSia()) {
            CatValidationException::throwForbiddenSpecifiedPatternAndSiaOrBalException($pattern, $breed);
        }

        if (null !== $pattern && Color::WHITE === $color) {
            CatValidationException::throwForbiddenWhiteAmountAndPatternException($pattern);
        }

        if ($pattern?->isSpecified() && $siamesePointed) {
            CatValidationException::throwForbiddenSpecifiedPatternAndSiamesePointedException($pattern);
        }

        if (
            Pattern::UNSPECIFIED_TABBY === $pattern &&
            WhiteAmount::BICOLOUR === $whiteAmount &&
            $breed->isOlhOrOsh()
        ) {
            CatValidationException::throwForbiddenUnspecifiedPatternAndBicolourWhiteAmountAndOlhOrOshException($breed);
        }

        if (EyeColor::DEEP_BLUE === $eyeColor && Breed::PEB === $breed) {
            CatValidationException::throwForbiddenEyeColorAndBreedException($eyeColor, $breed);
        }

        if (!in_array($eyeColor, [null, EyeColor::INTENSE_DEEP_BLUE], true) && $breed->isBalOrSia()) {
            CatValidationException::throwForbiddenEyeColorAndBreedException($eyeColor, $breed);
        }

        if (
            !in_array($eyeColor, [null, EyeColor::DEEP_BLUE, EyeColor::ODD, EyeColor::GREEN], true) &&
            $breed->isOlhOrOsh()
        ) {
            CatValidationException::throwForbiddenEyeColorAndBreedException($eyeColor, $breed);
        }

        if (null === $eyeColor && Color::WHITE === $color) {
            CatValidationException::throwForbiddenNoEyeColorAndWhiteColorException();
        }

        if (
            !in_array($eyeColor, [null, EyeColor::DEEP_BLUE, EyeColor::INTENSE_DEEP_BLUE, EyeColor::ODD], true) &&
            $siamesePointed
        ) {
            CatValidationException::throwForbiddenEyeColorAndSiamesePointedException($eyeColor);
        }

        if (
            !in_array($eyeColor, [null, EyeColor::ODD, EyeColor::GREEN], true) &&
            !in_array($pattern, [null, Pattern::UNSPECIFIED_TABBY], true)
        ) {
            CatValidationException::throwForbiddenEyeColorAndPatternException($eyeColor, $pattern);
        }
    }
}
