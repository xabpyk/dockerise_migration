<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\ExternalCharacteristic;

enum Pattern
{
    case BLOTCHED_TABBY;
    case MACKEREL_TABBY;
    case SPOTTED_TABBY;
    case TICKED_TABBY;
    case UNSPECIFIED_TABBY;

    public function isSpecified(): bool
    {
        return Pattern::UNSPECIFIED_TABBY !== $this;
    }
}
