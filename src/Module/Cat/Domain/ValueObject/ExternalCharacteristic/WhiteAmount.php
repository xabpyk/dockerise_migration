<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\ExternalCharacteristic;

enum WhiteAmount
{
    case VAN;
    case HARLEQUIN;
    case BICOLOUR;
    case UNSPECIFIED;
}
