<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\ExternalCharacteristic;

enum SilverSmoke
{
    case SILVER;
    case SMOKE;
}
