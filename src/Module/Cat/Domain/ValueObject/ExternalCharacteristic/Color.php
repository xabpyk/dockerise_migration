<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\ExternalCharacteristic;

enum Color
{
    case BLACK;
    case BLACK_TORTIE;
    case BLUE;
    case BLUE_TORTIE;
    case CHOCOLATE;
    case CHOCOLATE_TORTIE;
    case CINNAMON_SORREL;
    case CINNAMON_SORREL_TORTIE;
    case CREAM;
    case FAWN;
    case FAWN_TORTIE;
    case LILAC;
    case LILAC_TORTIE;
    case RED;
    case WHITE;

    public function isOneOfRed(): bool
    {
        return in_array($this, [Color::CREAM, Color::RED], true);
    }

    public function containsRed(): bool
    {
        return in_array(
            $this,
            [
                Color::BLACK_TORTIE,
                Color::BLUE_TORTIE,
                Color::CHOCOLATE_TORTIE,
                Color::CINNAMON_SORREL_TORTIE,
                Color::CREAM,
                Color::FAWN_TORTIE,
                Color::LILAC_TORTIE,
                Color::RED,
            ],
            true,
        );
    }

    public function isOneOfTortie(): bool
    {
        return in_array(
            $this,
            [
                Color::BLACK_TORTIE,
                Color::BLUE_TORTIE,
                Color::CHOCOLATE_TORTIE,
                Color::CINNAMON_SORREL_TORTIE,
                Color::FAWN_TORTIE,
                Color::LILAC_TORTIE,
            ],
            true,
        );
    }

    public function containsBlackOrBlue(): bool
    {
        return in_array($this, [Color::BLACK, Color::BLACK_TORTIE, Color::BLUE, Color::BLUE_TORTIE], true);
    }

    public function containsChocolateOrLilac(): bool
    {
        return in_array(
            $this,
            [
                Color::CHOCOLATE,
                Color::CHOCOLATE_TORTIE,
                Color::LILAC,
                Color::LILAC_TORTIE,
            ],
            true,
        );
    }

    public function containsCinnamonSorrelOrFawn(): bool
    {
        return in_array(
            $this,
            [Color::CINNAMON_SORREL, Color::CINNAMON_SORREL_TORTIE, Color::FAWN, Color::FAWN_TORTIE],
            true,
        );
    }

    public function isRedOrContainsBlackOrChocolateOrCinnamon(): bool
    {
        return in_array(
            $this,
            [
                Color::RED,
                Color::BLACK,
                Color::BLACK_TORTIE,
                Color::CHOCOLATE,
                Color::CHOCOLATE_TORTIE,
                Color::CINNAMON_SORREL,
                Color::CINNAMON_SORREL_TORTIE,
            ],
            true,
        );
    }

    public function isBlueOrCreamOrLilacOrFawn(): bool
    {
        return in_array($this, [Color::BLUE, Color::CREAM, Color::LILAC, Color::FAWN], true);
    }
}
