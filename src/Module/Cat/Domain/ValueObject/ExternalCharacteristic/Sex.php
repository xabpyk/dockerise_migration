<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\ExternalCharacteristic;

enum Sex
{
    case MALE;
    case FEMALE;
}
