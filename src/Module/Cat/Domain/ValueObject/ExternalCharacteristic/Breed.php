<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\ExternalCharacteristic;

enum Breed
{
    case PEB;
    case BAL;
    case SIA;
    case OLH;
    case OSH;

    public static function shortHairOrientalCases(): array
    {
        return [Breed::SIA, Breed::OSH];
    }

    public function isShortHairOriental(): bool
    {
        return in_array($this, self::shortHairOrientalCases(), true);
    }

    public static function longHairOrientalCases(): array
    {
        return [Breed::BAL, Breed::OLH];
    }

    public function isLongHairOriental(): bool
    {
        return in_array($this, self::longHairOrientalCases(), true);
    }

    public static function OrientalCases(): array
    {
        return [Breed::BAL, Breed::SIA, Breed::OLH, Breed::OSH];
    }

    public function isOriental(): bool
    {
        return in_array($this, self::OrientalCases(), true);
    }

    public function isBalOrSia(): bool
    {
        return in_array($this, [Breed::BAL, Breed::SIA], true);
    }

    public function isOlhOrOsh(): bool
    {
        return in_array($this, [Breed::OLH, Breed::OSH], true);
    }
}

