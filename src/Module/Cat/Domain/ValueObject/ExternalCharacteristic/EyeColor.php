<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\ExternalCharacteristic;

enum EyeColor
{
    case DEEP_BLUE;
    case ODD;
    case GREEN;
    case YELLOW;
    case AQUAMARINE;
    case INTENSE_DEEP_BLUE;

    public function isOneOfDeepBlue(): bool
    {
        return in_array($this, [EyeColor::DEEP_BLUE, EyeColor::INTENSE_DEEP_BLUE], true);
    }
}
