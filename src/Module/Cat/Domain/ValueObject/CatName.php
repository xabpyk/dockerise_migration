<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject;

use App\Module\Cat\Domain\Exception\CatValidationException;

final readonly class CatName
{
    public const int MIN_LENGTH = 7;
    public const int MAX_LENGTH = 45;
    public const int MIN_LENGTH_WITHOUT_SPACE = 3;

    public function __construct(
        public string $name,
    ) {
        if (mb_strlen($this->name) < self::MIN_LENGTH) {
            CatValidationException::throwCatNameMinLengthViolatedException(self::MIN_LENGTH);
        } elseif (mb_strlen($this->name) > self::MAX_LENGTH) {
            CatValidationException::throwCatNameMaxLengthViolatedException(self::MAX_LENGTH);
        } elseif (trim($this->name) !== $this->name) {
            CatValidationException::throwCatNameHasWhitespaceOnEdgeException();
        } elseif (mb_strpos($this->name, ' ') === false) {
            CatValidationException::throwCatNameDoesNotContainSpaceException();
        } elseif (mb_strpos($this->name, ' ') <= self::MIN_LENGTH_WITHOUT_SPACE - 1) {
            CatValidationException::throwCatNameWithSpaceInFirstFewCharactersException(self::MIN_LENGTH_WITHOUT_SPACE);
        }
    }
}
