<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Allele;

enum ColorAllele
{
    case C;
    case cs;
    case cb;
    case ca;
    case c;
 }
