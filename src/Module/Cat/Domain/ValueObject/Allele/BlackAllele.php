<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Allele;

enum BlackAllele
{
    case B;
    case b;
    case bl;
}
