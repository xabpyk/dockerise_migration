<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Allele;

enum DiluteModifierAllele
{
    case Dm;
    case dm;
}
