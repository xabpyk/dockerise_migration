<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Allele;

enum OrangeAllele
{
    case O;
    case o;
    case Y;
}
