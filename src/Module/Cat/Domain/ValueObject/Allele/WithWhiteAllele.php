<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Allele;

enum WithWhiteAllele
{
    case S;
    case s;
}
