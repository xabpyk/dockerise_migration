<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Allele;

enum TabbyAllele
{
    case Ta;
    case T;
    case ts;
    case tb;
}
