<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject;

use App\Module\Cat\Domain\Exception\CatValidationException;

final readonly class PedigreeNumber
{
    public const int MIN_LENGTH = 3;
    public const int MAX_LENGTH = 32;

    public function __construct(
        public string $number,
    ) {
        $this->ensureNumberIsValid();
    }

    private function ensureNumberIsValid(): void
    {
        $length = mb_strlen($this->number);

        if ($length < self::MIN_LENGTH) {
            CatValidationException::throwPedigreeNumberMinLengthViolatedException(self::MIN_LENGTH);
        } elseif (mb_strlen($this->number) > self::MAX_LENGTH) {
            CatValidationException::throwPedigreeNumberMaxLengthViolatedException(self::MAX_LENGTH);
        } elseif (trim($this->number) !== $this->number) {
            CatValidationException::throwPedigreeNumberHasWhitespaceOnEdgeException();
        } elseif (!preg_match('/^[a-zA-Z0-9() -]{3,}$/', $this->number)) {
            CatValidationException::throwPedigreeNumberContainsInvalidCharacterException();
        } elseif (!preg_match('/[0-9]/', $this->number)) {
            CatValidationException::throwPedigreeNumberDoesNotContainAnyDigitsException();
        }
    }
}
