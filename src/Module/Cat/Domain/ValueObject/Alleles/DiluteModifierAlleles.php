<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Alleles;

use App\Module\Cat\Domain\ValueObject\Allele\DiluteModifierAllele;

final readonly class DiluteModifierAlleles
{
    public function __construct(
        public ?DiluteModifierAllele $allele1,
        public ?DiluteModifierAllele $allele2,
    ) {
    }
}
