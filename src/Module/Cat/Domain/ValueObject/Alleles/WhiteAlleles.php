<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Alleles;

use App\Module\Cat\Domain\ValueObject\Allele\WhiteAllele;

final readonly class WhiteAlleles
{
    public function __construct(
        public WhiteAllele $allele1,
        public ?WhiteAllele $allele2,
    ) {
    }
}
