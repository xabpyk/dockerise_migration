<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Alleles;

use App\Module\Cat\Domain\ValueObject\Allele\AgutiAllele;

final readonly class AgutiAlleles
{
    public function __construct(
        public ?AgutiAllele $allele1,
        public ?AgutiAllele $allele2,
    ) {
    }
}
