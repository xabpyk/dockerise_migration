<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Alleles;

use App\Module\Cat\Domain\ValueObject\Allele\DilutorAllele;

final readonly class DilutorAlleles
{
    public function __construct(
        public ?DilutorAllele $allele1,
        public ?DilutorAllele $allele2,
    ) {
    }
}
