<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Alleles;

use App\Module\Cat\Domain\ValueObject\Allele\TabbyAllele;

final readonly class TabbyAlleles
{
    public function __construct(
        public ?TabbyAllele $allele1,
        public ?TabbyAllele $allele2,
    ) {
    }
}
