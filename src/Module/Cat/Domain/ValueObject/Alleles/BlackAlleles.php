<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Alleles;

use App\Module\Cat\Domain\ValueObject\Allele\BlackAllele;

final readonly class BlackAlleles
{
    public function __construct(
        public ?BlackAllele $allele1,
        public ?BlackAllele $allele2,
    ) {
    }
}
