<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Alleles;

use App\Module\Cat\Domain\ValueObject\Allele\OrangeAllele;
use App\Module\Cat\Domain\Exception\CatValidationException;

final readonly class OrangeAlleles
{
    public function __construct(
        public ?OrangeAllele $allele1,
        public ?OrangeAllele $allele2,
    ) {
        if ($this->allele1 === OrangeAllele::Y && $this->allele2 === OrangeAllele::Y) {
            CatValidationException::throwAllelesOrangeHasTwoYException();
        }
    }
}
