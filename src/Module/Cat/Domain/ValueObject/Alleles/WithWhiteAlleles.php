<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Alleles;

use App\Module\Cat\Domain\ValueObject\Allele\WithWhiteAllele;

final readonly class WithWhiteAlleles
{
    public function __construct(
        public ?WithWhiteAllele $allele1,
        public ?WithWhiteAllele $allele2,
    ) {
    }
}
