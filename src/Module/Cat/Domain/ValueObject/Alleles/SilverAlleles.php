<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Alleles;

use App\Module\Cat\Domain\ValueObject\Allele\SilverAllele;

final readonly class SilverAlleles
{
    public function __construct(
        public ?SilverAllele $allele1,
        public ?SilverAllele $allele2,
    ) {
    }
}
