<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Alleles;

use App\Module\Cat\Domain\ValueObject\Allele\ColorAllele;

final readonly class ColorAlleles
{
    public function __construct(
        public ?ColorAllele $allele1,
        public ?ColorAllele $allele2,
    ) {
    }
}
