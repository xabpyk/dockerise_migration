<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\ValueObject\Alleles;

use App\Module\Cat\Domain\ValueObject\Allele\PraAllele;

final readonly class PraAlleles
{
    public function __construct(
        public ?PraAllele $allele1,
        public ?PraAllele $allele2,
    ) {
    }
}
