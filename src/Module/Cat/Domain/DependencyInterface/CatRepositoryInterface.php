<?php

declare(strict_types=1);

namespace App\Module\Cat\Domain\DependencyInterface;

use App\Module\Cat\Domain\Entity\Cat;

interface CatRepositoryInterface
{
    public function nextId(): int;

    public function catWithSameNameExists(Cat $cat): bool;

    public function catWithSamePedigreeNumberExists(Cat $cat): bool;

    public function getCatsById(int $id): Cat;

    /** @return Cat[] */
    public function getCatsByIds(int ...$ids): iterable;

    /** @return Cat[] */
    public function getChildren(int $id): iterable;

    public function add(Cat $cat): void;

    public function update(Cat ...$cat): void;

    public function remove(int $id): void;
}
