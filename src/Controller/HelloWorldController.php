<?php

declare(strict_types=1);

namespace App\Controller;

use App\Common\Db\DbConnection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HelloWorldController extends AbstractController
{
    #[Route('/hello/world', name: 'hello.world')]
    public function helloWorldAction(DbConnection $db)
    {
        $now = $db->getOne("SELECT NOW()");

        return $this->render('hello_world.html.twig', [
            'hello_world' => 'Hello World!!!',
            'now_db' => $now,
            'now_php' => date('Y-m-d H:i:s'),
        ]);
    }
}
